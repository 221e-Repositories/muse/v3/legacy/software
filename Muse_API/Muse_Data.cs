﻿using System;

namespace _221e.Muse
{
    public class Muse_Data
    {
        private float[] gyro;
        private float[] axl;
        private float[] hdr_axl;
        private float[] magn;
        private int[] enc;
        private float[] th;
        private float[] tp;
        private int[] range;
        private float[] quaternion;
        private UInt64 timestamp;

        public Muse_Data()
        {
            gyro = new float[3];
            axl = new float[3];
            hdr_axl = new float[3];
            magn = new float[3];
            enc = new int[3];
            timestamp = 0;
            th = new float[2];
            tp = new float[2];
            range = new int[3];
            quaternion = new float[4];
        }

        public float[] Gyro
        {
            get { return gyro; }
            set
            {
                gyro[0] = value[0];
                gyro[1] = value[1];
                gyro[2] = value[2];
            }
        }

        public float[] Axl
        {
            get { return axl; }
            set
            {
                axl[0] = value[0];
                axl[1] = value[1];
                axl[2] = value[2];
            }
        }

        public float[] HDR_Axl
        {
            get { return hdr_axl; }
            set
            {
                hdr_axl[0] = value[0];
                hdr_axl[1] = value[1];
                hdr_axl[2] = value[2];
            }
        }

        public UInt64 Timestamp
        {
            get { return timestamp; }
            set { timestamp = value; }
        }

        public float[] Magn
        {
            get { return magn; }
            set
            {
                magn[0] = value[0];
                magn[1] = value[1];
                magn[2] = value[2];
            }
        }


        public int[] ENC
        {
            get { return enc; }
            set
            {
                enc[0] = value[0];
                enc[1] = value[1];
                enc[2] = value[2];
            }
        }

        public float[] TH
        {
            get { return th; }
            set
            {
                th[0] = value[0];
                th[1] = value[1];
            }
        }

        public float[] TP
        {
            get { return tp; }
            set
            {
                tp[0] = value[0];
                tp[1] = value[1];
            }
        }

        public int[] Range
        {
            get { return range; }
            set
            {
                range[0] = value[0];
                range[1] = value[1];
                range[2] = value[2];
            }
        }

        public float[] Quaternion
        {
            get { return quaternion; }
            set
            {
                quaternion[0] = value[0];
                quaternion[1] = value[1];
                quaternion[2] = value[2];
                quaternion[3] = value[3];
            }
        }

        public override string ToString()
        {
            string s =
                timestamp.ToString() + "\t" + getAXLString() + "\t" + getGyroString() + "\t" + getMagnString() + "\t" + getHDRAXLString() + "\t" + getENCString();
            return s;
        }

        // string

        public string getAXLString()
        {
            return axl[0] + "\t" + axl[1] + "\t" + axl[2];
        }

        public string getGyroString()
        {
            return gyro[0] + "\t" + gyro[1] + "\t" + gyro[2];
        }

        public string getMagnString()
        {
            return magn[0] + "\t" + magn[1] + "\t" + magn[2];
        }

        public string getHDRAXLString()
        {
            return hdr_axl[0] + "\t" + hdr_axl[1] + "\t" + hdr_axl[2];
        }

        public string getOrientationString()
        {
            return quaternion[0] + "\t" + quaternion[1] + "\t" + quaternion[2] + "\t" + quaternion[3];
        }

        public string getENCString()
        {
            return enc[0] + "\t" + enc[1] + "\t" + enc[2];
        }

        public string getTimestampString()
        {
            return timestamp.ToString();
        }

        public string getTHString()
        {
            return th[0] + "\t" + th[1];
        }

        public string getTPString()
        {
            return tp[0] + "\t" + tp[1];
        }

        public string getRangeString()
        {
            return range[0] + "\t" + range[1] + "\t" + range[2];
        }

    }

    


}
