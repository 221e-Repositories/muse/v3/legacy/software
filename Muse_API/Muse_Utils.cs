﻿using System;
using System.Collections.Generic;
using static _221e.Muse.Muse_HW;

namespace _221e.Muse
{
    public class Muse_Utils
    {
        

        #region CYCLICK REDUNDANCY CHECK

        public static uint CRC32(uint crc, uint data)
        {
            crc = crc ^ data;

            for (int i = 0; i < 32; i++)
                if ((crc & 0x80000000) != 0)
                    crc = (crc << 1) ^ 0x04C11DB7; // Polynomial used in STM32
                else
                    crc = (crc << 1);

            return (crc);
        }


        #endregion

        #region ENCODING COMMAND IMPLEMENTATION

        // General commands 
        // CMD_ACK = 0x00,                 //!< CMD_ACK
        public static byte[] Cmd_Acknowledge(Muse_HW.Acknowledge_Type ack)
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 3;

            buffer[0] = 0x00;
            buffer[1] = Convert.ToByte(respLen);
            buffer[2] = (byte)((byte)Muse_HW.Command.CMD_STATE);
            buffer[3] = (byte)((byte)ack);
            buffer[4] = (byte)((byte)Muse_HW.SystemState.SYS_CALIB);

            return buffer;
        }

        // CMD_SHUTDOWN = 0x01,            //!< CMD_SHUTDOWN
        public static byte[] Cmd_Shutdown()
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];

            // Header
            buffer[0] = (byte)((byte)Muse_HW.Command.CMD_SHUTDOWN);
            buffer[1] = 0x00;

            return buffer;
        }

        // CMD_STATE = 0x02,               //!< CMD_STATE
        public static byte[] Cmd_GetState()
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 0;

            // Header
            buffer[0] = (byte)((byte)Muse_HW.Command.CMD_STATE + 0x80);
            buffer[1] = Convert.ToByte(respLen);

            return buffer;
        }

        public static byte[] Cmd_SetState(Muse_HW.SystemState state, byte[] stateParams = null)
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 1;

            // Header
            buffer[0] = (byte)Muse_HW.Command.CMD_STATE;
            buffer[1] = Convert.ToByte(respLen);

            // Payload
            buffer[2] = (byte)state;

            // Parameters, if necessary
            switch (state)
            {
                // Bootloader only
                case Muse_HW.SystemState.SYS_BOOT_STARTUP: // = 0xF0, //!< APP_STARTUP
                case Muse_HW.SystemState.SYS_BOOT_IDLE: // = 0xF1, //!< APP_BOOT_IDLE
                case Muse_HW.SystemState.SYS_BOOT_WRITE: // = 0xF2, //!< APP_BOOT_WRITE
                case Muse_HW.SystemState.SYS_ERROR: // = 0xFF, //!< APP_ERROR

                // Application side
                case Muse_HW.SystemState.SYS_STARTUP: // = 0x01, //!< SYS_STARTUP
                case Muse_HW.SystemState.SYS_IDLE: // = 0x02, //!< SYS_IDLE
                case Muse_HW.SystemState.SYS_STANDBY: // = 0x03, //!< SYS_STANDBY
                case Muse_HW.SystemState.SYS_LOG: // = 0x04, //!< SYS_LOG
                case Muse_HW.SystemState.SYS_STREAMING: // = 0x06 //!< SYS_TX
                    break;
                case Muse_HW.SystemState.SYS_CALIB:
                    if (stateParams != null)
                    {
                        buffer[1] = Convert.ToByte(respLen + 5);
                        buffer[3] = Convert.ToByte(stateParams[0]);
                        buffer[4] = Convert.ToByte(stateParams[1]);
                        buffer[5] = Convert.ToByte(stateParams[2]);
                        buffer[6] = Convert.ToByte(stateParams[3]);
                        buffer[7] = Convert.ToByte(stateParams[4]);

                        //byte[] tmp = new byte[4];
                        //tmp = BitConverter.GetBytes(stateParams[1]);

                        //Array.Copy(tmp, 0, buffer, 4, 4);


                    }
                    break;   
                default:
                    break;
            }

            return buffer;
        }

        public static byte[] Cmd_StartStreaming(Muse_HW.Acquisition_Type streamMode, Muse_HW.AcquisitionFrequency streamFreq)
        {
            // Build message buffer
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 5;

            // Header
            buffer[0] = (byte)Muse_HW.Command.CMD_STATE;       // command
            buffer[1] = Convert.ToByte(respLen);                // length

            // Payload: nothing
            buffer[2] = (byte)Muse_HW.SystemState.SYS_STREAMING;

            byte[] tmp = new byte[4];
            tmp = BitConverter.GetBytes((UInt32)streamMode);

            Array.Copy(tmp,0,buffer,3,3);

            buffer[6] = (byte)streamFreq;       // Force streaming @ 5Hz

            return buffer;
        }

        public static byte[] Cmd_StartLog(Muse_HW.Acquisition_Type logMode, Muse_HW.AcquisitionFrequency logFreq)
        {
            // Build message buffer
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 5;

            // Header
            buffer[0] = (byte)Muse_HW.Command.CMD_STATE;       // command
            buffer[1] = Convert.ToByte(respLen);                // length

            // Payload: nothing
            buffer[2] = (byte)Muse_HW.SystemState.SYS_LOG;

            byte[] tmp = new byte[3];
            tmp = BitConverter.GetBytes((UInt32)logMode);

            Array.Copy(tmp, 0, buffer, 3, 3);

            buffer[6] = (byte)logFreq;       // Force streaming @ 5Hz

            return buffer;
        }

        // CMD_RESTART = 0x03,             //!< CMD_RESTART
        public static byte[] Cmd_Restart(Muse_HW.RestartMode restartMode)
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 1;

            // Header
            buffer[0] = (byte)Muse_HW.Command.CMD_RESTART;     // command
            buffer[1] = Convert.ToByte(respLen);               // length

            // Payload
            buffer[2] = (byte)restartMode;

            return buffer;
        }

        // CMD_APP_CRC = 0x04,             //!< CMD_APP_CRC
        public static byte[] Cmd_GetCRC()
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 0;

            buffer[0] = (byte)((byte)Muse_HW.Command.CMD_APP_INFO + 0x80);
            buffer[1] = Convert.ToByte(respLen);

            return buffer;
        }

        // CMD_FW_UPLOAD = 0x05,           //!< CMD_FW_UPLOAD
        // CMD_START_APP = 0x06,           //!< CMD_START_APP

        // CMD_BATTERY_CHARGE = 0x07,      //!< CMD_BATTERY_CHARGE
        public static byte[] Cmd_BatteryCharge()
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 0;

            buffer[0] = (byte)((byte)Muse_HW.Command.CMD_BATTERY_CHARGE + 0x80);
            buffer[1] = Convert.ToByte(respLen);

            return buffer;
        }

        // CMD_BATTERY_VOLTAGE = 0x08,     //!< CMD_BATTERY_VOLTAGE
        public static byte[] Cmd_BatteryVoltage()
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 0;

            buffer[0] = (byte)((byte)Muse_HW.Command.CMD_BATTERY_VOLTAGE + 0x80);
            buffer[1] = Convert.ToByte(respLen);

            return buffer;
        }

        // CMD_CHECK_UP = 0x09,            //!< CMD_CHECK_UP
        public static byte[] Cmd_Checkup()
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 0;

            // Header
            buffer[0] = (byte)((byte)Muse_HW.Command.CMD_CHECK_UP + 0x80);
            buffer[1] = Convert.ToByte(respLen);

            return buffer;
        }

        // CMD_FW_VERSION = 0x0A,          //!< CMD_FW_VERSION
        public static byte[] Cmd_FwVersion()
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 0;

            // Header
            buffer[0] = (byte)((byte)Muse_HW.Command.CMD_FW_VERSION + 0x80);
            buffer[1] = Convert.ToByte(respLen);

            return buffer;
        }

        // CMD_TIME = 0x0B,                //!< CMD_TIME
        public static byte[] Cmd_SetTime(uint secondsSinceEpoch)
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 4;

            buffer[0] = (byte)((byte)Muse_HW.Command.CMD_TIME);      // get date
            buffer[1] = Convert.ToByte(respLen);

            byte[] payload = BitConverter.GetBytes(secondsSinceEpoch);
            Array.Copy(payload, 0, buffer, 2, 4);

            return buffer;
        }
        public static byte[] Cmd_GetTime()
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 0;

            buffer[0] = (byte)((byte)Muse_HW.Command.CMD_TIME + 0x80);      // get date
            buffer[1] = Convert.ToByte(respLen);

            return buffer;
        }

        public static byte[] Cmd_SetTime()
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 0;

            buffer[0] = (byte)((byte)Muse_HW.Command.CMD_TIME);      // sset date
            buffer[1] = Convert.ToByte(respLen);

            return buffer;
        }


        // CMD_BLE_NAME = 0x0C,            //!< CMD_BLE_NAME
        public static byte[] Cmd_BLE_GetName()
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 0;

            // Header
            buffer[0] = (byte)((byte)Muse_HW.Command.CMD_BLE_NAME + 0x80);
            buffer[1] = Convert.ToByte(respLen);
            
            return buffer;
        }

        public static byte[] Cmd_BLE_SetName(string bleName)
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 0;

            // Header
            buffer[0] = (byte)Muse_HW.Command.CMD_BLE_NAME;
            respLen = bleName.Length;
            buffer[1] = Convert.ToByte(respLen);

            // Payload
            if (bleName != "" && respLen <= 15)
            {
                char[] tmpName = bleName.ToCharArray();
                for (int i = 0; i < respLen; i++)
                    buffer[2 + i] = Convert.ToByte(tmpName[i]);
            }

            return buffer;
        }

        // CMD_HW_VERSION = 0x0D,          //!< CMD_HW_VERSION
        // CMD_DEVICE_ID = 0x0E,           //!< CMD_DEVICE_ID
        public static byte[] Cmd_DeviceId()
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 0;

            // Header
            buffer[0] = (byte)((byte)Muse_HW.Command.CMD_DEVICE_ID + 0x80);
            buffer[1] = Convert.ToByte(respLen);

            return buffer;
        }



        // CMD_FS = 0x0F,                  //!< CMD_FS

        // Memory management
        // CMD_MEM_CONTROL = 0x20,         //!< CMD_MEMORY_CONTROL
        public static byte[] Cmd_Memory_Control(bool enableRead = true, byte bulk_erase = 0)
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 0;

            // Header
            if (enableRead)
            {
                respLen = 0;
                buffer[0] = (byte)((byte)Muse_HW.Command.CMD_MEM_CONTROL + 0x80);
            }
            else
            {
                respLen = 1;
                buffer[0] = (byte)Muse_HW.Command.CMD_MEM_CONTROL;
            }
                
            buffer[1] = Convert.ToByte(respLen);

            if (!enableRead)
                buffer[2] = (byte)bulk_erase;

            return buffer;
        }

        // CMD_MEM_FILE_INFO = 0x21,       //!< CMD_MEMORY_FILE_INFO
        public static byte[] Cmd_MemoryFileInfo(int fileId)
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 2;

            // Header
            buffer[0] = (byte)((byte)Muse_HW.Command.CMD_MEM_FILE_INFO + 0x80);
            buffer[1] = Convert.ToByte(respLen);

            // Payload
            byte[] valueBytes = BitConverter.GetBytes(Convert.ToInt16(fileId));
            buffer[2] = valueBytes[0];
            buffer[3] = valueBytes[1];

            return buffer;
        }

        // CMD_MEM_FILE_DOWNLOAD = 0x22,   //!< CMD_MEMORY_FILE_DOWNLOAD
        public static byte[] Cmd_MemoryFileDownload(int fileId, byte channel)
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 3;

            // Header
            buffer[0] = (byte)((byte)Muse_HW.Command.CMD_MEM_FILE_DOWNLOAD); // + 0x80);
            buffer[1] = Convert.ToByte(respLen);

            // Payload
            byte[] valueBytes = BitConverter.GetBytes(Convert.ToInt16(fileId));
            buffer[2] = valueBytes[0];
            buffer[3] = valueBytes[1];
            buffer[4] = channel; // set by default to BLE channel (if 0x00 it manages the USB transfer channel)

            return buffer;
        }

        public static byte[] Cmd_GetUserConfiguration()
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 0;

            buffer[0] = (byte)((byte)Muse_HW.Command.CMD_CFG + 0x80);
            buffer[1] = Convert.ToByte(respLen);

            return buffer;
        }

        public static byte[] Cmd_SetStandby(bool standby)
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 4;

            buffer[0] = (byte)((byte)Muse_HW.Command.CMD_CFG);
            buffer[1] = Convert.ToByte(respLen);

            // bit-mask
            byte[] tmp = new byte[2];
            tmp = BitConverter.GetBytes(1);
            Array.Copy(tmp, 0, buffer, 2, 2);

            if (standby)
            {
                tmp = BitConverter.GetBytes(1);
                Array.Copy(tmp, 0, buffer, 4, 2);
            }
            else
            {
                tmp = BitConverter.GetBytes(0);
                Array.Copy(tmp, 0, buffer, 4, 2);
            }

            return buffer;
        }

        public static byte[] Cmd_SetCircular(bool circular)
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 4;

            buffer[0] = (byte)((byte)Muse_HW.Command.CMD_CFG);
            buffer[1] = Convert.ToByte(respLen);

            // bit-mask
            byte[] tmp = new byte[2];
            tmp = BitConverter.GetBytes(2);
            Array.Copy(tmp, 0, buffer, 2, 2);

            if (circular)
            {
                tmp = BitConverter.GetBytes(2);
                Array.Copy(tmp, 0, buffer, 4, 2);
            }
            else
            {
                tmp = BitConverter.GetBytes(0);
                Array.Copy(tmp, 0, buffer, 4, 2);
            }

            return buffer;
        }

        public static byte[] Cmd_SetThresholds(float threshold1, float threshold2)
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 8;

            buffer[0] = (byte)((byte)Muse_HW.Command.CMD_CONDITIONAL_LOG_THRESHOLD);
            buffer[1] = Convert.ToByte(respLen);

            // Payload
            byte[] tmp = new byte[4];
            tmp = BitConverter.GetBytes(threshold1);
            Array.Copy(tmp, 0, buffer, 2, 4);
            tmp = BitConverter.GetBytes(threshold2);
            Array.Copy(tmp, 0, buffer, 6, 4);

            return buffer;
        }

        public static byte[] Cmd_GetThresholds()
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 0;

            buffer[0] = (byte)((byte)Muse_HW.Command.CMD_CONDITIONAL_LOG_THRESHOLD + 0x80);
            buffer[1] = Convert.ToByte(respLen);

            return buffer;

        }

        // Time sync

        // CMD_CLK_DRIFT = 0x30,           //!< CMD_CLK_DRIFT


        // CMD_CLK_OFFSET = 0x31,          //!< CMD_CLK_OFFSET
        public static byte[] Cmd_EstimateOffset()
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 0;

            // buffer[0] = (byte)((byte)Muse_HW.Command.CMD_TIME_SYNC);
            buffer[1] = Convert.ToByte(respLen);

            return buffer;
        }

        public static byte[] Cmd_GetClockOffset()
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 0;

            // buffer[0] = (byte)((byte)Muse_HW.Command.CMD_CLK_OFFSET + 0x80);
            buffer[1] = Convert.ToByte(respLen);

            return buffer;
        }

        public static byte[] Cmd_SetClockOffset(double inOffset = 0)
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 0;

            // buffer[0] = (byte)((byte)Muse_HW.Command.CMD_CLK_OFFSET);
            respLen = 8;

            // Payload
            byte[] valueBytes = BitConverter.GetBytes(Convert.ToUInt64(inOffset));
            buffer[9] = valueBytes[7];
            buffer[8] = valueBytes[6];
            buffer[7] = valueBytes[5];
            buffer[6] = valueBytes[4];
            buffer[5] = valueBytes[3];
            buffer[4] = valueBytes[2];
            buffer[3] = valueBytes[1];
            buffer[2] = valueBytes[0];

            buffer[1] = Convert.ToByte(respLen);

            return buffer;
        }

        // CMD_TIME_SYNC = 0x32,           //!< CMD_TIME_SYNC
        public static byte[] Cmd_EnterTimeSync(bool enableRead = true)
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 0;

            // if (enableRead)
                // buffer[0] = (byte)((byte)Muse_HW.Command.CMD_TIME_SYNC + 0x80);
            // else
                // buffer[0] = (byte)((byte)Muse_HW.Command.CMD_TIME_SYNC);

            buffer[1] = Convert.ToByte(respLen);

            return buffer;
        }

        // CMD_EXIT_TIME_SYNC = 0x33       //!< CMD_EXIT_TIME_SYNC           
        public static byte[] Cmd_ExitTimeSync()
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 0;

            // buffer[0] = (byte)((byte)Muse_HW.Command.CMD_EXIT_TIME_SYNC);
            buffer[1] = Convert.ToByte(respLen);

            return buffer;
        }

        // CMD_FW_UPLOAD = 0x02 //!< CMD_FW_UPLOAD (Bootloader only)
        public static byte[] Cmd_FirmwareUpload()
        {
            // !!! Bootloader only !!!

            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            return buffer;
        }

        // CMD_START = 0x03 //!< CMD_START
        public static byte[] Cmd_StartApplication()
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            return buffer;
        }

        // CMD_PARAMETERS = 0x11 //!< CMD_PARAMETERS
        public static byte[] Cmd_GetParameters()
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            return buffer;
        }

        public static byte[] Cmd_SetParameters()
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            return buffer;
        }

        // CMD_STORE = 0x12 //!< CMD_STORE
        public static byte[] Cmd_StoreParameters()
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            return buffer;
        }

        //CMD_SENSORS_FS = 0x40,         //!< CMD_SENSORS_FS
        public static byte[] Cmd_SetSensorsFS(Muse_HW.Accelerometer_FS axlFS, Muse_HW.Gyroscope_FS gyrFS, Muse_HW.Magnetometer_FS magFS, Muse_HW.Accelerometer_HDR_FS hdrFS)
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            buffer[0] = (byte)((byte)Muse_HW.Command.CMD_SENSORS_FS);
            int respLen = 3;

            buffer[1] = Convert.ToByte(respLen);

            byte[] fs = BitConverter.GetBytes((byte)axlFS | (byte)gyrFS | (byte)hdrFS | (byte)magFS);

            Array.Copy(fs, 0, buffer, 2, 4);

            return buffer;
        }

        public static byte[] Cmd_GetSensorsFS()
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 0;

            buffer[0] = (byte)((byte)Muse_HW.Command.CMD_FS);
            buffer[1] = Convert.ToByte(respLen);

            return buffer;
        }

        public static byte[] Cmd_SetCalibrationMatrix(byte rowId, byte colId, float r1, float r2, float r3)
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 14;

            // Header
            buffer[0] = (byte)((byte)Muse_HW.Command.CMD_CALIB_MATRIX);
            buffer[1] = Convert.ToByte(respLen);

            // Payload
            buffer[2] = rowId;                                  // Row index in the range 0-2
            buffer[3] = colId;                                  // Column index in the range 0-3

            byte[] valByteArray = BitConverter.GetBytes(r1);
            Array.Copy(valByteArray, 0, buffer, 4, 4);          // Value 1 - row 0
            valByteArray = BitConverter.GetBytes(r2);
            Array.Copy(valByteArray, 0, buffer, 8, 4);          // Value 2 - row 1
            valByteArray = BitConverter.GetBytes(r3);
            Array.Copy(valByteArray, 0, buffer, 12, 4);         // Value 3 - row 2

            return buffer;
        }

        public static byte[] Cmd_GetCalibrationMatrix(byte rowId, byte colId)
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 2;

            // Header
            buffer[0] = (byte)((byte)Muse_HW.Command.CMD_CALIB_MATRIX + 0x80);
            buffer[1] = Convert.ToByte(respLen);

            // Payload
            buffer[2] = rowId;
            buffer[3] = colId;

            return buffer;
        }

        public static byte[] Cmd_SetLogMode(Muse_HW.Acquisition_Type selectedLogMode, Muse_HW.AcquisitionFrequency selectedLogFreq)
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 4;

            // Header
            buffer[0] = (byte)((byte)Muse_HW.Command.CMD_BTN_LOG);
            buffer[1] = Convert.ToByte(respLen);

            // Payload
            byte[] tmp = new byte[3];
            tmp = BitConverter.GetBytes((UInt32)selectedLogMode);

            Array.Copy(tmp, 0, buffer, 2, 3);

            buffer[5] = (byte)selectedLogFreq;

            return buffer;
        }

        public static byte[] Cmd_GetLogMode()
        {
            byte[] buffer = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
            int respLen = 0;

            // Header
            buffer[0] = (byte)((byte)Muse_HW.Command.CMD_BTN_LOG + 0x80);
            buffer[1] = Convert.ToByte(respLen);

            return buffer;
        }

        #endregion

        #region DECODING FUNCTIONS

        public static int GetPacketDimension(UInt32 inMode)
        {
            int packet_dimension = 0;

            if ((inMode & (UInt32)Acquisition_Type.DATA_MODE_GYRO) > 0)
                packet_dimension += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_GYRO;
            if ((inMode & (UInt32)Acquisition_Type.DATA_MODE_AXL) > 0)
                packet_dimension += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_AXL;
            if ((inMode & (UInt32)Acquisition_Type.DATA_MODE_HDR) > 0)
                packet_dimension += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_HDR;
            if ((inMode & (UInt32)Acquisition_Type.DATA_MODE_MAGN) > 0)
                packet_dimension += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_MAGN;
            if ((inMode & (UInt32)Acquisition_Type.DATA_MODE_ORIENTATION) > 0)
                packet_dimension += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_ORIENTATION;
            if ((inMode & (UInt32)Acquisition_Type.DATA_MODE_TIMESTAMP) > 0)
                packet_dimension += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_TIMESTAMP;
            if ((inMode & (UInt32)Acquisition_Type.DATA_MODE_TEMP_HUM) > 0)
                packet_dimension += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_TEMP_HUM;
            if ((inMode & (UInt32)Acquisition_Type.DATA_MODE_TEMP_PRESS) > 0)
                packet_dimension += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_TEMP_PRESS;
            if ((inMode & (UInt32)Acquisition_Type.DATA_MODE_RANGE) > 0)
                packet_dimension += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_RANGE; 
            if ((inMode & (UInt32)Acquisition_Type.DATA_MODE_ENCODER) > 0)
                packet_dimension += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_ENCODER; 

            return packet_dimension;
        }

        public static Muse_Data retrieveData(uint acqMode,
                                          byte[] currentPayload,
                                          ulong timestamp,
                                          float axl_res, float gyr_res, float mag_res, float hdr_res,
                                          List<Muse_Data> encData)
        {
            int offset = 0;

            Muse_Data data = new Muse_Data();

            data.Timestamp = timestamp;

            if ((acqMode & (UInt32)Acquisition_Type.DATA_MODE_GYRO) > 0)
            {
                data.Gyro = DataTypeGYR(currentPayload, offset, gyr_res).Gyro;
                offset += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_GYRO;
            }

            if ((acqMode & (UInt32)Acquisition_Type.DATA_MODE_AXL) > 0)
            {
                data.Axl  = DataTypeAXL(currentPayload, offset, axl_res).Axl;
                offset += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_AXL;
            }
            if ((acqMode & (UInt32)Acquisition_Type.DATA_MODE_MAGN) > 0)
            {
                data.Magn = DataTypeMAGN(currentPayload, offset, mag_res).Magn;
                offset += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_MAGN;
            }
            if ((acqMode & (UInt32)Acquisition_Type.DATA_MODE_HDR) > 0)
            {
                data.HDR_Axl= DataTypeHdr(currentPayload, offset, hdr_res).HDR_Axl;
                offset += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_HDR;
            }

            if ((acqMode & (UInt32)Acquisition_Type.DATA_MODE_ORIENTATION) > 0)
            {
                data.Quaternion = DataTypeOrientation(currentPayload, offset).Quaternion;
                offset += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_ORIENTATION;
            }

            if ((acqMode & (UInt32)Acquisition_Type.DATA_MODE_ENCODER) > 0)
            {
                if (encData != null)
                    encData.Add(DataTypeEncoder(currentPayload, offset, encData));

                data.ENC = DataTypeEncoder(currentPayload, offset, encData).ENC;
                offset += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_ENCODER;
            }

            if ((acqMode & (UInt32)Acquisition_Type.DATA_MODE_TEMP_HUM) > 0)
            {
                data.TH  = DataTypeTempHum(currentPayload, offset).TH;
                offset += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_TEMP_HUM;
            }
            if ((acqMode & (UInt32)Acquisition_Type.DATA_MODE_TEMP_PRESS) > 0)
            {
                data.TP = DataTypeTempPress(currentPayload, offset).TP;
                offset += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_TEMP_PRESS;
            }
            // if ((acqMode & (UInt32)Acquisition_Type.DATA_MODE_RANGE) > 0)

            if ((acqMode & (UInt32)Acquisition_Type.DATA_MODE_TIMESTAMP) > 0)
            {
                data.Timestamp = DataTypeTimestamp(currentPayload, offset).Timestamp;
                offset += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_TIMESTAMP;
            }

            return data;
        }


        public static Muse_Data DataTypeAXL(byte[] currentPayload, int offset, float axl_res)
        {
            var currentData = new Muse_Data();
            byte[] tmp = new byte[2];

            // Axl
            Array.Copy(currentPayload, 0 + offset, tmp, 0, 2);
            currentData.Axl[0] = (float)(BitConverter.ToInt16(tmp, 0) * axl_res);
            Array.Copy(currentPayload, 2 + offset, tmp, 0, 2);
            currentData.Axl[1] = (float)(BitConverter.ToInt16(tmp, 0) * axl_res);
            Array.Copy(currentPayload, 4 + offset, tmp, 0, 2);
            currentData.Axl[2] = (float)(BitConverter.ToInt16(tmp, 0) * axl_res);

            return currentData;
        }

        public static Muse_Data DataTypeGYR(byte[] currentPayload, int offset, float gyr_res)
        {
            var currentData = new Muse_Data();
            byte[] tmp = new byte[2];

            // Gyr
            Array.Copy(currentPayload, 0 + offset, tmp, 0, 2);
            currentData.Gyro[0] = (float)(BitConverter.ToInt16(tmp, 0) * gyr_res);
            Array.Copy(currentPayload, 2 + offset, tmp, 0, 2);
            currentData.Gyro[1] = (float)(BitConverter.ToInt16(tmp, 0) * gyr_res);
            Array.Copy(currentPayload, 4 + offset, tmp, 0, 2);
            currentData.Gyro[2] = (float)(BitConverter.ToInt16(tmp, 0) * gyr_res);

            return currentData;
        }

        public static Muse_Data DataTypeMAGN(byte[] currentPayload, int offset, float mag_res)
        {
            var currentData = new Muse_Data();
            byte[] tmp = new byte[2];

            // mag
            Array.Copy(currentPayload, 0 + offset, tmp, 0, 2);
            currentData.Magn[0] = (float)(BitConverter.ToInt16(tmp, 0) * mag_res);
            Array.Copy(currentPayload, 2 + offset, tmp, 0, 2);
            currentData.Magn[1] = (float)(BitConverter.ToInt16(tmp, 0) * mag_res);
            Array.Copy(currentPayload, 4 + offset, tmp, 0, 2);
            currentData.Magn[2] = (float)(BitConverter.ToInt16(tmp, 0) * mag_res);

            return currentData;
        }

        public static Muse_Data DataTypeHdr(byte[] currentPayload, int offset, float hdr_res)
        {
            var currentData = new Muse_Data();
            byte[] tmp = new byte[2];

            // Hdr
            Array.Copy(currentPayload, 0 + offset, tmp, 0, 2);
            currentData.HDR_Axl[0] = (float)((BitConverter.ToInt16(tmp, 0) / 16) * hdr_res);
            Array.Copy(currentPayload, 2 + offset, tmp, 0, 2);
            currentData.HDR_Axl[1] = (float)((BitConverter.ToInt16(tmp, 0) / 16) * hdr_res);
            Array.Copy(currentPayload, 4 + offset, tmp, 0, 2);
            currentData.HDR_Axl[2] = (float)((BitConverter.ToInt16(tmp, 0) / 16) * hdr_res);

            return currentData;
        }

        public static Muse_Data DataTypeOrientation(byte[] currentPayload, int offset)
        {
            var currentData = new Muse_Data();
            byte[] tmp = new byte[2];

            // Orientation
            float[] tempFloat = new float[3];
            for (int i = 0; i < 3; i++)
            {
                tempFloat[i] = BitConverter.ToInt16(currentPayload, offset + i * 2);
                tempFloat[i] /= 32767;
                currentData.Quaternion[i + 1] = tempFloat[i];
            }
            currentData.Quaternion[0] = (float)Math.Sqrt(1 - (currentData.Quaternion[1] * currentData.Quaternion[1] +
                currentData.Quaternion[2] * currentData.Quaternion[2] + currentData.Quaternion[3] * currentData.Quaternion[3]));

            return currentData;
        }

        public static Muse_Data DataTypeTimestamp(byte[] currentPayload, int offset)
        {
            var currentData = new Muse_Data();
            UInt64 reference_epoch = 1580000000;

            // Timestamp
            byte[] tmp = new byte[8];
            Array.Copy(currentPayload, 0 + offset, tmp, 0, 6);
            UInt64 tempTime = (ulong)BitConverter.ToUInt64(tmp, 0);
            tempTime &= 0x0000FFFFFFFFFFFF;
            tempTime += ((UInt64)reference_epoch) * 1000;

            currentData.Timestamp = tempTime;

            return currentData;
        }

        public static Muse_Data DataTypeTempHum(byte[] currentPayload, int offset)
        {
            var currentData = new Muse_Data();

            // Temperature
            byte[] tmp = new byte[2];
            Array.Copy(currentPayload, 0 + offset, tmp, 0, 2);
            currentData.TH[0] = (float)(BitConverter.ToUInt16(tmp, 0)) * 0.002670f - 45;

            // Humidity
            Array.Copy(currentPayload, 2 + offset, tmp, 0, 2);
            currentData.TH[1] = (float)(BitConverter.ToUInt16(tmp, 0)) * 0.001907f - 6;

            return currentData;

        }

        public static Muse_Data DataTypeTempPress(byte[] currentPayload, int offset)
        {
            var currentData = new Muse_Data();

            // Temperature
            byte[] tmp = new byte[2];
            Array.Copy(currentPayload, 3 + offset, tmp, 0, 2);
            currentData.TP[0] = (float)(BitConverter.ToUInt16(tmp, 0)) / 100;

            // Pressure
            tmp = new byte[3];
            Array.Copy(currentPayload, 0 + offset, tmp, 0, 3);
            currentData.TP[1] = (float)(BitConverter.ToUInt16(tmp, 0)) / 4096;

            return currentData;
        }

        public static Muse_Data DataTypeEncoder(byte[] currentPayload, int offset, List<Muse_Data> logData)
        {
            var currentData = new Muse_Data();
            byte[] tmp = new byte[2];

            // Encoder
            Array.Copy(currentPayload, 0 + offset, tmp, 0, 2);
            currentData.ENC[0] = BitConverter.ToInt16(tmp, 0);
            Array.Copy(currentPayload, 2 + offset, tmp, 0, 2);
            currentData.ENC[1] = BitConverter.ToInt16(tmp, 0);
            Array.Copy(currentPayload, 4 + offset, tmp, 0, 2);
            currentData.ENC[2] = BitConverter.ToInt16(tmp, 0);

            // Add previous sample
            if (logData != null && logData.Count > 0)
            {
                currentData.ENC[0] += logData[logData.Count - 1].ENC[0];
                currentData.ENC[1] += logData[logData.Count - 1].ENC[1];
                currentData.ENC[2] += logData[logData.Count - 1].ENC[2];
            }

            return currentData;
        }

        public static float[] GetAnglesFromQuaternion(float[] q)
        {
            float[] result = new float[3];
            result[0] = (float)(Math.Atan2(2 * (q[0] * q[1] + q[2] * q[3]), 1 - 2 * (q[1] * q[1] + q[2] * q[2])) * 180 / Math.PI);
            result[1] = (float)(Math.Asin(2 * q[0] * q[2] - 2 * q[3] * q[1]) * 180 / Math.PI);
            result[2] = (float)(Math.Atan2(2 * (q[0] * q[3] + q[1] * q[2]), 1 - 2 * (q[2] * q[2] + q[3] * q[3])) * 180 / Math.PI);
            return result;
        }

        // TODO: to be implemented
        // DATA_SIZE_PRESSURE = 6,
        // DATA_SIZE_IMU_PRESSURE = 18

        public static float[] DecodeCalibrationMatrixValues(byte[] colVal)
        {
            float[] colValues = new float[3];

            byte[] tmp = new byte[4];
            Array.Copy(colVal, 0, tmp, 0, 4);
            colValues[0] = (float)BitConverter.ToInt32(tmp, 0);
            Array.Copy(colVal, 4, tmp, 0, 4);
            colValues[1] = (float)BitConverter.ToInt32(tmp, 0);
            Array.Copy(colVal, 8, tmp, 0, 4);
            colValues[2] = (float)BitConverter.ToInt32(tmp, 0);

            return colValues;
        }

        public static string GetDeviceStateString(byte arg)
        {
            string devStateStr = "N.A.";

            switch (arg)
            {
                // Bootloader mode only
                case (byte)Muse_HW.SystemState.SYS_BOOT_STARTUP:
                    devStateStr = "SYS_BOOT_STARTUP";
                    break;
                case (byte)Muse_HW.SystemState.SYS_BOOT_IDLE:
                    devStateStr = "SYS_BOOT_IDLE";
                    break;
                case (byte)Muse_HW.SystemState.SYS_BOOT_WRITE:
                    devStateStr = "SYS_BOOT_WRITE";
                    break;

                // Bootloader and Application modes
                case (byte)Muse_HW.SystemState.SYS_ERROR:
                    devStateStr = "SYS_ERROR";
                    break;

                // Application firmware
                case (byte)Muse_HW.SystemState.SYS_STARTUP:
                    devStateStr = "SYS_STARTUP";
                    break;
                case (byte)Muse_HW.SystemState.SYS_IDLE:
                    devStateStr = "SYS_IDLE";
                    break;
                case (byte)Muse_HW.SystemState.SYS_STANDBY:
                    devStateStr = "SYS_STANDBY";
                    break;
                case (byte)Muse_HW.SystemState.SYS_LOG:
                    devStateStr = "SYS_LOG";
                    break;
                case (byte)Muse_HW.SystemState.SYS_STREAMING:
                    devStateStr = "SYS_STREAMING";
                    break;
                default:
                    devStateStr = "N.A.";
                    break;
            }

            return devStateStr;
        }


        public static string ExtractMode(UInt32 currentAcqMode)
        {
            string modeStr = "";
            if ((currentAcqMode & (UInt32)Acquisition_Type.DATA_MODE_GYRO) > 0)
            {
                if (modeStr.Length > 0)
                    modeStr += " | ";
                modeStr += "GYRO";
            }
            if ((currentAcqMode & (UInt32)Acquisition_Type.DATA_MODE_AXL) > 0)
            {
                if (modeStr.Length > 0)
                    modeStr += " | ";
                modeStr += "AXL";
            }
            if ((currentAcqMode & (UInt32)Acquisition_Type.DATA_MODE_HDR) > 0)
            {
                if (modeStr.Length > 0)
                    modeStr += " | ";
                modeStr += "HDR";
            }
            if ((currentAcqMode & (UInt32)Acquisition_Type.DATA_MODE_MAGN) > 0)
            {
                if (modeStr.Length > 0)
                    modeStr += " | ";
                modeStr += "MAGN";
            }
            if ((currentAcqMode & (UInt32)Acquisition_Type.DATA_MODE_ORIENTATION) > 0)
            {
                if (modeStr.Length > 0)
                    modeStr += " | ";
                modeStr += "AHRS";
            }
            if ((currentAcqMode & (UInt32)Acquisition_Type.DATA_MODE_TIMESTAMP) > 0)
            {
                if (modeStr.Length > 0)
                    modeStr += " | ";
                modeStr += "TIME";
            }
            if ((currentAcqMode & (UInt32)Acquisition_Type.DATA_MODE_TEMP_HUM) > 0)
            {
                if (modeStr.Length > 0)
                    modeStr += " | ";
                modeStr += "TEMP+HUM";
            }
            if ((currentAcqMode & (UInt32)Acquisition_Type.DATA_MODE_TEMP_PRESS) > 0)
            {
                if (modeStr.Length > 0)
                    modeStr += " | ";
                modeStr += "TEMP+PRESS";
            }
            if ((currentAcqMode & (UInt32)Acquisition_Type.DATA_MODE_RANGE) > 0)
            {
                if (modeStr.Length > 0)
                    modeStr += " | ";
                modeStr += "RANGE";
            }
            if ((currentAcqMode & (UInt32)Acquisition_Type.DATA_MODE_ENCODER) > 0)
            {
                if (modeStr.Length > 0)
                    modeStr += " | ";
                modeStr += "ENC";
            }
            return modeStr;
        }

        public static string ExtractFrequency(byte currentFrequency)
        {
            // Log frequency
            string freqStr = "";

            switch (currentFrequency)
            {
                case (byte)Muse_HW.AcquisitionFrequency.DATA_FREQ_NONE: // = 0,          //!< DATA_FREQ_NONE    0x00
                    freqStr = "NONE";
                    break;
                case (byte)Muse_HW.AcquisitionFrequency.DATA_FREQ_25Hz: // = 1,          //!< DATA_FREQ_25Hz    0x01
                    freqStr = "25 Hz";
                    break;
                case (byte)Muse_HW.AcquisitionFrequency.DATA_FREQ_50Hz: // = 2,          //!< DATA_FREQ_50Hz    0x02
                    freqStr = "50 Hz";
                    break;
                case (byte)Muse_HW.AcquisitionFrequency.DATA_FREQ_100Hz: // = 4,         //!< DATA_FREQ_100Hz   0x04
                    freqStr = "100 Hz";
                    break;
                case (byte)Muse_HW.AcquisitionFrequency.DATA_FREQ_200Hz: // = 8,         //!< DATA_FREQ_200Hz   0x08
                    freqStr = "200 Hz";
                    break;
                case (byte)Muse_HW.AcquisitionFrequency.DATA_FREQ_400Hz: // = 16,        //!< DATA_FREQ_400Hz   0x10
                    freqStr = "400 Hz";
                    break;
                case (byte)Muse_HW.AcquisitionFrequency.DATA_FREQ_800Hz: // = 32,        //!< DATA_FREQ_800Hz   0x20
                    freqStr = "800 Hz";
                    break;
                case (byte)Muse_HW.AcquisitionFrequency.DATA_FREQ_1600Hz: // = 64        //!< DATA_FREQ_1600Hz  0x40
                    freqStr = "1600 Hz";
                    break;
                default:
                    freqStr = "NOT AVAILABLE";
                    break;
            }
            return freqStr;
        }

        public static Muse_HW.FullScales ExtractMEMSResolutions(UInt32 currentFSCode)
        {
            Muse_HW.FullScales full_scales;

            // Apply bitwise mask to get mems FS code (gyr, axl, hdr, mag order, LSB)
            byte gyrCode = (byte)(currentFSCode & 0x03);
            byte axlCode = (byte)(currentFSCode & 0x0C);
            byte hdrCode = (byte)(currentFSCode & 0x30);
            byte magCode = (byte)(currentFSCode & 0xC0);

            // Get accelerometer resolution from full scale
            switch (axlCode)
            {
                case (byte)Muse_HW.Accelerometer_FS.AXL_FS_4g:
                    full_scales.current_axl_resolution = Muse_HW.AXL_SENSITIVITY_4g;
                    full_scales.axl_fsStr = "4 g";
                    break;
                case (byte)Muse_HW.Accelerometer_FS.AXL_FS_08g:
                    full_scales.current_axl_resolution = Muse_HW.AXL_SENSITIVITY_8g;
                    full_scales.axl_fsStr = "8 g";
                    break;
                case (byte)Muse_HW.Accelerometer_FS.AXL_FS_16g:
                    full_scales.current_axl_resolution = Muse_HW.AXL_SENSITIVITY_16g;
                    full_scales.axl_fsStr = "16 g";
                    break;
                case (byte)Muse_HW.Accelerometer_FS.AXL_FS_32g:
                    full_scales.current_axl_resolution = Muse_HW.AXL_SENSITIVITY_32g;
                    full_scales.axl_fsStr = "32 g";
                    break;
                default:
                    full_scales.current_axl_resolution = 0;
                    full_scales.axl_fsStr = "n.a.";
                    break;
            }

            // Get gyroscope resolution from full scale
            switch (gyrCode)
            {
                case (byte)Muse_HW.Gyroscope_FS.GYR_FS_245dps:
                    full_scales.current_gyr_resolution = Muse_HW.GYR_SENSITIVITY_245dps;
                    full_scales.gyr_fsStr = "245 dps";
                    break;
                case (byte)Muse_HW.Gyroscope_FS.GYR_FS_500dps:
                    full_scales.current_gyr_resolution = Muse_HW.GYR_SENSITIVITY_500dps;
                    full_scales.gyr_fsStr = "500 dps";
                    break;
                case (byte)Muse_HW.Gyroscope_FS.GYR_FS_1000dps:
                    full_scales.current_gyr_resolution = Muse_HW.GYR_SENSITIVITY_1000dps;
                    full_scales.gyr_fsStr = "1000 dps";
                    break;
                case (byte)Muse_HW.Gyroscope_FS.GYR_FS_2000dps:
                    full_scales.current_gyr_resolution = Muse_HW.GYR_SENSITIVITY_2000dps;
                    full_scales.gyr_fsStr = "2000 dps";
                    break;
                default:
                    full_scales.current_gyr_resolution = 0;
                    full_scales.gyr_fsStr = "n.a.";
                    break;
            }

            // Get magnetometer resolution from full scale
            switch (magCode)
            {
                case (byte)Muse_HW.Magnetometer_FS.MAG_FS_04G:
                    full_scales.current_mag_resolution = Muse_HW.MAG_SENSITIVITY_04G;
                    full_scales.mag_fsStr = "4 G";
                    break;
                case (byte)Muse_HW.Magnetometer_FS.MAG_FS_08G:
                    full_scales.current_mag_resolution = Muse_HW.MAG_SENSITIVITY_08G;
                    full_scales.mag_fsStr = "8 G";
                    break;
                case (byte)Muse_HW.Magnetometer_FS.MAG_FS_12G:
                    full_scales.current_mag_resolution = Muse_HW.MAG_SENSITIVITY_12G;
                    full_scales.mag_fsStr = "12 G";
                    break;
                case (byte)Muse_HW.Magnetometer_FS.MAG_FS_16G:
                    full_scales.current_mag_resolution = Muse_HW.MAG_SENSITIVITY_16G;
                    full_scales.mag_fsStr = "16 G";
                    break;
                default:
                    full_scales.current_mag_resolution = 0;
                    full_scales.mag_fsStr = "n.a.";
                    break;
            }


            switch (hdrCode)
            {
                case (byte)Muse_HW.Accelerometer_HDR_FS.HDR_FS_100g:
                    full_scales.current_hdr_resolution = Muse_HW.HDR_SENSITIVITY_100g;
                    full_scales.hdr_fsStr = "100 g";
                    break;
                case (byte)Muse_HW.Accelerometer_HDR_FS.HDR_FS_200g:
                    full_scales.current_hdr_resolution = Muse_HW.HDR_SENSITIVITY_200g;
                    full_scales.hdr_fsStr = "200 g";
                    break;
                case (byte)Muse_HW.Accelerometer_HDR_FS.HDR_FS_400g:
                    full_scales.current_hdr_resolution = Muse_HW.HDR_SENSITIVITY_400g;
                    full_scales.hdr_fsStr = "400 g";
                    break;
                default:
                    full_scales.current_hdr_resolution = 0;
                    full_scales.hdr_fsStr = "n.a.";
                    break;
            }

            return full_scales;

        }

        #endregion
    }
}
