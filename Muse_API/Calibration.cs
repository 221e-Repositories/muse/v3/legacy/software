﻿using System;
using System.Collections.Generic;

namespace _221e.Muse
{

    public class Calibration
    {
        static int DOF = 9; // degrees of freedom
        static float[] DtD = new float[DOF * DOF];    // D'*D
        //static float iDtD[DOF*DOF]; // inv(D'*D)
        static float[] Dt1 = new float[DOF];        // D'*ones
        static float[] v = new float[DOF];          // ellipsoid parameters
        //static float v2[DOF];       // ellipsoid parameters (debug)

        static float[] DtDcopy = new float[DOF * DOF]; // see save/restore_stat_run_magcalib()
        static float[] Dt1copy = new float[DOF];

        static float vlast; // optimization (avoid risk of div-by-zero)

        static public float[] Oxyz = new float[3];    // ellipsoid center/offset
        static float[] Rxyz = new float[3];    // ellipsoid radii/gain (related to eigenvalues)
        static float[] R3mtx = new float[3 * 3]; // ellipsoid rotation matrix (eigenvectors)

        static public float[] R3mtxR = new float[3 * 3]; // optimize by composing rotation and scaling to ref radius
        static float[] OOxyz = new float[3]; // diagonal matrix * offset

        static float eig1, eig2, eig3; // eigenvectors, shared by gain_magcalib() and R3mtx_magcalib()
        static float eigN; // normalization factor

        static public void init_magcalib()
        {
            int r, c;
            for (r = 0; r < DOF; r++)
            {
                Dt1[r] = 0.0f;
                v[r] = 0.0f;
                for (c = 0; c < DOF; c++)
                    DtD[r * DOF + c] = 0.0f;
            }
            for (r = 0; r < 3; r++)
            {
                Oxyz[r] = 0.0f;
                Rxyz[r] = 1.0f;
                for (c = 0; c < 3; c++)
                    R3mtx[r * 3 + c] = ((r == c) ? 1.0f : 0.0f);
            }
        }

        static public void reset_remap_magcalib()
        {
            int r, c;
            for (r = 0; r < 3; r++)
            {
                Oxyz[r] = 0.0f;
                Rxyz[r] = 1.0f;
                for (c = 0; c < 3; c++)
                    R3mtx[r * 3 + c] = ((r == c) ? 1.0f : 0.0f);
            }
        }

        //Call this function to initilize the parameters with the ones stored in the flash
        static void set_magcalib(float[] O, float[] R)
        {
            int r, c;
            for (r = 0; r < 3; r++)
            {
                Oxyz[r] = O[r];
                for (c = 0; c < 3; c++)
                {
                    R3mtxR[r * 3 + c] = R[r * 3 + c];
                }
            }
        }

        // robust in case of near-spherical data
        static public void run_magcalib(float x, float y, float z)
        {
            int r, c;
            float x2, y2, z2, x2y2z2;
            x2 = x * x;
            y2 = y * y;
            z2 = z * z;
            x2y2z2 = x2 + y2 + z2;
            v[0] = x2 + y2 - 2.0f * z2;
            v[1] = x2 - 2.0f * y2 + z2;
            v[2] = 4.0f * x * y;
            v[3] = 2.0f * x * z;
            v[4] = 2.0f * y * z;
            v[5] = 2.0f * x;
            v[6] = 2.0f * y;
            v[7] = 2.0f * z;
            v[8] = 1.0f;
            for (r = 0; r < DOF; r++)
            {
                for (c = 0; c < DOF; c++)
                    DtD[r * DOF + c] += (v[r] * v[c]);
                Dt1[r] += v[r] * x2y2z2;
            }
        }

        static bool solve_magcalib()
        {
            float k;
            int r, r2, c2; //int c;
            // DtD is to be inverted, iDtD is the identity matrix
            for (r = 0; r < DOF; r++)
            {
                //for(c=0;c<DOF;c++) iDtD[r*DOF+c]=((r==c)?1.0L:0.0L);
                v[r] = Dt1[r]; // final multiplication merged in
            }
            for (r = 0; r < DOF; r++)
            {
                if ((k = DtD[r * DOF + r]) != 0.0f)
                {
                    for (c2 = 0; c2 < DOF; c2++)
                    {
                        DtD[r * DOF + c2] /= k;
                        //iDtD[r*DOF+c2]/=k;
                    }
                    v[r] /= k; // final multiplication merged in
                }
                else
                {
                    for (r2 = r + 1; (r2 < DOF) && ((k = DtD[r2 * DOF + r]) == 0.0f);
                            r2++)
                        ;
                    if (k == 0.0f)
                        return false; // can not be inverted
                    for (c2 = 0; c2 < DOF; c2++)
                    {
                        DtD[r * DOF + c2] += DtD[r2 * DOF + c2] / k;
                        //iDtD[r*DOF+c2]+=iDtD[r2*DOF+c2]/k;
                    }
                    v[r] += v[r2] / k; // final multiplication merged in
                }
                for (r2 = 0; r2 < DOF; r2++)
                {
                    if (r2 != r)
                    {
                        k = -DtD[r2 * DOF + r];
                        for (c2 = 0; c2 < DOF; c2++)
                        {
                            DtD[r2 * DOF + c2] += DtD[r * DOF + c2] * k;
                            //iDtD[r2*DOF+c2]+=iDtD[r*DOF+c2]*k;
                        }
                        v[r2] += v[r] * k; // final multiplication merged in
                    }
                }
            }

            // DtD is now the identity matrix, iDtD is the inverse
            //for(r=0;r<DOF;r++) { // final multiplication
            //  v2[r]=0.0L;
            //  for(c=0;c<DOF;c++) v2[r]+=(iDtD[r*DOF+c]*Dt1[c]);
            //}
            return true; // inversion successfull
        }

        // optimize by avoiding div-by-zero (last term)
        static void solve_finish_magcalib()
        {
            float t1, t2; // n;
            int r;
            vlast = v[8];
            for (r = DOF - 1; r > 2; r--)
                v[r] = -v[r - 1];
            v[3] = 2.0f * v[3];
            t1 = v[0];
            t2 = v[1];
            v[0] = (1.0f - t1 - t2);
            v[1] = (1.0f - t1 + 2.0f * t2);
            v[2] = (1.0f + 2.0f * t1 - t2);
            return;
        }

        static bool check_magcalib()
        {
            int ko = 0;
            if (v[0] <= 0.0f)
                ko = ko + 1; // a,b,c always positive
            if (v[1] <= 0.0f)
                ko = ko + 2;
            if (v[2] <= 0.0f)
                ko = ko + 4;
            if ((v[3] * v[3]) >= (4 * v[0] * v[1]))
                ko = ko + 8; // d^2-4ab<0 (XYcoeff^2-4*X2coeff*Y2coeff)
            if ((v[4] * v[4]) >= (4 * v[0] * v[2]))
                ko = ko + 16; // e^2-4ac<0 (XZcoeff^2-4*X2coeff*Z2coeff)
            if ((v[5] * v[5]) >= (4 * v[1] * v[2]))
                ko = ko + 32; // f^2-4bc<0 (YZcoeff^2-4*Y2coeff*Z2coeff)
            return (ko == 0); // return 'ok' flag
        }

        static bool offset_magcalib()
        {
            float[] T3 = new float[3 * 3];
            float k;
            int r, r2, c2;
            T3[0] = v[0];
            T3[1] = v[3];
            T3[2] = v[4];
            T3[3] = v[3];
            T3[4] = v[1];
            T3[5] = v[5];
            T3[6] = v[4];
            T3[7] = v[5];
            T3[8] = v[2];
            Oxyz[0] = -v[6];
            Oxyz[1] = -v[7];
            Oxyz[2] = -v[8];
            for (r = 0; r < 3; r++)
            {
                if ((k = T3[r * 3 + r]) != 0.0f)
                {
                    for (c2 = 0; c2 < 3; c2++)
                        T3[r * 3 + c2] /= k;
                    Oxyz[r] /= k; // final multiplication merged in
                }
                else
                {
                    for (r2 = r + 1; (r2 < 3) && ((k = T3[r2 * 3 + r]) == 0.0f); r2++)
                        ;
                    if (k == 0.0f)
                        return false; // can not be inverted
                    for (c2 = 0; c2 < 3; c2++)
                        T3[r * 3 + c2] += T3[r2 * 3 + c2] / k;
                    Oxyz[r] += Oxyz[r2] / k; // final multiplication merged in
                }
                for (r2 = 0; r2 < 3; r2++)
                {
                    if (r2 != r)
                    {
                        k = -T3[r2 * 3 + r];
                        for (c2 = 0; c2 < 3; c2++)
                            T3[r2 * 3 + c2] += T3[r * 3 + c2] * k;
                        Oxyz[r2] += Oxyz[r] * k; // final multiplication merged in
                    }
                }
            }
            return true;
        }

        static bool gain_magcalib()
        { // must be called after offset_magcalib()
            float a11, a12, a13, a22, a23, a33, b11, b12, b13, b22, b23, b33;
            float p1, p2, p, q, r, phi;
            int ok;

            eigN = (Oxyz[0] * v[0] + Oxyz[1] * v[3] + Oxyz[2] * v[4] + v[6]) * Oxyz[0]
            + (Oxyz[0] * v[3] + Oxyz[1] * v[1] + Oxyz[2] * v[5] + v[7])
                    * Oxyz[1]
            + (Oxyz[0] * v[4] + Oxyz[1] * v[5] + Oxyz[2] * v[2] + v[8])
                    * Oxyz[2]
            + (Oxyz[0] * v[6] + Oxyz[1] * v[7] + Oxyz[2] * v[8] - vlast);

            eigN = -eigN;

            a11 = v[0] / eigN;
            a12 = v[3] / eigN;
            a13 = v[4] / eigN;
            a22 = v[1] / eigN;
            a23 = v[5] / eigN;
            a33 = v[2] / eigN;

            p1 = a12 * a12 + a13 * a13 + a23 * a23;
            if (p1 == 0.0f)
            { // diagonal
                eig1 = a11;
                eig2 = a22;
                eig3 = a33;
            }
            else
            { // not diagonal
                q = (a11 + a22 + a33) / 3.0f;
                p2 = (a11 - q) * (a11 - q) + (a22 - q) * (a22 - q)
                        + (a33 - q) * (a33 - q) + 2.0f * p1;
                p = (float)Math.Sqrt(p2 / 6.0f);

                // B = (1/p)*(A-q*I)
                b11 = (a11 - q) / p;
                b12 = a12 / p;
                b13 = a13 / p;
                b22 = (a22 - q) / p;
                b23 = a23 / p;
                b33 = (a33 - q) / p;

                // r = det(B)/2;
                r = (b11 * (b22 * b33 - b23 * b23) - b12 * (b12 * b33 - b23 * b13)
                        + b13 * (b12 * b23 - b22 * b13)) / 2.0f;

                if (r <= -1.0f)
                    phi = (float)Math.PI / 3.0f;
                else if (r >= +1.0f)
                    phi = 0.0f;
                else
                    phi = (float)Math.Acos(r) / 3.0f;

                // eig3 <= eig2 <= eig1
                eig1 = q + 2.0f * p * ((float)Math.Cos(phi));
                eig3 = q + 2.0f * p * ((float)Math.Cos(phi + 2.0f * Math.PI / 3.0f));
                eig2 = 3.0f * q - eig1 - eig3; // tr(A) = eig1+eig2+eig3
            }
            ok = 1;
            if (eig3 > 0.0f)
                Rxyz[0] = (float)Math.Sqrt(1.0f / eig3);
            else
                ok = 0;
            if (eig2 > 0.0f)
                Rxyz[1] = (float)Math.Sqrt(1.0f / eig2);
            else
                ok = 0;
            if (eig1 > 0.0f)
                Rxyz[2] = (float)Math.Sqrt(1.0f / eig1);
            else
                ok = 0;
            return ok == 1;
        }


        static int rankreduce(float[] T)
        { // compute rank and reduce T to upper triangular matrix
            float k;
            int r, r2, c2;
            for (r = 0; r < 3; r++)
            {
                if ((k = T[r * 3 + r]) != 0.0f)
                {
                    for (c2 = 0; c2 < 3; c2++)
                        T[r * 3 + c2] /= k;
                }
                else
                {
                    for (r2 = r + 1; (r2 < 3) && ((k = T[r2 * 3 + r]) == 0.0f); r2++)
                        ;
                    if (k == 0.0f)
                        return r; // can not be inverted
                    for (c2 = 0; c2 < 3; c2++)
                        T[r * 3 + c2] += T[r2 * 3 + c2] / k;
                }
                for (r2 = r + 1; r2 < 3; r2++)
                {
                    k = -T[r2 * 3 + r];
                    for (c2 = 0; c2 < 3; c2++)
                        T[r2 * 3 + c2] += T[r * 3 + c2] * k;
                }
            }
            return 3;
        }


        static void UVcomplementS(float[] u, float[] v, float[] s, int o1, int o2, int o3)
        {
            float n;
            n = (float)Math.Sqrt(s[o3 + 0] * s[o3 + 0] + s[o3 + 1] * s[o3 + 1] + s[o3 + 2] * s[o3 + 2]);
            s[o3 + 0] /= n;
            s[o3 + 1] /= n;
            s[o3 + 2] /= n;
            if (Math.Abs(s[o3 + 0]) >= Math.Abs(s[o3 + 1]))
            {
                n = 1 / ((float)Math.Sqrt(s[o3 + 0] * s[o3 + 0] + s[o3 + 2] * s[o3 + 2]));
                u[o1 + 0] = -s[o3 + 2] * n;
                u[o1 + 1] = 0.0f;
                u[o1 + 2] = +s[o3 + 0] * n;
                v[o2 + 0] = +s[o3 + 1] * u[o1 + 2];
                v[o2 + 1] = +s[o3 + 2] * u[o1 + 0] - s[o3 + 0] * u[o1 + 2];
                v[o2 + 2] = -s[o3 + 1] * u[o1 + 0];
            }
            else
            {
                n = 1 / ((float)Math.Sqrt(s[o3 + 1] * s[o3 + 1] + s[o3 + 2] * s[o3 + 2]));
                u[o1 + 0] = 0.0f;
                u[o1 + 1] = +s[o3 + 2] * n;
                u[o1 + 2] = -s[o3 + 1] * n;
                v[o2 + 0] = +s[o3 + 1] * u[o1 + 2] - s[o3 + 2] * u[o1 + 1];
                v[o2 + 1] = -s[o3 + 0] * u[o1 + 2];
                v[o2 + 2] = +s[o3 + 0] * u[o1 + 1];
            }
        }

        static void SeqUcrossV(float[] s, float[] u, float[] v, int o1, int o2, int o3)
        {
            s[o1 + 0] = u[o2 + 1] * v[o3 + 2] - u[o2 + 2] * v[o3 + 1];
            s[o1 + 1] = u[o2 + 2] * v[o3 + 0] - u[o2 + 0] * v[o3 + 2];
            s[o1 + 2] = u[o2 + 0] * v[o3 + 1] - u[o2 + 1] * v[o3 + 0];
        }

        static void R3mtx_magcalib()
        { // must be called after gain_magcalib()
            float[] T3 = new float[3 * 3];
            float n;
            int rT;
            // A - eig1*eye(3)
            T3[0] = v[0] / eigN - eig3;
            T3[1] = v[3] / eigN;
            T3[2] = v[4] / eigN;
            T3[3] = T3[1];
            T3[4] = v[1] / eigN - eig3;
            T3[5] = v[5] / eigN;
            T3[6] = T3[2];
            T3[7] = T3[5];
            T3[8] = v[2] / eigN - eig3;
            rT = rankreduce(T3);
            if (rT == 0)
                return;  // init_magcalib() has already set R3mtx[] to eye(3)
            if (rT == 1)
            {
                UVcomplementS(R3mtx, R3mtx, T3, 0, 3, 0);
                SeqUcrossV(R3mtx, R3mtx, R3mtx, 6, 0, 3);
            }
            else
            { // (rT==2)
                SeqUcrossV(R3mtx, T3, T3, 0, 0, 3);

                n = (float)Math.Sqrt(R3mtx[0] * R3mtx[0] + R3mtx[1] * R3mtx[1] + R3mtx[2] * R3mtx[2]);
                R3mtx[0] /= n;
                R3mtx[1] /= n;
                R3mtx[2] /= n;
                // A - eig2*eye(3)
                T3[0] = v[0] / eigN - eig2;
                T3[1] = v[3] / eigN;
                T3[2] = v[4] / eigN;
                T3[3] = T3[1];
                T3[4] = v[1] / eigN - eig2;
                T3[5] = v[5] / eigN;
                T3[6] = T3[2];
                T3[7] = T3[5];
                T3[8] = v[2] / eigN - eig2;
                rT = rankreduce(T3);
                if (rT == 1)
                {
                    UVcomplementS(R3mtx, R3mtx, R3mtx, 3, 6, 0);
                }
                else
                { // (rt==2)
                    SeqUcrossV(R3mtx, T3, T3, 3, 0, 3);
                    n = (float)Math.Sqrt(R3mtx[3] * R3mtx[3] + R3mtx[4] * R3mtx[4] + R3mtx[5] * R3mtx[5]);
                    R3mtx[3] /= n;
                    R3mtx[4] /= n;
                    R3mtx[5] /= n;
                }
                SeqUcrossV(R3mtx, R3mtx, R3mtx, 6, 0, 3);
            }
        }

        // keep X,Y,Z axes if possible: largest values along diagonal, positive values
        static void R3mtx_gain_opt_magcalib()
        {
            int r, c, rm = 0, cm = 0;
            float t, m;
            m = 0.0f;
            for (r = 0; r < 3; r++)
                for (c = 0; c < 3; c++)
                    if ((t = (float)Math.Abs(R3mtx[r * 3 + c])) > m)
                    {
                        m = t;
                        rm = r;
                        cm = c;
                    }
            if (rm != cm)
            {
                for (c = 0; c < 3; c++)
                {
                    t = R3mtx[cm * 3 + c];
                    R3mtx[cm * 3 + c] = R3mtx[rm * 3 + c];
                    R3mtx[rm * 3 + c] = t;
                } // row cm <-> row rm
                t = Rxyz[cm];
                Rxyz[cm] = Rxyz[rm];
                Rxyz[rm] = t; // radii in sync with eigenvectors
            }
            // now decision to swap or not rows in remaining 2x2 matrix
            if (cm == 2)
            {
                m = (float)Math.Abs(R3mtx[0]);
                rm = 0;
                if ((t = (float)Math.Abs(R3mtx[1])) > m)
                {
                    m = t;
                    rm = 1;
                }
                if ((t = (float)Math.Abs(R3mtx[3])) > m)
                {
                    m = t;
                    rm = 1;
                }
                if (((float)Math.Abs(R3mtx[4])) > m)
                    rm = 0;
                if (rm > 0)
                {
                    for (c = 0; c < 3; c++)
                    {
                        t = R3mtx[c];
                        R3mtx[c] = R3mtx[3 + c];
                        R3mtx[3 + c] = t;
                    }
                    t = Rxyz[0];
                    Rxyz[0] = Rxyz[1];
                    Rxyz[1] = t;
                }
            }
            else if (cm == 1)
            {
                m = (float)Math.Abs(R3mtx[0]);
                rm = 0;
                if ((t = (float)Math.Abs(R3mtx[2])) > m)
                {
                    m = t;
                    rm = 1;
                }
                if ((t = (float)Math.Abs(R3mtx[6])) > m)
                {
                    m = t;
                    rm = 1;
                }
                if (((float)Math.Abs(R3mtx[8])) > m)
                    rm = 0;
                if (rm > 0)
                {
                    for (c = 0; c < 3; c++)
                    {
                        t = R3mtx[c];
                        R3mtx[c] = R3mtx[6 + c];
                        R3mtx[6 + c] = t;
                    }
                    t = Rxyz[0];
                    Rxyz[0] = Rxyz[2];
                    Rxyz[2] = t;
                }
            }
            else
            {
                m = (float)Math.Abs(R3mtx[4]);
                rm = 0;
                if ((t = (float)Math.Abs(R3mtx[5])) > m)
                {
                    m = t;
                    rm = 1;
                }
                if ((t = (float)Math.Abs(R3mtx[7])) > m)
                {
                    m = t;
                    rm = 1;
                }
                if (((float)Math.Abs(R3mtx[8])) > m)
                    rm = 0;
                if (rm > 0)
                {
                    for (c = 0; c < 3; c++)
                    {
                        t = R3mtx[3 + c];
                        R3mtx[3 + c] = R3mtx[6 + c];
                        R3mtx[6 + c] = t;
                    }
                    t = Rxyz[1];
                    Rxyz[1] = Rxyz[2];
                    Rxyz[2] = t;
                }
            }
            // positive along diagonal
            if (R3mtx[0] < 0.0f)
                for (c = 0; c < 3; c++)
                    R3mtx[c] = -R3mtx[c];
            if (R3mtx[4] < 0.0f)
                for (c = 0; c < 3; c++)
                    R3mtx[3 + c] = -R3mtx[3 + c];
            if (R3mtx[8] < 0.0f)
                for (c = 0; c < 3; c++)
                    R3mtx[6 + c] = -R3mtx[6 + c];
        }

        static public float REFR = 400.0f;

        static int init_remap_magcalib()
        {
            int r, c, r2, c2;
            float k;
            float[] TR3mtx = new float[3 * 3]; // iR3mtx[3*3], T[3*3];
            R3mtxR[0] = R3mtx[0] / Rxyz[0] * REFR;
            R3mtxR[1] = R3mtx[1] / Rxyz[0] * REFR;
            R3mtxR[2] = R3mtx[2] / Rxyz[0] * REFR;
            R3mtxR[3] = R3mtx[3] / Rxyz[1] * REFR;
            R3mtxR[4] = R3mtx[4] / Rxyz[1] * REFR;
            R3mtxR[5] = R3mtx[5] / Rxyz[1] * REFR;
            R3mtxR[6] = R3mtx[6] / Rxyz[2] * REFR;
            R3mtxR[7] = R3mtx[7] / Rxyz[2] * REFR;
            R3mtxR[8] = R3mtx[8] / Rxyz[2] * REFR;

            for (r = 0; r < 3; r++)
                for (c = 0; c < 3; c++)
                    TR3mtx[r + c * 3] = R3mtx[c + r * 3];
            for (r = 0; r < 3; r++)
            {
                if ((k = TR3mtx[r + r * 3]) != 0.0)
                    for (c2 = 0; c2 < 3; c2++)
                    {
                        TR3mtx[r + c2 * 3] /= k;
                        R3mtxR[c2 + r * 3] /= k;
                    }
                else
                {
                    for (r2 = r + 1; (r2 < 3) && ((k = TR3mtx[r2 + r * 3]) == 0.0); r2++)
                        ;
                    if (k == 0.0f)
                        return 0;
                    for (c2 = 0; c2 < 3; c2++)
                    {
                        TR3mtx[r + c2 * 3] += TR3mtx[r2 + c2 * 3] / k;
                        R3mtxR[c2 + r * 3] += R3mtxR[c2 + r2 * 3] / k;
                    }
                }
                for (r2 = 0; r2 < 3; r2++)
                    if (r2 != r)
                    {
                        k = -TR3mtx[r2 + r * 3];
                        for (c2 = 0; c2 < 3; c2++)
                        {
                            TR3mtx[r2 + c2 * 3] += TR3mtx[r + c2 * 3] * k;
                            R3mtxR[c2 + r2 * 3] += R3mtxR[c2 + r * 3] * k;
                        }
                    }
            }

            //printf("R3mtxR\n"); for(r=0;r<3;r++) { for(c=0;c<3;c++) printf("%10lf ",R3mtxR[c*3+r]); printf("\n"); }

            OOxyz[0] = 0.0f;
            OOxyz[1] = 0.0f;
            OOxyz[2] = 0.0f;
            for (c = 0; c < 3; c++)
            {
                OOxyz[0] += R3mtxR[0 + c] * Oxyz[c];
                OOxyz[1] += R3mtxR[3 + c] * Oxyz[c];
                OOxyz[2] += R3mtxR[6 + c] * Oxyz[c];
            }

            return 1;
        }

        static void remap_magcalib(float[] x, float[] y, float[] z)
        { // optimized, needs init_remap_magcalib()
            float tx, ty, tz;
            tx = x[0] - Oxyz[0];
            ty = y[0] - Oxyz[1];
            tz = z[0] - Oxyz[2]; // remove offset
            x[0] = tx * R3mtxR[0] + ty * R3mtxR[1] + tz * R3mtxR[2]; // rotate and scale to reference radius
            y[0] = tx * R3mtxR[3] + ty * R3mtxR[4] + tz * R3mtxR[5];
            z[0] = tx * R3mtxR[6] + ty * R3mtxR[7] + tz * R3mtxR[8];
        }


        static void save_stat_run_magcalib()
        {
            int r, c;
            for (r = 0; r < DOF; r++)
            {
                Dt1copy[r] = Dt1[r];
                for (c = 0; c < DOF; c++)
                    DtDcopy[r * DOF + c] = DtD[r * DOF + c];
            }
        }

        static void restore_stat_run_magcalib()
        {
            int r, c;
            for (r = 0; r < DOF; r++)
            {
                Dt1[r] = Dt1copy[r];
                for (c = 0; c < DOF; c++)
                    DtD[r * DOF + c] = DtDcopy[r * DOF + c];
            }
        }

        static public float[] getParameters()
        {
            float[] result = new float[12];
            for (int i = 0; i < 9; i++)
            {
                result[i] = R3mtxR[i];
            }
            //Compute the equivalent offset (M3txR*Oxyz)
            result[9] = OOxyz[0];
            result[10] = OOxyz[1];
            result[11] = OOxyz[2];
            return result;
        }

        static public bool runCalibration()
        {
            bool ok = solve_magcalib();
            solve_finish_magcalib();
            ok = ok && check_magcalib();
            ok = ok && offset_magcalib();
            ok = ok && gain_magcalib();
            if (ok)
            {
                R3mtx_magcalib(); // rotation matrix
                R3mtx_gain_opt_magcalib(); // optimize rotation/gain
                ok = ok && init_remap_magcalib() == 1; // now remap_mag_calib() can be used
            }
            return ok;
        }

        static public bool minimizationCalibration(List<float[]> list)
        {
            //Initilization
            Matrix x = new Matrix(9, 1);
            x[0, 0] = 1;
            x[1, 0] = 0;
            x[2, 0] = 0;
            x[3, 0] = 1;
            x[4, 0] = 0;
            x[5, 0] = 1;
            x[6, 0] = 0;
            x[7, 0] = 0;
            x[8, 0] = 0;

            //Compute reference magnitude
            float M = REFR;


            //Jacobian
            try
            {
                Matrix f = new Matrix(list.Count, 1);
                for (int i = 0; i < 5; i++)
                {
                    Matrix J = new Matrix(list.Count, 9);

                    for (int j = 0; j < list.Count; j++)
                    {
                        float[] acq = list[j];

                        f[j, 0] = M - (float)Math.Sqrt((acq[0] * x[0, 0] + acq[1] * x[1, 0] + acq[2] * x[2, 0] - x[6, 0]) * (acq[0] * x[0, 0] + acq[1] * x[1, 0] + acq[2] * x[2, 0] - x[6, 0]) + (acq[0] * x[1, 0] + acq[1] * x[3, 0] + acq[2] * x[4, 0] - x[7, 0]) * (acq[0] * x[1, 0] + acq[1] * x[3, 0] + acq[2] * x[4, 0] - x[7, 0]) + (acq[0] * x[2, 0] + acq[1] * x[4, 0] + acq[2] * x[5, 0] - x[8, 0]) * (acq[0] * x[2, 0] + acq[1] * x[4, 0] + acq[2] * x[5, 0] - x[8, 0]));

                        float argSum = (float)((acq[0] * x[0, 0] + acq[1] * x[1, 0] + acq[2] * x[2, 0] - x[6, 0]) * (acq[0] * x[0, 0] + acq[1] * x[1, 0] + acq[2] * x[2, 0] - x[6, 0])) + (float)((acq[0] * x[1, 0] + acq[1] * x[3, 0] + acq[2] * x[4, 0] - x[7, 0]) * (acq[0] * x[1, 0] + acq[1] * x[3, 0] + acq[2] * x[4, 0] - x[7, 0])) + (float)((acq[0] * x[2, 0] + acq[1] * x[4, 0] + acq[2] * x[5, 0] - x[8, 0]) * (acq[0] * x[2, 0] + acq[1] * x[4, 0] + acq[2] * x[5, 0] - x[8, 0]));


                        //Jacobian Computation
                        float arg1 = (float)(acq[0] * x[0, 0] + acq[1] * x[1, 0] + acq[2] * x[2, 0] - x[6, 0]);
                        float J1 = (float)(-0.5 / Math.Sqrt(argSum) * 2 * (arg1) * acq[0]);
                        float arg2 = (float)(acq[0] * x[1, 0] + acq[1] * x[3, 0] + acq[2] * x[4, 0] - x[7, 0]);
                        float J2 = (float)(-0.5 / Math.Sqrt(argSum) * (2 * (arg1) * acq[1] + 2 * (arg2) * acq[0]));
                        arg2 = (float)(acq[0] * x[2, 0] + acq[1] * x[4, 0] + acq[2] * x[5, 0] - x[8, 0]);
                        float J3 = (float)(-0.5 / Math.Sqrt(argSum) * (2 * (arg1) * acq[2] + 2 * (arg2) * acq[0]));


                        float arg = (float)(acq[0] * x[1, 0] + acq[1] * x[3, 0] + acq[2] * x[4, 0] - x[7, 0]);
                        float J4 = (float)(-0.5 / Math.Sqrt(argSum) * 2 * (arg) * acq[1]);
                        arg2 = (float)(acq[0] * x[2, 0] + acq[1] * x[4, 0] + acq[2] * x[5, 0] - x[8, 0]);
                        float J5 = (float)(-0.5 / Math.Sqrt(argSum) * (2 * (arg) * acq[2] + 2 * (arg2) * acq[1]));

                        arg = (float)(acq[0] * x[2, 0] + acq[1] * x[4, 0] + acq[2] * x[5, 0] - x[8, 0]);
                        float J6 = (float)(-0.5 / Math.Sqrt(argSum) * 2 * (arg) * acq[2]);

                        //Offset
                        arg = (float)(acq[0] * x[0, 0] + acq[1] * x[1, 0] + acq[2] * x[2, 0] - x[6, 0]);
                        float J7 = (float)(0.5 / Math.Sqrt(argSum) * 2 * (arg));
                        arg = (float)(acq[0] * x[1, 0] + acq[1] * x[3, 0] + acq[2] * x[4, 0] - x[7, 0]);
                        float J8 = (float)(0.5 / Math.Sqrt(argSum) * 2 * (arg));
                        arg = (float)(acq[0] * x[2, 0] + acq[1] * x[4, 0] + acq[2] * x[5, 0] - x[8, 0]);
                        float J9 = (float)(0.5 / Math.Sqrt(argSum) * 2 * (arg));

                        J[j, 0] = J1;
                        J[j, 1] = J2;
                        J[j, 2] = J3;
                        J[j, 3] = J4;
                        J[j, 4] = J5;
                        J[j, 5] = J6;
                        J[j, 6] = J7;
                        J[j, 7] = J8;
                        J[j, 8] = J9;
                    }
                    //Gauss-Newton Step
                    x = x - ((Matrix.Transpose(J) * J)).Invert() * Matrix.Transpose(J) * f;
                }
                R3mtxR[0] = (float)x[0, 0];
                R3mtxR[1] = (float)x[1, 0];
                R3mtxR[2] = (float)x[2, 0];
                R3mtxR[3] = (float)x[1, 0];
                R3mtxR[4] = (float)x[3, 0];
                R3mtxR[5] = (float)x[4, 0];
                R3mtxR[6] = (float)x[2, 0];
                R3mtxR[7] = (float)x[4, 0];
                R3mtxR[8] = (float)x[5, 0];
                OOxyz[0] = (float)x[6, 0];
                OOxyz[1] = (float)x[7, 0];
                OOxyz[2] = (float)x[8, 0];
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
    }
}
