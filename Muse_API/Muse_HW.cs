﻿using System;
using System.Collections.Generic;
/** 
* @file Muse_HW.cs
* 
* This file is part of Muse_API library.

* Muse_API is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.

* Muse_API is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.

* You should have received a copy of the GNU General Public License
* along with Nome-Programma.If not, see<http://www.gnu.org/licenses/>.

* Copyright (c) 2020 by 221e srl.
*/
namespace _221e.Muse
{
    /**
    * @brief Muse_HW class
    *
    * Hardware related communication protocol specifications
    *
    * @todo add some functionalities
    */
    public class Muse_HW
    {
        /// <summary>Overall message length [Bytes]</summary>
        public const int COMM_MESSAGE_LEN_CMD = 20;
        public const int COMM_MESSAGE_LEN_DAT = 128;

        /// <summary>Message Header length (2 Bytes)</summary>
        public const int COMM_MSG_HEADER_LEN_2 = 2;
        /// <summary>Message Header length (4 Bytes)</summary>
        public const int COMM_MSG_HEADER_LEN_4 = 4;
        /// <summary>Message Header length (8 Bytes)</summary>
        public const int COMM_MSG_HEADER_LEN_8 = 8;
        /// <summary>Message Header length (12 Bytes)</summary>
        public const int COMM_MSG_HEADER_LEN_12 = 12;

        /// <summary>Message Paybload length (12 Bytes)</summary>
        public const int COMM_PAYLOAD_LEN_12 = 12;
        /// <summary>Message Paybload length (16 Bytes)</summary>
        public const int COMM_PAYLOAD_LEN_16 = 16;
        /// <summary>Message Paybload length (18 Bytes)</summary>
        public const int COMM_PAYLOAD_LEN_18 = 18;

        /// <summary>Number of channels (1, e.g. Timestamp)</summary>
        public const int NUM_OF_CHANNELS_TIME = 1;

        /// <summary>Number of channels (3, e.g. Gyr, Axl, Mag)</summary>
        public const int NUM_OF_CHANNELS_3DOF = 3;              
        /// <summary>Number of channels (6, e.g. Gyr + Axl)</summary>
        public const int NUM_OF_CHANNELS_6DOF = 6;              
        /// <summary>Number of channels (9, e.g. Gyr + Axl + Mag)</summary>
        public const int NUM_OF_CHANNELS_9DOF = 9;

        /// <summary>Number of channels (3, e.g. imaginary part of quaternion)</summary>
        public const int NUM_OF_CHANNELS_QUAT = 3;

        /// <summary>Reference starting epoch: Monday 1 April 2019 08:00:00</summary>
        // public const UInt64 REFERENCE_EPOCH = 1554105600;
        public const UInt64 REFERENCE_EPOCH = 1580000000; //Sunday 26 January 2020 00:53:20

        /// <summary>Minimum log frequency [Hz]</summary>
        public const uint ACQ_LOG_FREQUENCY_MIN = 1;            
        /// <summary>Maximum log frequency [Hz]</summary>
        public const uint ACQ_LOG_FREQUENCY_MAX = 200;           

        /// <summary>Minimum stream frequency [Hz]</summary>
        public const uint ACQ_STREAM_FREQUENCY_MIN = 1;          
        /// <summary>Maximum stream frequency [Hz]</summary>
        public const uint ACQ_STREAM_FREQUENCY_MAX = 50;         

        public enum Command : byte
        {
            /// <summary>Acknowledge</summary>
            CMD_ACK = 0x00,
            /// <summary>Shutdown</summary>
            CMD_SHUTDOWN = 0x01,
            /// <summary>State [get/set]</summary>
            CMD_STATE = 0x02,
            /// <summary>Restart</summary>
            CMD_RESTART = 0x03,
            /// <summary>Application firmware information [readonly]</summary>
            CMD_APP_INFO = 0x04,
            /// <summary>Firmware Upload [<b>bootloader mode only</b>]</summary>
            CMD_FW_UPLOAD = 0x05,
            /// <summary>Start Application firmware</summary>
            CMD_START_APP = 0x06,
            /// <summary>Battery Charge [readonly]</summary>
            CMD_BATTERY_CHARGE = 0x07,
            /// <summary>Battery Voltage [readonly]</summary>
            CMD_BATTERY_VOLTAGE = 0x08,
            /// <summary>Device check up flag [readonly]</summary>
            CMD_CHECK_UP = 0x09,
            /// <summary>Installed Firmware Version [readonly]</summary>
            CMD_FW_VERSION = 0x0A,

            /// <summary>Current Time [get/set]</summary>
            CMD_TIME = 0x0B,
            /// <summary>Bluetooth Module Name [get/set]</summary>
            CMD_BLE_NAME = 0x0C,
            /// <summary>Hardware version [readonly]</summary>
            // CMD_HW_VERSION = 0x0D,
            /// <summary>Device Identification code [readonly]</summary>
            CMD_DEVICE_ID = 0x0E,

            /// <summary>Memory state [readonly]</summary>
            CMD_MEM_CONTROL = 0x20,         
            /// <summary>Get file information [readonly]</summary>
            CMD_MEM_FILE_INFO = 0x21,       
            /// <summary>File download</summary>
            CMD_MEM_FILE_DOWNLOAD = 0x22,

            /// <summary>Clock drift [get/set]</summary>
            // CMD_CLK_DRIFT = 0x30,           
            /// <summary>Clock offset [get/set]</summary>
            // CMD_CLK_OFFSET = 0x31,          
            /// <summary>Enter time sync mode</summary>
            // CMD_TIME_SYNC = 0x32,           
            /// <summary>Exit time sync mode</summary>
            // CMD_EXIT_TIME_SYNC = 0x33,

            // <summary>Sensors full scales [get/set]</summary>
            CMD_FS = 0xC0,
            CMD_SENSORS_FS = 0x40,
            // Config button log
            CMD_BTN_LOG = 0x50,
            // <summary>Sensors calibration matricies [get/set]</summary>
            CMD_CALIB_MATRIX = 0x48,

            //<summary> User configuration settings [get/set]</summary>
            CMD_CFG = 0x51,

            //<summary> Conditional log thresholds [get/set]</summary>
            CMD_CONDITIONAL_LOG_THRESHOLD = 0x61
        }

        public enum Acknowledge_Type : byte
        {
            /// <summary>Success</summary>
            ACK_SUCCESS = 0x00,                             
            /// <summary>Error</summary>
            ACK_ERROR = 0x01,                               
            /// <summary>Packet ok (bootloader only)</summary>
            ACK_FW_UPLOAD_PACKET_OK = 0x02,
            /// <summary>Packet refused error (bootloader only)</summary>
            ACK_FW_UPLOAD_PACKET_REFUSED = 0x03,
            /// <summary>Packet wrong id error (bootloader only)</summary>
            ACK_FW_UPLOAD_PACKET_WRONG_ID = 0x04		    
        }

        public enum SystemState : byte
        {
            /// <summary>System state NULL</summary>
            SYS_NULL = 0x00,
            /// <summary>Startup state [<b>bootloader mode only</b>]</summary>
            SYS_BOOT_STARTUP = 0xF0,
            /// <summary>Idle state [<b>bootloader mode only</b>]</summary>
            SYS_BOOT_IDLE = 0xF1,
            /// <summary>Write state [<b>bootloader mode only</b>]</summary>
            SYS_BOOT_WRITE = 0xF2,

            SYS_ERROR = 0xFF,       //!< SYS_ERROR
            SYS_STARTUP = 0x01,     //!< SYS_STARTUP
            SYS_IDLE = 0x02,        //!< SYS_RUN
            SYS_STANDBY = 0x03,     //!< SYS_STANDBY
            SYS_LOG = 0x04,         //!< SYS_LOG
            SYS_STREAMING = 0x06,   //!< SYS_STREAMING
            SYS_CALIB = 0x07        //!< SYS_CALIB
        }

        public enum RestartMode : byte
        {
            /// <summary>Restart device in application mode</summary>
            RESTART_RESET = 0x00,
            /// <summary>Restart device in bootloader mode</summary>
            RESTART_BOOT_LOADER = 0x01
        }

        public enum Acquisition_Type : UInt32
        {

            DATA_MODE_NONE = 0x00, //!< DATA_MODE_NONE
            DATA_MODE_GYRO = 0x01, //!< DATA_MODE_GYRO
            DATA_MODE_AXL = 0x02, //!< DATA_MODE_AXL
            DATA_MODE_IMU = DATA_MODE_AXL | DATA_MODE_GYRO, //!< DATA_MODE_GYRO
            DATA_MODE_MAGN = 0x04, //!< DATA_MODE_MAGN
            DATA_MODE_9DOF = DATA_MODE_MAGN | DATA_MODE_IMU, //!< DATA_MODE_9DOF
            DATA_MODE_HDR = 0x08, //!< DATA_MODE_HDR
            DATA_MODE_IMU_HDR = DATA_MODE_IMU | DATA_MODE_HDR, //!< DATA_MODE_IMU_HDR
            DATA_MODE_ORIENTATION = 0x10, //!< DATA_MODE_ORIENTATION
            DATA_MODE_IMU_HDR_TIMESTAMP = DATA_MODE_IMU_HDR | DATA_MODE_TIMESTAMP, //!< DATA_MODE_IMU_HDR_TIMESTAMP
            DATA_MODE_IMU_ORIENTATION = DATA_MODE_ORIENTATION | DATA_MODE_IMU, //!< DATA_MODE_IMU_ORIENTATION
            DATA_MODE_9DOF_ORIENTATION = DATA_MODE_9DOF | DATA_MODE_ORIENTATION, //!< DATA_MODE_9DOF_ORIENTATION
            DATA_MODE_TIMESTAMP = 0x20, //!< DATA_MODE_TIMESTAMP
            DATA_MODE_TEMP_HUM = 0x40,
            DATA_MODE_TEMP_PRESS = 0x80,
            DATA_MODE_RANGE = 0x0100,
            DATA_MODE_CONDITIONALLOG = 0x8000,
            DATA_MODE_ENCODER = 0x10000, //!< DATA_MODE_ENCODER
        }

        public enum DataView : UInt32
        {
            DATA_VIEW_NONE = 0,                                                 
            DATA_VIEW_AXL = 1,
            DATA_VIEW_GYRO = 2,
            DATA_VIEW_MAGN = 3,
            DATA_VIEW_HDR = 4,
            DATA_VIEW_ORIENTATION = 5,
            DATA_VIEW_TEMP = 6,
            DATA_VIEW_HUM = 7,
            DATA_VIEW_PRESS = 8,
            DATA_VIEW_ENCODER = 9
        }

        public enum AcquisitionPayloadSize : UInt32
        {

            DATA_SIZE_NONE = 0,
            DATA_SIZE_AXL = 6,
            DATA_SIZE_GYRO = 6,
            DATA_SIZE_MAGN = 6,
            DATA_SIZE_HDR = 6,
            DATA_SIZE_ORIENTATION = 6,
            DATA_SIZE_TIMESTAMP = 6,
            DATA_SIZE_TEMP_HUM = 6,
            DATA_SIZE_TEMP_PRESS = 6,
            DATA_SIZE_RANGE = 6,
            DATA_SIZE_ENCODER = 6

        }

        public enum AcquisitionFrequency : byte
        {
            DATA_FREQ_NONE = 0,          //!< DATA_FREQ_NONE    0x00
            DATA_FREQ_25Hz = 1,          //!< DATA_FREQ_25Hz    0x01
            DATA_FREQ_50Hz = 2,          //!< DATA_FREQ_50Hz    0x02
            DATA_FREQ_100Hz = 4,         //!< DATA_FREQ_100Hz   0x04
            DATA_FREQ_200Hz = 8,         //!< DATA_FREQ_200Hz   0x08
            DATA_FREQ_400Hz = 16,        //!< DATA_FREQ_400Hz   0x10
            DATA_FREQ_800Hz = 32,        //!< DATA_FREQ_800Hz   0x20
            DATA_FREQ_1600Hz = 64        //!< DATA_FREQ_1600Hz  0x40
        }

        public struct AcquisitionStamped
        {
            public UInt16 id;
            public string log_mode;
            public int log_frequency;
            public int fs;
            public uint file_mode;
            public List<Muse_Data> acquisitionList;
        }

        /**
         * Struct used to manage data
         */
        public struct Data_Typedef
        {
            public SystemState State { get; set; }              // Device state
            //public AcquisitionPayloadSize Mode { get; set; }    // Acquisition mode
            public AcquisitionFrequency Freq { get; set; }      // Acquisition frequency
        }

        // LSM6DSO
        // Gyroscope full scale
        public enum Gyroscope_FS : byte
        {
            GYR_FS_245dps = 0x00,
            GYR_FS_500dps = 0x01,
            GYR_FS_1000dps = 0x02,
            GYR_FS_2000dps = 0x03
        }
        public const float GYR_SENSITIVITY_245dps = 0.00875f;
        public const float GYR_SENSITIVITY_500dps = 0.0175f;
        public const float GYR_SENSITIVITY_1000dps = 0.035f;
        public const float GYR_SENSITIVITY_2000dps = 0.070f;

        // Accelerometer full scale
        public enum Accelerometer_FS : byte
        {
            AXL_FS_4g = 0x00,
            AXL_FS_32g = 0x04,
            AXL_FS_08g = 0x08,
            AXL_FS_16g = 0x0C
        }
        public const float AXL_SENSITIVITY_4g = 0.122f;
        public const float AXL_SENSITIVITY_8g = 0.244f;
        public const float AXL_SENSITIVITY_16g = 0.488f;
        public const float AXL_SENSITIVITY_32g = 0.976f;

        // LIS2MDL - Magnetometer
        public enum Magnetometer_FS : byte
        {
            MAG_FS_04G = 0x00,
            MAG_FS_08G = 0x40,
            MAG_FS_12G = 0x80,
            MAG_FS_16G = 0xC0
        }
        public const float MAG_SENSITIVITY_04G = 1000.0f/6842.0f;
        public const float MAG_SENSITIVITY_08G = 1000.0f/3421.0f;
        public const float MAG_SENSITIVITY_12G = 1000.0f/2281.0f;
        public const float MAG_SENSITIVITY_16G = 1000.0f/1711.0f;

        public const float MAG_TEMPERATURE = 0.125f;

        // H3LIS331
        // Accelerometer HDR full scale
        public enum Accelerometer_HDR_FS : byte
        {
            HDR_FS_100g = 0x00,
            HDR_FS_200g = 0x10,
            HDR_FS_400g = 0x30
        }
        public const float HDR_SENSITIVITY_100g = 49.0f;
        public const float HDR_SENSITIVITY_200g = 98.0f;
        public const float HDR_SENSITIVITY_400g = 195.0f;

        public struct FullScales
        {
            public float current_axl_resolution;
            public float current_gyr_resolution;
            public float current_mag_resolution;
            public float current_hdr_resolution;

            public string axl_fsStr;
            public string gyr_fsStr;
            public string mag_fsStr;
            public string hdr_fsStr;
        }

        public enum Calib_Type : byte
        { 
            CALIB_NONE = 0x00,                              //!< CALIB_NONE
            CALIB_GYR = 0x01,                               //!< CALIB_GYR
            CALIB_AXL = 0x02,                               //!< CALIB_AXL
            CALIB_MAG = 0x03,                               //!< CALIB_MAG
            CALIB_DIP = 0x04                                //!< CALIB_DIP
        }

        public enum Calibration_Type : byte 
        {
            CALIB_TYPE_GYR = 0,
            CALIB_TYPE_AXL = 1,
            CALIB_TYPE_MAG = 2
        }
    }
}
