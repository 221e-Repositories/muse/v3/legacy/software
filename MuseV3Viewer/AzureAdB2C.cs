﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MuseV3Viewer
{
    internal class AzureAdB2C
    {
        public static string ClientId { get; } = "a84e4af5-523c-4f6b-8ce4-8cabb318e16e";
        public static string TenantId { get; } = "organizations";
        //public static string RedirectUri { get; } = "https://alpinestars-my.sharepoint.com/personal/weshare_alpinestars_com/_layouts/15/onedrive.aspx?id=%2Fpersonal%2Fweshare%5Falpinestars%5Fcom%2FDocuments%2FARCHIVE%2FIT%2F221e&FolderCTID=0x01200047F3E8DFD0A5934F887042EBC7B32B27";
        public static string RedirectUri { get; } = "https://login.microsoftonline.com/common/oauth2/nativeclient";
        public static string Authority { get; } = "https://login.microsoftonline.com/" + TenantId;
        public static string Scope { get; } = "user.read";
    }
}
