﻿using System;
using System.Windows;
using System.Windows.Interop;
using System.Runtime.InteropServices;

namespace MuseV3Viewer
{
    /// <summary>
    /// Interaction logic for ThreeDView.xaml
    /// </summary>
    public partial class ThreeDViewWindow : Window
    {
        private const int GWL_STYLE = -16;
    private const int WS_SYSMENU = 0x80000;
    [DllImport("user32.dll", SetLastError = true)]
    private static extern int GetWindowLong(IntPtr hWnd, int nIndex);
    [DllImport("user32.dll")]
    private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

    private ThreeDViewModel mod_;

    public ThreeDViewWindow()
    {
        this.InitializeComponent();

        mod_ = new ThreeDViewModel();
        DataContext = mod_;
    }

    private void scvWindow_Loaded(object sender, RoutedEventArgs e)
    {
        // Hide CLOSE button
        var hwnd = new WindowInteropHelper(this).Handle;
        SetWindowLong(hwnd, GWL_STYLE, GetWindowLong(hwnd, GWL_STYLE) & ~WS_SYSMENU);
    }

    public ThreeDViewModel ViewModel
    {
        get { return mod_; }
        set { mod_ = value; }
    }
    
    }

}
