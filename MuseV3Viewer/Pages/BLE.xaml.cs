﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using _221e.Muse;
using Windows.Devices.Bluetooth;
using Windows.Devices.Bluetooth.GenericAttributeProfile;
using Windows.Devices.Enumeration;
using Windows.Storage.Streams;
using Windows.UI.Popups;


namespace MuseV3Viewer
{

    public class comboItem : INotifyPropertyChanged
    {
        public string mode { get; set; }

        private bool _ischecked;

        public bool ischecked
        {
            get
            {
                return _ischecked;
            }
            set
            {
                if (value != _ischecked)
                {
                    _ischecked = value;
                    OnPropertyChanged();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName = "")
        {
            if (this.PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    public partial class BLE : Window
    {

        // BLE Device enumeration variables
        private DeviceWatcher bleDeviceWatcher;
        public ObservableCollection<BluetoothLEDeviceDisplay> KnownDevices { get; set; }
        public List<DeviceInformation> UnknownDevices { get; set; }

        // Service and Characteristic collections
        public BluetoothLEDevice bluetoothLeDevice { get; set; }
        public ObservableCollection<BluetoothLEAttributeDisplay> ServiceCollection { get; set; }
        public ObservableCollection<BluetoothLEAttributeDisplay> CharacteristicCollection { get; set; }

        // BLE Network
        public ObservableCollection<BLENode> BLENetwork { get; set; }

        // Variables for Full Scales settings
        private Muse_HW.Accelerometer_FS selectedAxl_FS;
        private Muse_HW.Gyroscope_FS selectedGyr_FS;
        private Muse_HW.Magnetometer_FS selectedMag_FS;
        private Muse_HW.Accelerometer_HDR_FS selectedHDR_FS; 

        private Muse_HW.AcquisitionFrequency selectedLogFreq;

        public bool enable_standby;
        public bool standby_changed;
        public bool set_circular;
        public bool memory_changed;

        // Variable necessary to manage calibration type
        //public byte selectedMEMS = 0;


        public ObservableCollection<comboItem> comboMode { get; set; }
        private int modecount;

        public Muse_HW.Acquisition_Type SelectedAcquisitionMode { get; set; }

        public ObservableCollection<comboItem> comboAcqItems { get; set; }
        private int acqcount;

        public Muse_HW.Acquisition_Type SelectedAcquisitionData { get; set; }


        public static bool readFile;
        DateTime processedFileName = new DateTime();

        

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        public ThreeDViewWindow spaceChart_;
        public LineChartWindow gyroChart_;
        public LineChartWindow accChart_;
        public LineChartWindow accHDRChart_;
        public LineChartWindow magnChart_;

        public LineChartWindow eulerChart_;
        public LineChartWindow quatChart_;


        public BLE()
        {
            this.InitializeComponent();
            this.DataContext = this;

            PagesGateway.BlePg = this;

            // BLE Network
            KnownDevices = new ObservableCollection<BluetoothLEDeviceDisplay>();
            UnknownDevices = new List<DeviceInformation>();

            BLENetwork = new ObservableCollection<BLENode>();

            comboMode = new ObservableCollection<comboItem>();
            comboMode.Add(new comboItem { mode = "Gyroscope", ischecked = false });
            comboMode.Add(new comboItem { mode = "Accelerometer", ischecked = false });
            comboMode.Add(new comboItem { mode = "Magnetometer", ischecked = false });
            comboMode.Add(new comboItem { mode = "HDR Accelerometer", ischecked = false });
            comboMode.Add(new comboItem { mode = "Temperature & Relative Humidity", ischecked = false });
            comboMode.Add(new comboItem { mode = "Temperature & Ambient pressure", ischecked = false });
            comboMode.Add(new comboItem { mode = "Orientation", ischecked = false });
            comboMode.Add(new comboItem { mode = "Timestamp", ischecked = false });
            comboMode.Add(new comboItem { mode = "Conditional Log", ischecked = false });
            comboMode.Add(new comboItem { mode = "Encoder", ischecked = false });

            comboAcqItems = new ObservableCollection<comboItem>();
            comboAcqItems.Add(new comboItem { mode = "Gyroscope", ischecked = false });
            comboAcqItems.Add(new comboItem { mode = "Accelerometer", ischecked = false });
            comboAcqItems.Add(new comboItem { mode = "Magnetometer", ischecked = false });
            comboAcqItems.Add(new comboItem { mode = "HDR Accelerometer", ischecked = false });
            comboAcqItems.Add(new comboItem { mode = "Temperature & Relative Humidity", ischecked = false });
            comboAcqItems.Add(new comboItem { mode = "Temperature & Ambient pressure", ischecked = false });
            comboAcqItems.Add(new comboItem { mode = "Orientation", ischecked = false });
            comboAcqItems.Add(new comboItem { mode = "Timestamp", ischecked = false });
            comboAcqItems.Add(new comboItem { mode = "Conditional Log", ischecked = false });
            comboAcqItems.Add(new comboItem { mode = "Encoder", ischecked = false });

        }

        private void comboBox_ConnectedDeviced_DropDownOpened(object sender, EventArgs e)
        {

        }

        private void comboBox_ConnectedDeviced_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CBConnectedDevices.SelectedIndex = -1;
        }

        #region BLE DEVICE ENUMERATION

        // Start / Stop device enumeration
        public void EnumerateDevices()
        {
            if (bleDeviceWatcher == null)
            {
                StartBleDeviceWatcher();
            }
            else
            {
                StopBleDeviceWatcher();
            }
        }

        // Starts a device watcher that looks for all nearby Bluetooth devices (paired or unpaired). 
        // Attaches event handlers to populate the device collection.
        private void StartBleDeviceWatcher()
        {
            // Standard property strings: https://msdn.microsoft.com/en-us/library/windows/desktop/ff521659(v=vs.85).aspx
            string[] requestedProperties = { "System.Devices.Aep.DeviceAddress", "System.Devices.Aep.IsConnected", "System.Devices.Aep.Bluetooth.Le.IsConnectable", "System.DeviceInterface.Bluetooth.ServiceGuid", "System.Devices.Aep.IsPresent" };

            // Search for paired and non-paired devices in a single query
            string aqsAllBluetoothLEDevices = "(System.Devices.Aep.ProtocolId:=\"{bb7bb05e-5972-42b5-94fc-76eaa7084d49}\")";

            bleDeviceWatcher = DeviceInformation.CreateWatcher(
                                aqsAllBluetoothLEDevices,
                                requestedProperties,
                                DeviceInformationKind.AssociationEndpoint);

            // Register event handlers before starting the watcher
            bleDeviceWatcher.Added += BleDeviceWatcher_Added;
            bleDeviceWatcher.Updated += BleDeviceWatcher_Updated;
            bleDeviceWatcher.Removed += BleDeviceWatcher_Removed;
            bleDeviceWatcher.EnumerationCompleted += BleDeviceWatcher_EnumerationCompleted;
            bleDeviceWatcher.Stopped += BleDeviceWatcher_Stopped;

            // Clear device collection
            KnownDevices.Clear();
            UnknownDevices.Clear();

            // Start device watcher
            bleDeviceWatcher.Start();
        }

        // Stops watching for all nearby Bluetooth devices
        public void StopBleDeviceWatcher()
        {
            if (bleDeviceWatcher != null)
            {
                // Unregister the event handlers
                bleDeviceWatcher.Added -= BleDeviceWatcher_Added;
                bleDeviceWatcher.Updated -= BleDeviceWatcher_Updated;
                bleDeviceWatcher.Removed -= BleDeviceWatcher_Removed;
                bleDeviceWatcher.EnumerationCompleted -= BleDeviceWatcher_EnumerationCompleted;
                bleDeviceWatcher.Stopped -= BleDeviceWatcher_Stopped;

                // Stop device watcher
                bleDeviceWatcher.Stop();
                bleDeviceWatcher = null;
            }
        }

        // Manages KNOWN devices display
        private BluetoothLEDeviceDisplay FindBluetoothLEDeviceDisplay(string id)
        {
            foreach (BluetoothLEDeviceDisplay bleDeviceDisplay in KnownDevices)
            {
                if (bleDeviceDisplay.Id == id)
                {
                    return bleDeviceDisplay;
                }
            }
            return null;
        }

        // Manages UN-KNOWN devices display
        private DeviceInformation FindUnknownDevices(string id)
        {
            foreach (DeviceInformation bleDeviceInfo in UnknownDevices)
            {
                if (bleDeviceInfo.Id == id)
                {
                    return bleDeviceInfo;
                }
            }
            return null;
        }

        // Add device to the device list (i.e., observable collection) on the UI thread because the collection is databound to a UI element
        private async void BleDeviceWatcher_Added(DeviceWatcher sender, DeviceInformation deviceInfo)
        {
            await Dispatcher.InvokeAsync(() =>
             {
                 lock (this)
                 {
                     Debug.WriteLine(String.Format("Added {0}{1}", deviceInfo.Id, deviceInfo.Name));

                    // If the task runs after the app stopped the deviceWatcher, check for race condition
                    if (sender == bleDeviceWatcher)
                     {
                        // Make sure device isn't already present in the list
                        if (FindBluetoothLEDeviceDisplay(deviceInfo.Id) == null)
                         {
                             if (deviceInfo.Name != string.Empty)
                             {

                                // Display device friendly name, if available
                                KnownDevices.Add(new BluetoothLEDeviceDisplay(deviceInfo));
                             }
                             else
                             {
                                // Add it to a list in case the name gets updated later
                                UnknownDevices.Add(deviceInfo);
                             }
                         }

                     }
                 }
             }, System.Windows.Threading.DispatcherPriority.Normal);
        }

        // Updates the device list (i.e., observable collection) on the UI thread because the collection is databound to a UI element
        private async void BleDeviceWatcher_Updated(DeviceWatcher sender, DeviceInformationUpdate deviceInfoUpdate)
        {
            await Dispatcher.InvokeAsync(() =>
           {
               lock (this)
               {
                   Debug.WriteLine(String.Format("Updated {0}{1}", deviceInfoUpdate.Id, ""));

                    // If the task runs after the app stopped the deviceWatcher, check for race condition
                    if (sender == bleDeviceWatcher)
                   {
                       BluetoothLEDeviceDisplay bleDeviceDisplay = FindBluetoothLEDeviceDisplay(deviceInfoUpdate.Id);
                       if (bleDeviceDisplay != null)
                       {
                            // Device is already being displayed - update UX
                            bleDeviceDisplay.Update(deviceInfoUpdate);
                           return;
                       }

                       DeviceInformation deviceInfo = FindUnknownDevices(deviceInfoUpdate.Id);
                       if (deviceInfo != null)
                       {
                           deviceInfo.Update(deviceInfoUpdate);

                            // If device has been updated with a friendly name it's no longer unknown
                            if (deviceInfo.Name != String.Empty)
                           {
                               KnownDevices.Add(new BluetoothLEDeviceDisplay(deviceInfo));
                               UnknownDevices.Remove(deviceInfo);
                           }
                       }
                   }
               }
           }, System.Windows.Threading.DispatcherPriority.Normal);
        }

        // Removes device from the device list (i.e., observable collection) on the UI thread because the collection is databound to a UI element
        private async void BleDeviceWatcher_Removed(DeviceWatcher sender, DeviceInformationUpdate deviceInfoUpdate)
        {
            await Dispatcher.InvokeAsync(() =>
            {
                lock (this)
                {
                    Debug.WriteLine(String.Format("Removed {0}{1}", deviceInfoUpdate.Id, ""));

                    // If the task runs after the app stopped the deviceWatcher, check for race condition
                    if (sender == bleDeviceWatcher)
                    {
                        // Find the corresponding DeviceInformation in the collection and remove it
                        BluetoothLEDeviceDisplay bleDeviceDisplay = FindBluetoothLEDeviceDisplay(deviceInfoUpdate.Id);
                        if (bleDeviceDisplay != null)
                        {
                            KnownDevices.Remove(bleDeviceDisplay);
                        }

                        DeviceInformation deviceInfo = FindUnknownDevices(deviceInfoUpdate.Id);
                        if (deviceInfo != null)
                        {
                            UnknownDevices.Remove(deviceInfo);
                        }
                    }
                }
            }, System.Windows.Threading.DispatcherPriority.Normal);
        }

        // Manage "ENUMERATION COMPLETED" event
        private async void BleDeviceWatcher_EnumerationCompleted(DeviceWatcher sender, object e)
        {
            await Dispatcher.InvokeAsync(() =>
           {
               if (sender == bleDeviceWatcher)
               {
                   //rootPage.NotifyUser($"{KnownDevices.Count} devices found. Enumeration completed.",
                   //     NotifyType.StatusMessage);
               }
           }, System.Windows.Threading.DispatcherPriority.Normal);
        }

        // Stop device watcher
        private async void BleDeviceWatcher_Stopped(DeviceWatcher sender, object e)
        {
            await Dispatcher.InvokeAsync(() =>
            {
                if (sender == bleDeviceWatcher)
                {
                    // rootPage.NotifyUser($"No longer watching for devices.",
                    //         sender.Status == DeviceWatcherStatus.Aborted ? NotifyType.ErrorMessage : NotifyType.StatusMessage);
                }
            }, System.Windows.Threading.DispatcherPriority.Normal);
        }

        // Show device list
        private void ResultsListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        #endregion

        #region BLE DEVICE CONNECTION

        // Connect selected device
        public async Task<bool> ConnectBleDevice(List<BluetoothLEDeviceDisplay> selectedDevices)
        {

            try
            {
                // Stop device watcher during device connection
                StopBleDeviceWatcher();

                // BluetoothLEDevice.FromIdAsync must be called from a UI thread because it may prompt for consent.
                foreach (var item in selectedDevices)
                {
                    bluetoothLeDevice = null;
                    ServiceCollection = null;
                    CharacteristicCollection = null;

                    var bleDeviceDisplay = item as BluetoothLEDeviceDisplay;
                    if (bleDeviceDisplay != null)
                        bluetoothLeDevice = await BluetoothLEDevice.FromIdAsync(bleDeviceDisplay.Id);

                    if (bluetoothLeDevice != null)
                    {
                        // Note: BluetoothLEDevice.GattServices property will return an empty list for unpaired devices. For all uses we recommend using the GetGattServicesAsync method.
                        // BT_Code: GetGattServicesAsync returns a list of all the supported services of the device (even if it's not paired to the system).
                        // If the services supported by the device are expected to change during BT usage, subscribe to the GattServicesChanged event.
                        GattDeviceServicesResult result = await bluetoothLeDevice.GetGattServicesAsync(BluetoothCacheMode.Uncached);

                        if (result.Status == GattCommunicationStatus.Success)
                        {
                            ServiceCollection = new ObservableCollection<BluetoothLEAttributeDisplay>();
                            foreach (var service in result.Services)
                                ServiceCollection.Add(new BluetoothLEAttributeDisplay(service));

                            if (ServiceCollection.Count >= 3 && ServiceCollection[2].service.Uuid.ToString().Equals("c8c0a708-e361-4b5e-a365-98fa6b0a836f"))
                            {
                                // Select the 3rd element of the services list by default
                                CharacteristicCollection = await BLENode.GetCharacteristics(ServiceCollection[2].service);
                                if (CharacteristicCollection.Count == 1)
                                {
                                    // Select only command characteristic (BOOTLOADER MODE)
                                    BLENetwork.Add(new BLENode(bluetoothLeDevice, ServiceCollection, CharacteristicCollection));
                                }
                                else if (CharacteristicCollection.Count == 2)
                                {
                                    // Select the first characteristic for command and the second for data (APP / NORMAL MODE)
                                    BLENetwork.Add(new BLENode(bluetoothLeDevice, ServiceCollection, CharacteristicCollection));
                                   
                                }
                            }
                        }
                    }
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        // Disconnect

        public async Task<bool> DisconnectBleDevice(List<BluetoothLEDeviceDisplay> selectedDevices)
        {

            try
            {
                // Stop device watcher during device connection
                //StopBleDeviceWatcher();

                // BluetoothLEDevice.FromIdAsync must be called from a UI thread because it may prompt for consent.
                foreach (var item in selectedDevices)
                {

                    var bleDeviceDisplay = item as BluetoothLEDeviceDisplay;
                    if (bleDeviceDisplay != null)
                        bluetoothLeDevice = await BluetoothLEDevice.FromIdAsync(bleDeviceDisplay.Id);

                    if (bluetoothLeDevice != null)
                    {
                        GattDeviceServicesResult result = await bluetoothLeDevice.GetGattServicesAsync(BluetoothCacheMode.Uncached);

                        if (result.Status == GattCommunicationStatus.Success)
                        {
                            ServiceCollection = new ObservableCollection<BluetoothLEAttributeDisplay>();
                            foreach (var service in result.Services)
                                ServiceCollection.Add(new BluetoothLEAttributeDisplay(service));

                            if (ServiceCollection.Count >= 3 && ServiceCollection[2].service.Uuid.ToString().Equals("c8c0a708-e361-4b5e-a365-98fa6b0a836f"))
                            {
                                //Select the 3rd element of the services list by default
                                //CharacteristicCollection = await BLENode.GetCharacteristics(ServiceCollection[2].service);
                                //if (CharacteristicCollection.Count > 0)
                                //{
                                //    //Select the first characteristic for command and the second for data(APP / NORMAL MODE)

                                //    BLENetwork.Remove(new BLENode(bluetoothLeDevice, ServiceCollection, CharacteristicCollection));

                                //    foreach (var characteristic in CharacteristicCollection)
                                //    {
                                //        await characteristic.characteristic.WriteClientCharacteristicConfigurationDescriptorAsync(GattClientCharacteristicConfigurationDescriptorValue.None);

                                //        characteristic?.service?.Session?.Dispose();
                                //        characteristic?.service?.Dispose();
                                //        characteristic?.service = null;
                                //    }
                                //}

                                foreach (var service in ServiceCollection)
                                {
                                    service.service?.Dispose();
                                }

                                bluetoothLeDevice?.Dispose();
                                bluetoothLeDevice = null;


                            }
                        }



                    }

                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        #endregion


        private void ButtonFechar_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void btnMaximize_Click(object sender, RoutedEventArgs e)
        {
            switch (this.WindowState)
            {
                case WindowState.Normal:
                    this.WindowState = WindowState.Maximized;
                    break;
                case WindowState.Maximized:
                    this.WindowState = WindowState.Normal;
                    break;
            }
        }

        private void btnMinimize_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void GridBarraTitulo_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private async void Usb_Click(object sender, MouseButtonEventArgs e)
        {
            StopBleDeviceWatcher();

            KnownDevices.Clear();
            UnknownDevices.Clear();

            BLENetwork.Clear();

            // Clear all fields

            comboMode.Clear();
            comboMode.Add(new comboItem { mode = "Gyroscope", ischecked = false });
            comboMode.Add(new comboItem { mode = "Accelerometer", ischecked = false });
            comboMode.Add(new comboItem { mode = "Magnetometer", ischecked = false });
            comboMode.Add(new comboItem { mode = "HDR Accelerometer", ischecked = false });
            comboMode.Add(new comboItem { mode = "Temperature & Relative Humidity", ischecked = false });
            comboMode.Add(new comboItem { mode = "Temperature & Ambient pressure", ischecked = false });
            comboMode.Add(new comboItem { mode = "Orientation", ischecked = false });
            comboMode.Add(new comboItem { mode = "Timestamp", ischecked = false });
            comboMode.Add(new comboItem { mode = "Conditonal Log", ischecked = false });
            comboMode.Add(new comboItem { mode = "Encoder", ischecked = false });
            CBLogMode.Text = "Data mode";

            CBLogFreq.Text = "Freq";
            SelectedAcquisitionMode = Muse_HW.Acquisition_Type.DATA_MODE_NONE;


            comboAcqItems.Clear();
            comboAcqItems.Add(new comboItem { mode = "Gyroscope", ischecked = false });
            comboAcqItems.Add(new comboItem { mode = "Accelerometer", ischecked = false });
            comboAcqItems.Add(new comboItem { mode = "Magnetometer", ischecked = false });
            comboAcqItems.Add(new comboItem { mode = "HDR Accelerometer", ischecked = false });
            comboAcqItems.Add(new comboItem { mode = "Temperature & Relative Humidity", ischecked = false });
            comboAcqItems.Add(new comboItem { mode = "Temperature & Ambient pressure", ischecked = false });
            comboAcqItems.Add(new comboItem { mode = "Orientation", ischecked = false });
            comboAcqItems.Add(new comboItem { mode = "Timestamp", ischecked = false });
            comboAcqItems.Add(new comboItem { mode = "Conditional Log", ischecked = false });
            comboAcqItems.Add(new comboItem { mode = "Encoder", ischecked = false });
            CBAcquisitionData.Text = "Data mode";

            CBAcquisitionFreq.Text = "Freq";
            SelectedAcquisitionData = Muse_HW.Acquisition_Type.DATA_MODE_NONE;

            USB win2 = new USB();           
            win2.Show();
            win2.WindowState = this.WindowState;
            this.Close();

        }

        #region CLICK BUTTONS

        private void BtnEnumerate_Click(object sender, RoutedEventArgs e)
        {
            EnumerateDevices();
        }

        private async void BtnConnect_Click(object sender, RoutedEventArgs e)
        {
            if (CBConnectedDevices.SelectedItems.Count > 0)
            {
                //Add selected device to connectable list
                List<BluetoothLEDeviceDisplay> tmp = new List<BluetoothLEDeviceDisplay>();
                foreach (var item in CBConnectedDevices.SelectedItems)
                    tmp.Add(item as BluetoothLEDeviceDisplay);

                // Device connection management
                bool result = await ConnectBleDevice(tmp);

                if (LV_NetworkNodes != null)
                {
                    LV_NetworkNodes.SelectAll();

                    if (LV_NetworkNodes.SelectedItems.Count == 1)
                    {
                        byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                        msg = Muse_Utils.Cmd_GetUserConfiguration();
                        BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);

                        updateStatus();
                        UpdateLogFiles();
                    }
                }
            }


        }

        public async void updateStatus()
        {
            if (CBConnectedDevices.SelectedItems.Count > 0)
            {
                //Add selected device to connectable list
                List<BluetoothLEDeviceDisplay> tmp = new List<BluetoothLEDeviceDisplay>();
                foreach (var item in CBConnectedDevices.SelectedItems)
                    tmp.Add(item as BluetoothLEDeviceDisplay);

                // Device connection management
                bool result = await ConnectBleDevice(tmp);

                if (LV_NetworkNodes != null)
                {
                    if (LV_NetworkNodes.SelectedItems.Count == 1)
                    {
                        byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];

                        msg = Muse_Utils.Cmd_GetState();
                        await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);

                        msg = Muse_Utils.Cmd_BatteryCharge();
                        await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);

                        msg = Muse_Utils.Cmd_BatteryVoltage();
                        await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);

                        msg = Muse_Utils.Cmd_FwVersion();
                        await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);

                        msg = Muse_Utils.Cmd_DeviceId();
                        await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);

                        msg = Muse_Utils.Cmd_GetTime();
                        await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);
                    }
                }
            }
        }

        private async void BtnResetAll_Click(object sender, RoutedEventArgs e)
        {
            // TODO: review reset all method

            List<BluetoothLEDeviceDisplay> tmp = new List<BluetoothLEDeviceDisplay>();
            foreach (var item in CBConnectedDevices.SelectedItems)
                tmp.Add(item as BluetoothLEDeviceDisplay);

            // Device connection management
            bool result = await DisconnectBleDevice(tmp);

            StopBleDeviceWatcher();

            KnownDevices.Clear();
            UnknownDevices.Clear();

            BLENetwork.Clear();

            // Clear all fieds

            comboMode.Clear();
            comboMode.Add(new comboItem { mode = "Gyroscope", ischecked = false });
            comboMode.Add(new comboItem { mode = "Accelerometer", ischecked = false });
            comboMode.Add(new comboItem { mode = "Magnetometer", ischecked = false });
            comboMode.Add(new comboItem { mode = "HDR Accelerometer", ischecked = false });
            comboMode.Add(new comboItem { mode = "Temperature & Relative Humidity", ischecked = false });
            comboMode.Add(new comboItem { mode = "Temperature & Ambient pressure", ischecked = false });
            comboMode.Add(new comboItem { mode = "Orientation", ischecked = false });
            comboMode.Add(new comboItem { mode = "Timestamp", ischecked = false });
            comboMode.Add(new comboItem { mode = "Conditional Log", ischecked = false });
            comboMode.Add(new comboItem { mode = "Encoder", ischecked = false });
            CBLogMode.Text = "Data mode";

            CBLogFreq.Text = "Freq";
            SelectedAcquisitionMode = Muse_HW.Acquisition_Type.DATA_MODE_NONE;

            comboAcqItems.Clear();
            comboAcqItems.Add(new comboItem { mode = "Gyroscope", ischecked = false });
            comboAcqItems.Add(new comboItem { mode = "Accelerometer", ischecked = false });
            comboAcqItems.Add(new comboItem { mode = "Magnetometer", ischecked = false });
            comboAcqItems.Add(new comboItem { mode = "HDR Accelerometer", ischecked = false });
            comboAcqItems.Add(new comboItem { mode = "Temperature & Relative Humidity", ischecked = false });
            comboAcqItems.Add(new comboItem { mode = "Temperature & Ambient pressure", ischecked = false });
            comboAcqItems.Add(new comboItem { mode = "Orientation", ischecked = false });
            comboAcqItems.Add(new comboItem { mode = "Timestamp", ischecked = false });
            comboAcqItems.Add(new comboItem { mode = "Conditional Log", ischecked = false });
            comboAcqItems.Add(new comboItem { mode = "Encoder", ischecked = false });
            CBAcquisitionData.Text = "Data mode";

            CBAcquisitionFreq.Text = "Freq";
            SelectedAcquisitionData = Muse_HW.Acquisition_Type.DATA_MODE_NONE;

            LblCurrentState.Text = "---";
            LblBatteryCharge.Text = "---";
            LblBatteryVoltage.Text = "---";
            LblFirmwareVersion.Text = "---";
            LblDeviceId.Text = "---"; 
            LblDateTime.Text = "---";

        }

        private async void BtnWrite_Click(object sender, RoutedEventArgs e)
        {
            if (LV_NetworkNodes.Items.Count > 0)
            {
                if (LV_NetworkNodes.SelectedItems.Count == 1)
                {
                    string str = LblWrite.Text;

                    if (str.Length >= 4)
                    {
                        if (str.ToCharArray().Count(c => c == ' ') == 0)
                            str = str.ToCharArray().Aggregate("", (result, c) => result += ((!string.IsNullOrEmpty(result) && (result.Length + 1) % 3 == 0) ? " " : "") + c.ToString());

                        string[] splitStr = str.Split(' ');

                        int len = splitStr.Length;

                        byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];

                        if (str != "" && len > 0 && len <= 20)
                        {
                            for (int i = 0; i < len; i++)
                            {
                                int value = Convert.ToInt32(splitStr[i], 16);
                                msg[i] = Convert.ToByte(value);
                            }

                            await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);
                        }

                        if (msg[2] == 0x06)
                            BLENetwork[LV_NetworkNodes.SelectedIndex].acquisitionStreamEnabled = true;
                    }
                    else
                    {
                        System.Windows.Forms.MessageBox.Show("At least 4 bytes are required to form a Muse V3 command", "Attention!", MessageBoxButtons.OK);
                    }


                }
                else
                {
                    // check your selection!
                }

            }

        }

        private async void BtnGetState_Click(object sender, RoutedEventArgs e)
        {
            if (LV_NetworkNodes.Items.Count > 0)
            {
                if (LV_NetworkNodes.SelectedItems.Count == 1)
                {
                    byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                    msg = Muse_Utils.Cmd_GetState();

                    await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);
                }
                else
                {
                    // check your selection!
                }

            }

        }

        private async void BtnGetBatteryCharge_Click(object sender, RoutedEventArgs e)
        {
            if (LV_NetworkNodes.Items.Count > 0)
            {
                if (LV_NetworkNodes.SelectedItems.Count == 1)
                {
                    byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                    msg = Muse_Utils.Cmd_BatteryCharge();

                    await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);
                }
                else
                {
                    // check your selection!
                }

            }
        }

        private async void BtnGetBatteryVoltage_Click(object sender, RoutedEventArgs e)
        {
            if (LV_NetworkNodes.Items.Count > 0)
            {
                if (LV_NetworkNodes.SelectedItems.Count == 1)
                {
                    byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                    msg = Muse_Utils.Cmd_BatteryVoltage();

                    await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);
                }
                else
                {
                    // check your selection!
                }

            }

        }

        private async void BtnGetFirmwareVersion_Click(object sender, RoutedEventArgs e)
        {
            if (LV_NetworkNodes.Items.Count > 0)
            {
                if (LV_NetworkNodes.SelectedItems.Count == 1)
                {
                    byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                    msg = Muse_Utils.Cmd_FwVersion();

                    await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);
                }
                else
                {
                    // check your selection!
                }

            }

        }

        private async void BtnGetCheckUp_Click(object sender, RoutedEventArgs e)
        {
            if (LV_NetworkNodes.Items.Count > 0)
            {
                if (LV_NetworkNodes.SelectedItems.Count == 1)
                {
                    byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                    msg = Muse_Utils.Cmd_Checkup();

                    await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);
                }
                else
                {
                    // check your selection!
                }

            }

        }

        private async void BtnGetDeviceId_Click(object sender, RoutedEventArgs e)
        {
            if (LV_NetworkNodes.Items.Count > 0)
            {
                if (LV_NetworkNodes.SelectedItems.Count == 1)
                {
                    byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                    msg = Muse_Utils.Cmd_DeviceId();

                    await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);
                }
                else
                {
                    // check your selection!
                }

            }

        }

        private async void BtnGetDateTime_Click(object sender, RoutedEventArgs e)
        {
            if (LV_NetworkNodes.Items.Count > 0)
            {
                if (LV_NetworkNodes.SelectedItems.Count == 1)
                {
                    byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                    msg = Muse_Utils.Cmd_GetTime();

                    await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);
                }
                else
                {
                    // check your selection!
                }

            }

        }

        private async void BtnSetDateTime_Click(object sender, RoutedEventArgs e)
        {
            if (LV_NetworkNodes.Items.Count > 0)
            {
                if (LV_NetworkNodes.SelectedItems.Count == 1)
                {
                    byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];

                    TimeSpan timeSpan = DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
                    uint secondsSinceEpoch = (uint)timeSpan.TotalSeconds;

                    msg = Muse_Utils.Cmd_SetTime(secondsSinceEpoch);
                    await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);

                    msg = Muse_Utils.Cmd_GetTime();
                    await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);
                }
                else
                {
                    // check your selection!
                }

            }

        }

        private async void BtnGetBLEName_Click(object sender, RoutedEventArgs e)
        {
            if (LV_NetworkNodes.Items.Count > 0)
            {
                if (LV_NetworkNodes.SelectedItems.Count == 1)
                {
                    byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                    msg = Muse_Utils.Cmd_BLE_GetName();
                    await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);
                }
                else
                {
                    // check your selection!
                }

            }

        }

        private async void BtnSetBLEName_Click(object sender, RoutedEventArgs e)
        {
            if (LV_NetworkNodes.Items.Count > 0)
            {
                if (LV_NetworkNodes.SelectedItems.Count == 1)
                {
                    byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                    string bleName = LblNewBLEName.Text;
                    msg = Muse_Utils.Cmd_BLE_SetName(bleName);
                    await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);
                }
                else
                {
                    // check your selection!
                }

            }

        }



        private void LV_NetworkNodes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }


        private async void BtnCalibrate_Click(object sender, RoutedEventArgs e)
        {
            bool success = await ManageMEMSCalibration();
            if (success)
            {
                if (LV_NetworkNodes.Items.Count > 0)
                {
                    if (LV_NetworkNodes.SelectedItems.Count == 1)
                    {
                        if (BLENetwork[LV_NetworkNodes.SelectedIndex].calibChX != null && BLENetwork[LV_NetworkNodes.SelectedIndex].calibChY != null && BLENetwork[LV_NetworkNodes.SelectedIndex].calibChZ != null)
                        {
                            int stepCounter = BLENetwork[LV_NetworkNodes.SelectedIndex].calibChX.Count;
                            for (int i = 0; i < stepCounter; i++)
                            {
                                Calibration.run_magcalib(BLENetwork[LV_NetworkNodes.SelectedIndex].calibChX[i], BLENetwork[LV_NetworkNodes.SelectedIndex].calibChY[i], BLENetwork[LV_NetworkNodes.SelectedIndex].calibChZ[i]);
                            }

                            if (Calibration.runCalibration())
                            {
                                BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult = Calibration.getParameters();

                                var title = "Device Calibration Terminated";
                                var uploadCommand = new UICommand("Upload", cmd => { });
                                var saveCommand = new UICommand("Save", cmd => { });
                                var cancelCommand = new UICommand("Cancel", cmd => { });

                                var content = String.Format("{0} {1} {2} {3}\n{4} {5} {6} {7}\n{8} {9} {10} {11}",
                                     BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult[0], BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult[3], BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult[6], BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult[9],
                                     BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult[1], BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult[4], BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult[7], BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult[10],
                                     BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult[2], BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult[5], BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult[8], BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult[11]);

                                var dialog = new MessageDialog(content, title);
                                dialog.Options = MessageDialogOptions.None;
                                dialog.Commands.Add(uploadCommand);
                                dialog.Commands.Add(saveCommand);
                                dialog.Commands.Add(cancelCommand);

                                dialog.DefaultCommandIndex = 0;
                                dialog.CancelCommandIndex = 0;

                                var command = await dialog.ShowAsync();

                                if (command == uploadCommand)
                                {
                                    // Upload calibration results to device.
                                    // The upload routine stars here, then it is managed on MainPage.xaml.cs through the cmd update characteristic
                                    byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                                    msg = Muse_Utils.Cmd_SetCalibrationMatrix((byte)calibrationIndex, 0, BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult[0], BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult[1], BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult[2]);

                                    BLENetwork[LV_NetworkNodes.SelectedIndex].enableUploadCalibration = true;
                                    BLENetwork[LV_NetworkNodes.SelectedIndex].calibColCounter++;

                                    await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);
                                }
                                else if (command == saveCommand)
                                {
                                    // Save calibration results locally (.txt file)
                                    var savePicker = new Windows.Storage.Pickers.FileSavePicker();
                                    savePicker.SuggestedStartLocation =
                                        Windows.Storage.Pickers.PickerLocationId.DocumentsLibrary;
                                    // Dropdown of file types the user can save the file as
                                    savePicker.FileTypeChoices.Add("Plain Text", new List<string>() { ".txt" });
                                    // Default file name if the user does not type one in or select a file to replace
                                    savePicker.SuggestedFileName = "New Document";

                                    Windows.Storage.StorageFile file = await savePicker.PickSaveFileAsync();
                                    if (file != null)
                                    {
                                        // Prevent updates to the remote version of the file until
                                        // we finish making changes and call CompleteUpdatesAsync.
                                        Windows.Storage.CachedFileManager.DeferUpdates(file);

                                        // write to file
                                        await Windows.Storage.FileIO.WriteTextAsync(file, content);
                                        // Let Windows know that we're finished changing the file so
                                        // the other app can update the remote version of the file.
                                        // Completing updates may require Windows to ask for user input.
                                        Windows.Storage.Provider.FileUpdateStatus status =
                                            await Windows.Storage.CachedFileManager.CompleteUpdatesAsync(file);
                                    }

                                    command = await dialog.ShowAsync();
                                }
                                else
                                {
                                    // Cancel command
                                    // Reset calibration variables and results
                                    BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult = null;
                                    BLENetwork[LV_NetworkNodes.SelectedIndex].calibChX = null;
                                    BLENetwork[LV_NetworkNodes.SelectedIndex].calibChY = null;
                                    BLENetwork[LV_NetworkNodes.SelectedIndex].calibChZ = null;

                                }
                            }
                        }

                    }
                    else
                    {
                        // check your selection!
                    }

                }


            }
        }

        private async void BtnGetCalib_Click(object sender, RoutedEventArgs e)
        {
            if (LV_NetworkNodes.Items.Count > 0)
            {
                if (LV_NetworkNodes.SelectedItems.Count == 1)
                {
                    byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];

                    msg = Muse_Utils.Cmd_GetCalibrationMatrix((byte)calibrationIndex, 0);

                    BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult = new float[12];

                    BLENetwork[LV_NetworkNodes.SelectedIndex].enableDownloadCalibration = true;
                    BLENetwork[LV_NetworkNodes.SelectedIndex].calibColCounter++;

                    await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);
                }
                else
                {
                    // check your selection!
                }

            }

        }

        private async void BtnResetCalib_Click(object sender, RoutedEventArgs e)
        {
            if (LV_NetworkNodes.Items.Count > 0)
            {
                if (LV_NetworkNodes.SelectedItems.Count == 1)
                {
                    BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult = new float[12] { 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0 };

                    var title = "Device Calibration Reset";
                    var uploadCommand = new UICommand("Upload", cmd => { });
                    var saveCommand = new UICommand("Save", cmd => { });
                    var cancelCommand = new UICommand("Cancel", cmd => { });

                    var content = String.Format("{0} {1} {2} {3}\n{4} {5} {6} {7}\n{8} {9} {10} {11}",
                        BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult[0], BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult[3], BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult[6], BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult[9],
                        BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult[1], BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult[4], BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult[7], BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult[10],
                        BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult[2], BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult[5], BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult[8], BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult[11]);

                    var dialog = new MessageDialog(content, title);
                    dialog.Options = MessageDialogOptions.None;
                    dialog.Commands.Add(uploadCommand);
                    dialog.Commands.Add(cancelCommand);

                    dialog.DefaultCommandIndex = 0;
                    dialog.CancelCommandIndex = 0;

                    var command = await dialog.ShowAsync();

                    if (command == uploadCommand)
                    {
                        // Upload calibration results to device.
                        // The upload routine stars here, then it is managed on MainPage.xaml.cs through the cmd update characteristic
                        byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                        msg = Muse_Utils.Cmd_SetCalibrationMatrix((byte)calibrationIndex, 0, BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult[0], BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult[1], BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult[2]);

                        BLENetwork[LV_NetworkNodes.SelectedIndex].enableUploadCalibration = true;
                        BLENetwork[LV_NetworkNodes.SelectedIndex].calibColCounter++;

                        await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);
                    }
                    else
                    {
                        // Cancel command
                        // Reset calibration variables and results
                        BLENetwork[LV_NetworkNodes.SelectedIndex].calibResult = null;
                        BLENetwork[LV_NetworkNodes.SelectedIndex].calibChX = null;
                        BLENetwork[LV_NetworkNodes.SelectedIndex].calibChY = null;
                        BLENetwork[LV_NetworkNodes.SelectedIndex].calibChZ = null;

                    }

                }
                else
                {
                    // check your selection!
                }

            }

        }

        private async Task<bool> ManageMEMSCalibration()
        {
            if (LV_NetworkNodes.Items.Count > 0)
            {
               if (LV_NetworkNodes.SelectedItems.Count == 1)
                {

                    /////////////////////////////////////////////////////////////////////////////
                    //var yesCommand = new UICommand("Yes", cmd => { });
                    //var cancelCommand = new UICommand("Cancel", cmd => { });

                    if (calibrationIndex == 1)
                    {
                        // Setup dialog box
                        var content = "This procedure will change the current gyroscope offset calibraton parameters.\n" +
                            "Place the device in a static position and DO NOT MOVE IT.\n" +
                            "Wait until the green light stops blinking.";
                        var title = "Gyroscope Calibration";


                        if (System.Windows.Forms.MessageBox.Show(content, title, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                        {
                            BLENetwork[LV_NetworkNodes.SelectedIndex].cCalibrationType = Muse_HW.Calibration_Type.CALIB_TYPE_GYR;

                            // Set device into SYS_CALIB state in order to run the embedded magnetometer calibration procedure.
                            byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                            msg = Muse_Utils.Cmd_SetState(Muse_HW.SystemState.SYS_CALIB, new byte[5] { 1, 0, 0, 0, 0 });

                            await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);

                            await Task.Delay(10000);
                            System.Windows.Forms.MessageBox.Show("Gyroscope calibration completed.", title, MessageBoxButtons.OK);

                            return true;
                        }
                        return false;
                    }
                    else if (calibrationIndex == 0)
                    {
                        // Setup dialog box
                        var content = "This procedure will change the current calibraton parameters of the accelerometer.\nPlease, follow the next instructions.";
                        var title = "Accelerometer Calibration";

                            // ACCELEROMETER CALIBRATION
                            if (System.Windows.Forms.MessageBox.Show(content, title, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                            {
                            BLENetwork[LV_NetworkNodes.SelectedIndex].cCalibrationType = Muse_HW.Calibration_Type.CALIB_TYPE_AXL;

                            Calibration.REFR = 1000;

                            BLENetwork[LV_NetworkNodes.SelectedIndex].calibChX = new List<float>();
                            BLENetwork[LV_NetworkNodes.SelectedIndex].calibChY = new List<float>();
                            BLENetwork[LV_NetworkNodes.SelectedIndex].calibChZ = new List<float>();

                            Calibration.reset_remap_magcalib();
                            Calibration.init_magcalib();

                            ///////////////////////////////////////////////////////////////////////
                            content = "POSITION 1 - Place the device stable and UPWARD.";

                            if (System.Windows.Forms.MessageBox.Show(content, title, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                            {
                                // DATA POSITION #1

                                // Enable calibration flag

                                // Start AXL streaming for at least 30 seconds in static position
                                byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                                msg = Muse_Utils.Cmd_SetState(Muse_HW.SystemState.SYS_CALIB, new byte[5] { 0, 0x00, 0x00, 0x7a, 0x44 });
                                await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);

                                await Task.Delay(5000);

                                ///////////////////////////////////////////////////////////////////////
                                content = "POSITION 2 - When the green led slows blinking, UPSIDE DOWN the device.";

                                if (System.Windows.Forms.MessageBox.Show(content, title, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                                {
                                    // DATA POSITION #2

                                    // Enable calibration flag

                                    // Start AXL streaming for at least 30 seconds in static position
                                    msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                                    msg = Muse_Utils.Cmd_Acknowledge(Muse_HW.Acknowledge_Type.ACK_SUCCESS);

                                    await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);

                                    await Task.Delay(5000);

                                    ///////////////////////////////////////////////////////////////////////
                                    content = "POSITION 3 - When the green led slows blinking, rotate the device with the X AXIS POINTING UPWARD (USB port facing upwards).";

                                    if (System.Windows.Forms.MessageBox.Show(content, title, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                                    {
                                        // DATA POSITION #3

                                        // Enable calibration flag

                                        // Start AXL streaming for at least 30 seconds in static position
                                        msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                                        msg = Muse_Utils.Cmd_Acknowledge(Muse_HW.Acknowledge_Type.ACK_SUCCESS);
                                        await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);

                                        await Task.Delay(5000);

                                        ///////////////////////////////////////////////////////////////////////
                                        content = "POSITION 4 - When the green led slows blinking, rotate the device with the X AXIS POINTING DOWNWARD (USB port facing downwards).";

                                        if (System.Windows.Forms.MessageBox.Show(content, title, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                                        {
                                            // DATA POSITION #4

                                            // Enable calibration flag

                                            // Start IMU streaming for at least 30 seconds in static position
                                            msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                                            msg = Muse_Utils.Cmd_Acknowledge(Muse_HW.Acknowledge_Type.ACK_SUCCESS);
                                            await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);

                                            await Task.Delay(5000);

                                            ///////////////////////////////////////////////////////////////////////
                                            content = "POSITION 5 - When the green led slows blinking, rotate the device with the Y AXIS POINTING UPWARD (USB port facing right).";

                                            if (System.Windows.Forms.MessageBox.Show(content, title, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                                            {
                                                // DATA POSITION #5

                                                // Enable calibration flag

                                                // Start IMU streaming for at least 30 seconds in static position
                                                msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                                                msg = Muse_Utils.Cmd_Acknowledge(Muse_HW.Acknowledge_Type.ACK_SUCCESS);
                                                await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);

                                                await Task.Delay(5000);

                                                ///////////////////////////////////////////////////////////////////////
                                                content = "POSITION 6 - When the green led slows blinking, rotate the device with the Y AXIS POINTING DOWNWARD (USB port facing left).";

                                                if (System.Windows.Forms.MessageBox.Show(content, title, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                                                {
                                                    // DATA POSITION #6

                                                    // Enable calibration flag

                                                    // Start IMU streaming for at least 30 seconds in static position
                                                    msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                                                    msg = Muse_Utils.Cmd_Acknowledge(Muse_HW.Acknowledge_Type.ACK_SUCCESS);
                                                    await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);

                                                    await Task.Delay(5000);

                                                    ///////////////////////////////////////////////////////////////////////
                                                    //title = "Accelerometer Calibration";
                                                    content = "POSITION 7 - When the green led slows blinking, tilt the device by 45 deg with the X AXIS POINTING UPWARD.";

                                                    if (System.Windows.Forms.MessageBox.Show(content, title, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                                                    {
                                                        // DATA POSITION #7

                                                        // Enable calibration flag
                                                        // Start IMU streaming for at least 30 seconds in static position
                                                        msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                                                        msg = Muse_Utils.Cmd_Acknowledge(Muse_HW.Acknowledge_Type.ACK_SUCCESS);
                                                        await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);

                                                        await Task.Delay(5000);

                                                        ///////////////////////////////////////////////////////////////////////
                                                        content = "POSITION 8 - When the green led slows blinking, rotate the device 45 degrees clockwise.";

                                                        if (System.Windows.Forms.MessageBox.Show(content, title, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                                                        {
                                                            // DATA POSITION #8

                                                            // Enable calibration flag

                                                            // Start IMU streaming for at least 30 seconds in static position
                                                            msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                                                            msg = Muse_Utils.Cmd_Acknowledge(Muse_HW.Acknowledge_Type.ACK_SUCCESS);
                                                            await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);

                                                            await Task.Delay(5000);
                                                            //cNode.itCounter = 0;

                                                            ///////////////////////////////////////////////////////////////////////
                                                            content = "POSITION 9 - When the green led slows blinking, rotate the device 45 degrees clockwise.";

                                                            if (System.Windows.Forms.MessageBox.Show(content, title, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                                                            {
                                                                // DATA POSITION #9

                                                                // Enable calibration flag

                                                                // Start IMU streaming for at least 30 seconds in static position
                                                                msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                                                                msg = Muse_Utils.Cmd_Acknowledge(Muse_HW.Acknowledge_Type.ACK_SUCCESS);
                                                                await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);

                                                                await Task.Delay(5000);

                                                                ///////////////////////////////////////////////////////////////////////
                                                                content = "POSITION 10 - When the green led slows blinking, rotate the device 45 degrees clockwise.";

                                                                if (System.Windows.Forms.MessageBox.Show(content, title, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                                                                {
                                                                    // DATA POSITION #10

                                                                    // Enable calibration flag

                                                                    // Start IMU streaming for at least 30 seconds in static position
                                                                    msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                                                                    msg = Muse_Utils.Cmd_Acknowledge(Muse_HW.Acknowledge_Type.ACK_SUCCESS);
                                                                    await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);

                                                                    await Task.Delay(5000);

                                                                    ///////////////////////////////////////////////////////////////////////
                                                                    content = "POSITION 11 - When the green led slows blinking, flip the device and tilt it by 45 deg with the X AXIS POINTING UPWARD.";

                                                                    if (System.Windows.Forms.MessageBox.Show(content, title, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                                                                    {
                                                                        // DATA POSITION #11

                                                                        // Enable calibration flag

                                                                        // Start IMU streaming for at least 30 seconds in static position
                                                                        msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                                                                        msg = Muse_Utils.Cmd_Acknowledge(Muse_HW.Acknowledge_Type.ACK_SUCCESS);
                                                                        await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);

                                                                        await Task.Delay(5000);

                                                                        ///////////////////////////////////////////////////////////////////////
                                                                        content = "POSITION 12 - When the green led slows blinking, rotate the device 45 degrees clockwise.";

                                                                        if (System.Windows.Forms.MessageBox.Show(content, title, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                                                                        {
                                                                            // DATA POSITION #12

                                                                            // Enable calibration flag

                                                                            // Start IMU streaming for at least 30 seconds in static position
                                                                            msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                                                                            msg = Muse_Utils.Cmd_Acknowledge(Muse_HW.Acknowledge_Type.ACK_SUCCESS);
                                                                            await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);

                                                                            await Task.Delay(5000);

                                                                            ///////////////////////////////////////////////////////////////////////
                                                                            content = "POSITION 13 - When the green led slows blinking, rotate the device 45 degrees clockwise.";

                                                                            if (System.Windows.Forms.MessageBox.Show(content, title, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                                                                            {
                                                                                // DATA POSITION #13

                                                                                // Enable calibration flag

                                                                                // Start IMU streaming for at least 30 seconds in static position
                                                                                msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                                                                                msg = Muse_Utils.Cmd_Acknowledge(Muse_HW.Acknowledge_Type.ACK_SUCCESS);
                                                                                await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);

                                                                                await Task.Delay(5000);

                                                                                ///////////////////////////////////////////////////////////////////////
                                                                                //title = "Accelerometer Calibration";
                                                                                //content = "POSITION #14\nIf the green led slows blinking, place the device stable, tilted by 45 deg and with the Y AXIS POINTING DOWNWARD";
                                                                                content = "POSITION 14 - When the green led slows blinking, rotate the device 45 degrees clockwise.";

                                                                                if (System.Windows.Forms.MessageBox.Show(content, title, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                                                                                {
                                                                                    // DATA POSITION #14

                                                                                    // Enable calibration flag

                                                                                    // Start IMU streaming for at least 30 seconds in static position
                                                                                    msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                                                                                    msg = Muse_Utils.Cmd_Acknowledge(Muse_HW.Acknowledge_Type.ACK_SUCCESS);
                                                                                    await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);

                                                                                    await Task.Delay(6000);

                                                                                    msg = Muse_Utils.Cmd_Acknowledge(Muse_HW.Acknowledge_Type.ACK_ERROR);
                                                                                    await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);

                                                                                    await Task.Delay(1000);
                                                                                    System.Windows.Forms.MessageBox.Show("Accelerometer calibration completed.", title, MessageBoxButtons.OK);

                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        // MAGNETOMETER CALIBRATION

                        var content = "This procedure will change the current calibraton parameters of the magnetometer.\n" +
                            "Rotate the device in all directions for a few seconds.\n" +
                            "Wait until the green light stops blinking.";
                        var title = "Magnetomer Calibration";

                        if (System.Windows.Forms.MessageBox.Show(content, title, MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                        {
                            BLENetwork[LV_NetworkNodes.SelectedIndex].cCalibrationType = Muse_HW.Calibration_Type.CALIB_TYPE_MAG;

                            // Set device into SYS_CALIB state in order to run the embedded magnetometer calibration procedure.
                            byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                            msg = Muse_Utils.Cmd_SetState(Muse_HW.SystemState.SYS_CALIB, new byte[5] { 2, 0x00, 0x00, 0xc8, 0x43 });

                            await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);

                            await Task.Delay(15000);
                            System.Windows.Forms.MessageBox.Show("Magnetometer calibration completed.", title, MessageBoxButtons.OK);

                            return true;
                        }



                        return false;
                    }
                }
                else
                {
                    // check your selection!
                }

            }
            return true;
        }

        private async void BtnSetFS_Click(object sender, RoutedEventArgs e)
        {
            if (LV_NetworkNodes.Items.Count > 0)
            {
                if (LV_NetworkNodes.SelectedItems.Count == 1)
                {
                    byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                    msg = Muse_Utils.Cmd_SetSensorsFS(selectedAxl_FS, selectedGyr_FS, selectedMag_FS, selectedHDR_FS);

                    await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);
                }
                else
                {
                    // check your selection!
                }

            }

        }

        private void CBAxlFS_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (CBAxlFS.SelectedIndex)
            {
                case 0:
                    selectedAxl_FS = Muse_HW.Accelerometer_FS.AXL_FS_4g;
                    CBAxlFS.Text = "4";
                    break;
                case 1:
                    selectedAxl_FS = Muse_HW.Accelerometer_FS.AXL_FS_08g;
                    CBAxlFS.Text = "8";
                    break;
                case 2:
                    selectedAxl_FS = Muse_HW.Accelerometer_FS.AXL_FS_16g;
                    CBAxlFS.Text = "16";
                    break;
                case 3:
                    selectedAxl_FS = Muse_HW.Accelerometer_FS.AXL_FS_32g;
                    CBAxlFS.Text = "32";
                    break;
                default:
                    selectedAxl_FS = Muse_HW.Accelerometer_FS.AXL_FS_4g;
                    CBAxlFS.Text = "4";
                    break;
            }
        }

        private void CBGyrFS_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (CBGyrFS.SelectedIndex)
            {
                case 0:
                    selectedGyr_FS = Muse_HW.Gyroscope_FS.GYR_FS_245dps;
                    CBGyrFS.Text = "245";
                    break;
                case 1:
                    selectedGyr_FS = Muse_HW.Gyroscope_FS.GYR_FS_500dps;
                    CBGyrFS.Text = "500";
                    break;
                case 2:
                    selectedGyr_FS = Muse_HW.Gyroscope_FS.GYR_FS_1000dps;
                    CBGyrFS.Text = "1000";
                    break;
                case 3:
                    selectedGyr_FS = Muse_HW.Gyroscope_FS.GYR_FS_2000dps;
                    CBGyrFS.Text = "2000";
                    break;
                default:
                    selectedGyr_FS = Muse_HW.Gyroscope_FS.GYR_FS_2000dps;
                    CBGyrFS.Text = "2000";
                    break;
            }
        }

        private void CBmagFS_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (CBmagFS.SelectedIndex)
            {
                case 0:
                    selectedMag_FS = Muse_HW.Magnetometer_FS.MAG_FS_04G;
                    CBmagFS.Text = "4";
                    break;
                case 1:
                    selectedMag_FS = Muse_HW.Magnetometer_FS.MAG_FS_08G;
                    CBmagFS.Text = "8";
                    break;
                case 2:
                    selectedMag_FS = Muse_HW.Magnetometer_FS.MAG_FS_12G;
                    CBmagFS.Text = "12";
                    break;
                case 3:
                    selectedMag_FS = Muse_HW.Magnetometer_FS.MAG_FS_16G;
                    CBmagFS.Text = "16";
                    break;
                default:
                    selectedMag_FS = Muse_HW.Magnetometer_FS.MAG_FS_04G;
                    CBmagFS.Text = "4";
                    break;
            }
        }

        private void CBhdrFS_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (CBhdrFS.SelectedIndex)
            {
                case 0:
                    selectedHDR_FS = Muse_HW.Accelerometer_HDR_FS.HDR_FS_100g;
                    CBhdrFS.Text = "100";
                    break;
                case 1:
                    selectedHDR_FS = Muse_HW.Accelerometer_HDR_FS.HDR_FS_200g;
                    CBhdrFS.Text = "200";
                    break;
                case 2:
                    selectedHDR_FS = Muse_HW.Accelerometer_HDR_FS.HDR_FS_400g;
                    CBhdrFS.Text = "400";
                    break;
                default:
                    selectedHDR_FS = Muse_HW.Accelerometer_HDR_FS.HDR_FS_100g;
                    CBhdrFS.Text = "100";
                    break;
            }
        }

        private async void BtnSetLogMode_Click(object sender, RoutedEventArgs e)
        {
            if (LV_NetworkNodes.Items.Count > 0)
            {
                if (LV_NetworkNodes.SelectedItems.Count == 1)
                {
                    if (SelectedAcquisitionMode == Muse_HW.Acquisition_Type.DATA_MODE_NONE ||
                    selectedLogFreq == Muse_HW.AcquisitionFrequency.DATA_FREQ_NONE)
                    {
                        System.Windows.Forms.MessageBox.Show("No log configuration found. Please select at least one acquisiton mode and a frequency.",
                                        "Error",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                    }
                    else if (2040 % Muse_Utils.GetPacketDimension((UInt32)SelectedAcquisitionMode) != 0)
                    {
                        System.Windows.Forms.MessageBox.Show("No log configuration found. Every data type has a dimension of 6 bytes. Only combinations whose size is divisor of 2040 are accepted.",
                                       "Error",
                                       MessageBoxButtons.OK,
                                       MessageBoxIcon.Error);

                    }
                    else
                    {
                        byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                        msg = Muse_Utils.Cmd_SetLogMode(SelectedAcquisitionMode, selectedLogFreq);

                        await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);

                        MessageBox.Text = "New Log Mode encoded";
                    }
                }
                else
                {
                    // check your selection!
                }

            }

        }

        private void CBLogFreq_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (CBLogFreq.SelectedIndex)
            {
                case 0:
                    selectedLogFreq = Muse_HW.AcquisitionFrequency.DATA_FREQ_25Hz;
                    CBLogFreq.Text = "25";
                    break;
                case 1:
                    selectedLogFreq = Muse_HW.AcquisitionFrequency.DATA_FREQ_50Hz;
                    CBLogFreq.Text = "50";
                    break;
                case 2:
                    selectedLogFreq = Muse_HW.AcquisitionFrequency.DATA_FREQ_100Hz;
                    CBLogFreq.Text = "100";
                    break;
                case 3:
                    selectedLogFreq = Muse_HW.AcquisitionFrequency.DATA_FREQ_200Hz;
                    CBLogFreq.Text = "200";
                    break;
                case 4:
                    selectedLogFreq = Muse_HW.AcquisitionFrequency.DATA_FREQ_400Hz;
                    CBLogFreq.Text = "400";
                    break;
                case 5:
                    selectedLogFreq = Muse_HW.AcquisitionFrequency.DATA_FREQ_800Hz;
                    CBLogFreq.Text = "800";
                    break;
                case 6:
                    selectedLogFreq = Muse_HW.AcquisitionFrequency.DATA_FREQ_1600Hz;
                    CBLogFreq.Text = "1600";
                    break;
                default:
                    selectedLogFreq = Muse_HW.AcquisitionFrequency.DATA_FREQ_NONE;
                    break;
            }
        }

        private async void BtnGetFS_Click(object sender, RoutedEventArgs e)
        {
            if (LV_NetworkNodes.Items.Count > 0)
            {

                if (LV_NetworkNodes.SelectedItems.Count == 1)
                {
                    byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                    msg = Muse_Utils.Cmd_GetSensorsFS();

                    await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);
                }
                else
                {
                    // check your selection!
                }

            }

        }

        private void comboBox_DropDownClosed(object sender, EventArgs e)
        {
            showmodes();
        }

        private void showmodes()
        {
            CBLogMode.Text = null;
            modecount = 0;
            SelectedAcquisitionMode = Muse_HW.Acquisition_Type.DATA_MODE_NONE;

            for (int i = 0; i < CBLogMode.Items.Count; i++)
            {
                comboItem item = CBLogMode.Items.GetItemAt(i) as comboItem;
                if (item.ischecked)
                {
                    string acqModeAcr = "";
                    if (item.mode == "Gyroscope")
                    {
                        acqModeAcr = "GYR";
                        SelectedAcquisitionMode |= Muse_HW.Acquisition_Type.DATA_MODE_GYRO;
                    }
                    else if (item.mode == "Accelerometer")
                    {
                        acqModeAcr = "AXL";
                        SelectedAcquisitionMode |= Muse_HW.Acquisition_Type.DATA_MODE_AXL;
                    }
                    else if (item.mode == "Magnetometer")
                    {
                        acqModeAcr = "MAGN";
                        SelectedAcquisitionMode |= Muse_HW.Acquisition_Type.DATA_MODE_MAGN;

                    }
                    else if (item.mode == "HDR Accelerometer")
                    {
                        acqModeAcr = "HDR";
                        SelectedAcquisitionMode |= Muse_HW.Acquisition_Type.DATA_MODE_HDR;

                    }
                    else if (item.mode == "Temperature & Relative Humidity")
                    {
                        acqModeAcr = "TEMP+HUM";
                        SelectedAcquisitionMode |= Muse_HW.Acquisition_Type.DATA_MODE_TEMP_HUM;

                    }
                    else if (item.mode == "Temperature & Ambient pressure")
                    {
                        acqModeAcr = "TEMP+PRESS";
                        SelectedAcquisitionMode |= Muse_HW.Acquisition_Type.DATA_MODE_TEMP_PRESS;

                    }
                    else if (item.mode == "Orientation")
                    {
                        acqModeAcr = "AHRS";
                        SelectedAcquisitionMode |= Muse_HW.Acquisition_Type.DATA_MODE_ORIENTATION;

                    }
                    else if (item.mode == "Timestamp")
                    {
                        acqModeAcr = "TIMESTAMP";
                        SelectedAcquisitionMode |= Muse_HW.Acquisition_Type.DATA_MODE_TIMESTAMP;

                    }
                    else if (item.mode == "Conditonal Log")
                    {
                        acqModeAcr = "COND";
                        SelectedAcquisitionMode |= Muse_HW.Acquisition_Type.DATA_MODE_CONDITIONALLOG;

                    }
                    else if (item.mode == "Encoder")
                    {
                        acqModeAcr = "ENC";
                        SelectedAcquisitionMode |= Muse_HW.Acquisition_Type.DATA_MODE_ENCODER;

                    }
                    CBLogMode.Text = CBLogMode.Text + acqModeAcr + ", ";
                    modecount++;
                }
            }

            if (modecount != 0)
            {
                if (modecount == 9)
                {
                    CBLogMode.Text = "All";
                }
                else
                {
                    CBLogMode.Text = CBLogMode.Text.TrimEnd(' ', ',');
                }
            }
            else
            {
                CBLogMode.Text = "Data mode";
            }
        }

        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CBLogMode.SelectedIndex = -1;
        }

        private void comboBoxAcq_DropDownClosed(object sender, EventArgs e)
        {
            showacq();
        }

        private void showacq()
        {
            CBAcquisitionData.Text = null;
            acqcount = 0;
            SelectedAcquisitionData = Muse_HW.Acquisition_Type.DATA_MODE_NONE;

            for (int i = 0; i < CBAcquisitionData.Items.Count; i++)
            {
                comboItem item = CBAcquisitionData.Items.GetItemAt(i) as comboItem;
                if (item.ischecked)
                {
                    string acqModeAcr = "";
                    if (item.mode == "Gyroscope")
                    {
                        acqModeAcr = "GYR";
                        SelectedAcquisitionData |= Muse_HW.Acquisition_Type.DATA_MODE_GYRO;
                    }
                    else if (item.mode == "Accelerometer")
                    {
                        acqModeAcr = "AXL";
                        SelectedAcquisitionData |= Muse_HW.Acquisition_Type.DATA_MODE_AXL;
                    }
                    else if (item.mode == "Magnetometer")
                    {
                        acqModeAcr = "MAGN";
                        SelectedAcquisitionData |= Muse_HW.Acquisition_Type.DATA_MODE_MAGN;

                    }
                    else if (item.mode == "HDR Accelerometer")
                    {
                        acqModeAcr = "HDR";
                        SelectedAcquisitionData |= Muse_HW.Acquisition_Type.DATA_MODE_HDR;

                    }
                    else if (item.mode == "Temperature & Relative Humidity")
                    {
                        acqModeAcr = "TEMP+HUM";
                        SelectedAcquisitionData |= Muse_HW.Acquisition_Type.DATA_MODE_TEMP_HUM;

                    }
                    else if (item.mode == "Temperature & Ambient pressure")
                    {
                        acqModeAcr = "TEMP+PRESS";
                        SelectedAcquisitionData |= Muse_HW.Acquisition_Type.DATA_MODE_TEMP_PRESS;

                    }
                    else if (item.mode == "Orientation")
                    {
                        acqModeAcr = "AHRS";
                        SelectedAcquisitionData |= Muse_HW.Acquisition_Type.DATA_MODE_ORIENTATION;

                    }
                    else if (item.mode == "Timestamp")
                    {
                        acqModeAcr = "TIMESTAMP";
                        SelectedAcquisitionData |= Muse_HW.Acquisition_Type.DATA_MODE_TIMESTAMP;

                    }
                    else if (item.mode == "Conditional Log")
                    {
                        acqModeAcr = "COND";
                        SelectedAcquisitionData |= Muse_HW.Acquisition_Type.DATA_MODE_CONDITIONALLOG;

                    }
                    else if (item.mode == "Encoder")
                    {
                        acqModeAcr = "ENC";
                        SelectedAcquisitionData |= Muse_HW.Acquisition_Type.DATA_MODE_ENCODER;

                    }
                    CBAcquisitionData.Text = CBAcquisitionData.Text + acqModeAcr + ", ";
                    acqcount++;
                }
            }

            if (acqcount != 0)
            {
                if (acqcount == 9)
                {
                    CBAcquisitionData.Text = "All";
                }
                else
                {
                    CBAcquisitionData.Text = CBAcquisitionData.Text.TrimEnd(' ', ',');
                }
            }
            else
            {
                CBAcquisitionData.Text = "Data mode";
            }
        }

        private void comboBoxAcq_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CBLogMode.SelectedIndex = -1;
        }

        private async void BtnGetLogMode_Click(object sender, RoutedEventArgs e)
        {
            if (LV_NetworkNodes.Items.Count > 0)
            {
                if (LV_NetworkNodes.SelectedItems.Count == 1)
                {
                    byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                    msg = Muse_Utils.Cmd_GetLogMode();

                    await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);
                }
                else
                {
                    // check your selection!
                }

            }

        }

        private async void BtnSetThresholds_Click(object sender, RoutedEventArgs e)
        {
            if (LV_NetworkNodes.Items.Count > 0)
            {
                if (LV_NetworkNodes.SelectedItems.Count == 1 && LblThreshold1Input.Text != "" && LblThreshold2Input.Text != "")
                {
                    byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];

                    try
                    {
                        msg = Muse_Utils.Cmd_SetThresholds(float.Parse(LblThreshold1Input.Text), float.Parse(LblThreshold2Input.Text));
                        await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);
                    }
                    catch (FormatException)
                    {
                        MessageBox.Text = "Invalid thresholds format.";
                    }
                    catch (ArgumentNullException)
                    {
                        MessageBox.Text = "Found a NULL threshold.";
                    }

                    

                    
                }
                else
                {
                    // check your selection!
                }

            }

        }

        private async void BtnGetThresholds_Click(object sender, RoutedEventArgs e)
        {
            if (LV_NetworkNodes.Items.Count > 0)
            {
               if (LV_NetworkNodes.SelectedItems.Count == 1)
                {
                    byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                    msg = Muse_Utils.Cmd_GetThresholds();

                    await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);
                }
                else
                {
                    // check your selection!
                }

            }

        }

        private async void BtnStandby_Click(object sender, RoutedEventArgs e)
        {
            if (LV_NetworkNodes.Items.Count > 0)
            {
                if (LV_NetworkNodes.SelectedItems.Count == 1)
                {
                    standby_changed = true;

                    byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];

                    if (enable_standby)
                        msg = Muse_Utils.Cmd_SetStandby(true);
                    else
                        msg = Muse_Utils.Cmd_SetStandby(false);
                    await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);
                }
                else
                {
                    // check your selection!
                }

            }


        }

        private async void BtnMemoryBuffer_Click(object sender, RoutedEventArgs e)
        {
            if (LV_NetworkNodes.Items.Count > 0)
            {
                if (LV_NetworkNodes.SelectedItems.Count == 1)
                {
                    if (System.Windows.Forms.MessageBox.Show("Changing the device memory buffer configuration will erase the device internal memory.\n" +
                        "OK to continue. CANCEL otherwise.",
                                "Attention!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
                    {
                        memory_changed = true;

                        // Build message
                        byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];

                        if (set_circular)
                            msg = Muse_Utils.Cmd_SetCircular(true);
                        else
                            msg = Muse_Utils.Cmd_SetCircular(false);

                        await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);
                    }

                }
                else
                {
                    // check your selection!
                }

            }

        }

        private async void BtnRestart_Click(object sender, RoutedEventArgs e)
        {
            if (LV_NetworkNodes.Items.Count > 0)
            {
                if (LV_NetworkNodes.SelectedItems.Count == 1)
                {
                    // Build message
                    byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                    if (RBNormal.IsChecked == true)
                        msg = Muse_Utils.Cmd_Restart(Muse_HW.RestartMode.RESTART_RESET);
                    else
                        msg = Muse_Utils.Cmd_Restart(Muse_HW.RestartMode.RESTART_BOOT_LOADER);

                    await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);

                    KnownDevices.Clear();
                    UnknownDevices.Clear();

                    BLENetwork.Clear();

                    // Clear all fieds

                    comboMode.Clear();
                    comboMode.Add(new comboItem { mode = "Gyroscope", ischecked = false });
                    comboMode.Add(new comboItem { mode = "Accelerometer", ischecked = false });
                    comboMode.Add(new comboItem { mode = "Magnetometer", ischecked = false });
                    comboMode.Add(new comboItem { mode = "HDR Accelerometer", ischecked = false });
                    comboMode.Add(new comboItem { mode = "Temperature & Relative Humidity", ischecked = false });
                    comboMode.Add(new comboItem { mode = "Temperature & Ambient pressure", ischecked = false });
                    comboMode.Add(new comboItem { mode = "Orientation", ischecked = false });
                    comboMode.Add(new comboItem { mode = "Timestamp", ischecked = false });
                    comboMode.Add(new comboItem { mode = "Conditional Log", ischecked = false });
                    comboMode.Add(new comboItem { mode = "Encoder", ischecked = false });
                    CBLogMode.Text = "Data mode";

                    CBLogFreq.Text = "Freq";
                    SelectedAcquisitionMode = Muse_HW.Acquisition_Type.DATA_MODE_NONE;

                    comboAcqItems.Clear();
                    comboAcqItems.Add(new comboItem { mode = "Gyroscope", ischecked = false });
                    comboAcqItems.Add(new comboItem { mode = "Accelerometer", ischecked = false });
                    comboAcqItems.Add(new comboItem { mode = "Magnetometer", ischecked = false });
                    comboAcqItems.Add(new comboItem { mode = "HDR Accelerometer", ischecked = false });
                    comboAcqItems.Add(new comboItem { mode = "Temperature & Relative Humidity", ischecked = false });
                    comboAcqItems.Add(new comboItem { mode = "Temperature & Ambient pressure", ischecked = false });
                    comboAcqItems.Add(new comboItem { mode = "Orientation", ischecked = false });
                    comboAcqItems.Add(new comboItem { mode = "Timestamp", ischecked = false });
                    comboAcqItems.Add(new comboItem { mode = "Conditional Log", ischecked = false });
                    comboAcqItems.Add(new comboItem { mode = "Encoder", ischecked = false });
                    CBAcquisitionData.Text = "Data mode";

                    CBAcquisitionFreq.Text = "Freq";
                    SelectedAcquisitionData = Muse_HW.Acquisition_Type.DATA_MODE_NONE;

                    LblCurrentState.Text = "---";
                    LblBatteryCharge.Text = "---";
                    LblBatteryVoltage.Text = "---";
                    LblFirmwareVersion.Text = "---";
                    LblDeviceId.Text = "---";
                    LblDateTime.Text = "---";
                }
                else
                {
                    // check your selection!
                }

            }
        }

        // Select binary file to be uploaded
        Windows.Storage.StorageFile file_;
        public byte[] fsBytes_ = null;
        public int fsBytesLength_ = 0;


        private async void btn_Browse_Click(object sender, RoutedEventArgs e)
        {
            // Setup file browser

            // Perform file browsing
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Binary files (*.bin;*.binary;*.elf)|*.bin;*.binary;*.elf|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                FirmwareFilePath.Text = openFileDialog.FileName;

                if (openFileDialog.OpenFile() != null)
                {
                    using (FileStream fsSource = new FileStream(openFileDialog.FileName, FileMode.Open, FileAccess.Read))
                    {
                        fsBytes_ = new byte[fsSource.Length];

                        int numBytesToRead = (int)fsSource.Length;
                        int numBytesRead = 0;
                        while (numBytesToRead > 0)
                        {
                            // Read may return anything from 0 to numBytesToRead.
                            int n = fsSource.Read(fsBytes_, numBytesRead, numBytesToRead);

                            // Break when the end of the file is reached.
                            if (n == 0)
                                break;

                            numBytesRead += n;
                            numBytesToRead -= n;
                        }
                    }

                    if (fsBytes_ != null)
                    {
                        // Retrieve buffer size
                        fsBytesLength_ = fsBytes_.Length;
                    }
                    else
                        FirmwareFilePath.Text = "ERROR! - Operation cancelled. Null byte array.";
                }
                
            }
            else
            {
                FirmwareFilePath.Text = "ERROR! - Operation cancelled. Null input file.";
            }
        }

        private async void btn_Upload_Click(object sender, RoutedEventArgs e)
        {
            if (LV_NetworkNodes.Items.Count > 0)
            {
                if (LV_NetworkNodes.SelectedItems.Count == 1)
                {
                    byte[] buffer = new byte[20];
                    int respLen = 0;

                    if (fsBytes_ != null)
                    {
                        int binSize = fsBytes_.Length;
                        if (binSize > 0)
                        {
                            // Header
                            buffer[0] = (byte)Muse_HW.Command.CMD_FW_UPLOAD;
                            respLen = 4; // 8;
                            buffer[1] = Convert.ToByte(respLen);

                            // Payload
                            byte[] tmp = BitConverter.GetBytes(binSize);        // Firmware lenght [num of bytes]
                            Array.Copy(tmp, 0, buffer, 2, 4);
                        }
                    }

                    if (buffer != null)
                    {
                        // Setup data writer object
                        var writer = new DataWriter();
                        writer.ByteOrder = ByteOrder.BigEndian;

                        writer.WriteBytes(buffer);

                        try
                        {
                            // Write to selected characteristic
                            //await mpRef.BLENetwork[LV_NetworkNodes.SelectedIndex].SelectedCharacteristic_cmd.WriteValueAsync(writer.DetachBuffer());

                            await BLENetwork[LV_NetworkNodes.SelectedIndex].SelectedCharacteristic_cmd.WriteValueAsync(writer.DetachBuffer());
                            await Task.Delay(2500);
                        }
                        catch (Exception)
                        {
                        }

                    }
                }
                else
                {
                    // check your selection!
                }

            }
        }


        #endregion

        private async void BtnStartStream_Click(object sender, RoutedEventArgs e)
        {
            // Sent start log command to all selected devices
            if (LV_NetworkNodes.Items.Count > 0)
            {
                if (SelectedAcquisitionData == Muse_HW.Acquisition_Type.DATA_MODE_NONE ||
                SelectedAcquisitionFrequency == Muse_HW.AcquisitionFrequency.DATA_FREQ_NONE)
                {
                    System.Windows.Forms.MessageBox.Show("No stream configuration found.",
                                        "Error",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);
                }
                else if (2040 % Muse_Utils.GetPacketDimension((UInt32)SelectedAcquisitionData) != 0)
                {
                    System.Windows.Forms.MessageBox.Show("No stream configuration found. Every data type has a dimension of 6 bytes. Only combinations whose size is divisor of 2040 are accepted.",
                                        "Error",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                }
                else
                {
                      List<int> selectedItemIndexes = new List<int>();
                        foreach (object o in LV_NetworkNodes.SelectedItems)
                            selectedItemIndexes.Add(LV_NetworkNodes.Items.IndexOf(o));

                        foreach (var idx in selectedItemIndexes)
                        {


                            byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                        msg = Muse_Utils.Cmd_GetSensorsFS();
                        await BLENetwork[idx].SendMessage(msg);

                        System.Threading.Thread.Sleep(100);
                        msg = Muse_Utils.Cmd_StartStreaming(SelectedAcquisitionData, SelectedAcquisitionFrequency);
                        await BLENetwork[idx].SendMessage(msg);

                        BLENetwork[idx].acquisitionStreamEnabled = true;

                        if( (SelectedAcquisitionData & Muse_HW.Acquisition_Type.DATA_MODE_ORIENTATION)> 0) // ||
                            //(SelectedAcquisitionData & Muse_HW.Acquisition_Type.DATA_MODE_MAGN) > 0) ||
                            //SelectedAcquisitionData == (Muse_HW.Acquisition_Type.DATA_MODE_ORIENTATION | Muse_HW.Acquisition_Type.DATA_MODE_MAGN))
                        {
                            spaceChart_ = new ThreeDViewWindow();
                            spaceChart_.Title = "3D View";
                            spaceChart_.Show();
                        }
                        if ((SelectedAcquisitionData & Muse_HW.Acquisition_Type.DATA_MODE_GYRO) > 0)
                        {
                            gyroChart_ = new LineChartWindow(3, new string[] { "x", "y", "z" }, "Sample", "Gyroscope [dps]"); //, new int[] { 0, 500 }, new int[] { ble.bleNode.gyro_fs_list[0], ble.bleNode.gyro_fs_list[0] });
                            gyroChart_.Title = "Gyroscope";
                            gyroChart_.Show();

                        }
                        if ((SelectedAcquisitionData & Muse_HW.Acquisition_Type.DATA_MODE_AXL) > 0)
                        {
                            accChart_ = new LineChartWindow(3, new string[] { "x", "y", "z" }, "Sample", "Accelerometer [mg]"); //, new int[] { 0, 500 }), new int[] { ble.bleNode.axl_fs_list[1] * 1000, ble.bleNode.axl_fs_list[1] * 1000 });
                            accChart_.Title = "Acceleration";
                            accChart_.Show();

                        }
                        if ((SelectedAcquisitionData & Muse_HW.Acquisition_Type.DATA_MODE_MAGN) > 0)
                        {
                            magnChart_ = new LineChartWindow(3, new string[] {"x", "y", "z" }, "Sample", "Magnetometer [mG]"); //, new int[] { 0, 500 }), new int[] { ble.bleNode.fs[2] * 1000, fullScaleParams_[2] * 1000 });
                            magnChart_.Title = "Magnetometer";
                            magnChart_.Show();
                        }
                        if ((SelectedAcquisitionData & Muse_HW.Acquisition_Type.DATA_MODE_HDR) > 0)
                        {
                            accHDRChart_ = new LineChartWindow(3, new string[] { "x", "y", "z" }, "Sample", "HDR Accelerometer [mg]"); //, new int[] { 0, 500 }), new int[] { -fullScaleParams_[3] * 1000, fullScaleParams_[3] * 1000 });
                            accHDRChart_.Title = "HDR";
                            accHDRChart_.Show();
                        }
                        //if (SelectedAcquisitionData == Muse_HW.Acquisition_Type.DATA_MODE_TEMP_HUM)
                        //{

                        //}
                        //if (SelectedAcquisitionData == Muse_HW.Acquisition_Type.DATA_MODE_TEMP_PRESS)
                        //{

                        //}
                        if ((SelectedAcquisitionData & Muse_HW.Acquisition_Type.DATA_MODE_ORIENTATION) > 0)
                        {
                            quatChart_ = new LineChartWindow(4, new string[] { "w", "x", "y", "z" }, "Sample", "Quaternion"); //, new int[] { 0, 500 }, new int[] { -1, 1 });
                            quatChart_.Title = "Quaternion";
                            quatChart_.Show();
                        }
                        //if (SelectedAcquisitionData == Muse_HW.Acquisition_Type.DATA_MODE_TIMESTAMP)
                        //{

                        //}
                        //if (SelectedAcquisitionData == Muse_HW.Acquisition_Type.DATA_MODE_ENCODER)
                        //{

                        //}

                    }

                    MessageBox.Text = "Stream started";
                }

            }
        }

        public Muse_HW.AcquisitionFrequency SelectedAcquisitionFrequency { get; set; }

        private void CBAcquisitionFreq_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (CBAcquisitionFreq.SelectedIndex)
            {
                case 0:     // DATA_FREQ_25Hz = 1,
                    SelectedAcquisitionFrequency = Muse_HW.AcquisitionFrequency.DATA_FREQ_25Hz;
                    CBAcquisitionFreq.Text = "25";
                    break;
                case 1:     // DATA_FREQ_50Hz = 2,
                    SelectedAcquisitionFrequency = Muse_HW.AcquisitionFrequency.DATA_FREQ_50Hz;
                    CBAcquisitionFreq.Text = "50";
                    break;
                case 2:     // DATA_FREQ_100Hz = 4,
                    SelectedAcquisitionFrequency = Muse_HW.AcquisitionFrequency.DATA_FREQ_100Hz;
                    CBAcquisitionFreq.Text = "100";
                    break;
                case 3:     // DATA_FREQ_200Hz = 8,
                    SelectedAcquisitionFrequency = Muse_HW.AcquisitionFrequency.DATA_FREQ_200Hz;
                    CBAcquisitionFreq.Text = "200";
                    break;
                case 4:     // DATA_FREQ_400Hz = 16,
                    SelectedAcquisitionFrequency = Muse_HW.AcquisitionFrequency.DATA_FREQ_400Hz;
                    CBAcquisitionFreq.Text = "400";
                    break;
                case 5:     // DATA_FREQ_800Hz = 32,
                    SelectedAcquisitionFrequency = Muse_HW.AcquisitionFrequency.DATA_FREQ_800Hz;
                    CBAcquisitionFreq.Text = "800";
                    break;
                case 6:     // DATA_FREQ_1600Hz = 64,
                    SelectedAcquisitionFrequency = Muse_HW.AcquisitionFrequency.DATA_FREQ_1600Hz;
                    CBAcquisitionFreq.Text = "1600";
                    break;
                default:     // DATA_FREQ_NONE = 0,
                    SelectedAcquisitionFrequency = Muse_HW.AcquisitionFrequency.DATA_FREQ_NONE;
                    break;
            }
        }

        private async void BtnStartLog_Click(object sender, RoutedEventArgs e)
        {
            // Sent start log command to all selected devices
            if (LV_NetworkNodes.Items.Count > 0)
            {
                // Clear all fieds
                MessageBox.Text = "";
                LblCurrentFileInfo.Text = "";
                LblMemoryControl.Text = "";

                
                    if (SelectedAcquisitionData == Muse_HW.Acquisition_Type.DATA_MODE_NONE ||
                        SelectedAcquisitionFrequency == Muse_HW.AcquisitionFrequency.DATA_FREQ_NONE)
                    {
                        System.Windows.Forms.MessageBox.Show("No log configuration found. Please select at least one acquisiton mode and a frequency.",
                                        "Error",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                    }
                    else if (2040 % Muse_Utils.GetPacketDimension((UInt32)SelectedAcquisitionData) != 0)
                    {
                        System.Windows.Forms.MessageBox.Show("No log configuration found. Every data type has a dimension of 6 bytes. Only combinations whose size is divisor of 2040 are accepted.",
                                        "Error",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                    }
                    else
                    {
                        byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                        msg = Muse_Utils.Cmd_StartLog(SelectedAcquisitionData, SelectedAcquisitionFrequency);

                    List<int> selectedItemIndexes = new List<int>();
                    foreach (object o in LV_NetworkNodes.SelectedItems)
                        selectedItemIndexes.Add(LV_NetworkNodes.Items.IndexOf(o));

                    foreach (var idx in selectedItemIndexes)
                    {
                        await BLENetwork[idx].SendMessage(msg);
                        BLENetwork[idx].acquisitionLogEnabled = true;

                        }

                    MessageBox.Text = "Log started";
                }
                

            }
        }

        private async void BtnStopAcquisition_Click(object sender, RoutedEventArgs e)
        {
            // Stop acquisition of all selected nodes

            logUpdate = false;
            MessageBox.Text = "";
            LblCurrentFileInfo.Text = "";
            LblMemoryControl.Text = "";

            if (LV_NetworkNodes.Items.Count > 0)
            {
                byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                msg = Muse_Utils.Cmd_SetState(Muse_HW.SystemState.SYS_IDLE);

                List<int> selectedItemIndexes = new List<int>();
                foreach (object o in LV_NetworkNodes.SelectedItems)
                    selectedItemIndexes.Add(LV_NetworkNodes.Items.IndexOf(o));

                foreach (var idx in selectedItemIndexes)
                {
                    await BLENetwork[idx].SendMessage(msg);
                    BLENetwork[idx].acquisitionStreamEnabled = false;
                    BLENetwork[idx].acquisitionLogEnabled = false;
                    UpdateLogFiles();

                    if (spaceChart_ != null)
                    {
                        spaceChart_.Close();
                    }

                    if (gyroChart_ != null)
                    {
                        gyroChart_.ClearChart();
                        gyroChart_.Close();
                    }

                    if (accChart_ != null) 
                    {
                        accChart_.ClearChart();
                        accChart_.Close();
                    }

                    if (accHDRChart_ != null)
                    {
                        accHDRChart_.ClearChart();
                        accHDRChart_.Close();
                    }

                    if (magnChart_ != null)
                    {
                        magnChart_.ClearChart();
                        magnChart_.Close();
                    }
                    if (eulerChart_ != null)
                    {
                        eulerChart_.ClearChart();
                        eulerChart_.Close();
                    }

                    if (quatChart_ != null)
                    {
                        quatChart_.ClearChart();
                        quatChart_.Close();
                    }


                }

                MessageBox.Text = "Acquisition stopped";
            }

        }

        public static bool logUpdate;

        private string fileInfo;
        public string FileInfo
        {
            get { return fileInfo; }
            set
            {
                fileInfo = value;
                OnPropertyChanged("FileInfo");
            }
        }

        public async void UpdateLogFiles()
        {
            if (LV_NetworkNodes.Items.Count > 0)
            {
                if (LV_NetworkNodes.SelectedItems.Count == 1)
                {
                    logUpdate = true;
                    //await Task.Delay(200);

                    // Works only for one selected device at a time
                    byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                    msg = Muse_Utils.Cmd_Memory_Control();

                    //foreach(var idx in selectedItemIndexes)
                    await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);

                }
            }
        
        }


        #region MEMORY MANAGEMENT COMMANDS
        private async void BtnMemoryControl_Click(object sender, RoutedEventArgs e)
        {
            logUpdate = false;
            MessageBox.Text = "";
            LblCurrentFileInfo.Text = "";
            LblMemoryControl.Text = "";

            if (LV_NetworkNodes.Items.Count > 0)
            {
                
                if (LV_NetworkNodes.SelectedItems.Count == 1)
                {
                    // Works only for one selected device at a time
                    byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                    msg = Muse_Utils.Cmd_Memory_Control();
                    await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);

                }
            }
        }

        private async void BtnEraseMemory_Click(object sender, RoutedEventArgs e)
        {
            // Erase the memory content of all the selected devices
            if (LV_NetworkNodes.Items.Count > 0)
            {
                System.Windows.Style style = new System.Windows.Style();
                style.Setters.Add(new Setter(Xceed.Wpf.Toolkit.MessageBox.YesButtonContentProperty, "All"));
                style.Setters.Add(new Setter(Xceed.Wpf.Toolkit.MessageBox.NoButtonContentProperty, "Written"));
                style.Setters.Add(new Setter(Xceed.Wpf.Toolkit.MessageBox.CancelButtonContentProperty, "No"));
                MessageBoxResult result = Xceed.Wpf.Toolkit.MessageBox.Show("Are you sure to erase the memory?", "Memory erase", MessageBoxButton.YesNoCancel, MessageBoxImage.Warning, style);

                if(result == MessageBoxResult.Yes)
                {
                    byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                    msg = Muse_Utils.Cmd_Memory_Control(false, 1);

                    List<int> selectedItemIndexes = new List<int>();
                    foreach (object o in LV_NetworkNodes.SelectedItems)
                        selectedItemIndexes.Add(LV_NetworkNodes.Items.IndexOf(o));

                    foreach (var idx in selectedItemIndexes)
                        await BLENetwork[idx].SendMessage(msg);
      
                    System.Windows.Forms.MessageBox.Show("Memory erase process in progress!\nThe operation will erase all device memory.\nPlease wait until red LED stops blinking.",
                                "Memory Erase", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);

                }

                //else if (command == writtenCommand)
                else if (result == MessageBoxResult.No)
                {
                    byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                    msg = Muse_Utils.Cmd_Memory_Control(false, 0);

                    List<int> selectedItemIndexes = new List<int>();
                    foreach (object o in LV_NetworkNodes.SelectedItems)
                        selectedItemIndexes.Add(LV_NetworkNodes.Items.IndexOf(o));

                    foreach (var idx in selectedItemIndexes)
                        await BLENetwork[idx].SendMessage(msg);

                    System.Windows.Forms.MessageBox.Show("Memory erase process in progress!\nThe operation will erase the written memory of the device.\nPlease wait.",
                               "Memory Erase", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);

                }
                else
                {
                    // handle cancel command  --> no command
                }

                MessageBox.Text = "";
                LblCurrentFileInfo.Text = "";
                LblMemoryControl.Text = "";
            }
        }

        private async void BtnListUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (CBConnectedDevices.Items.Count > 0)
            {

                if (LV_NetworkNodes != null)
                {
                    if (LV_NetworkNodes.SelectedItems.Count == 1)
                    {

                        // Access to file info of one file / one device at a time
                        if (comboBoxBleFile.SelectedIndex >= 0)
                        {
                            UpdateLogFiles();
                        }
                    }
                }
            }
        }

        private async void BtnFileInfo_Click(object sender, RoutedEventArgs e)
        {
            if (CBConnectedDevices.Items.Count > 0)
            {

                if (LV_NetworkNodes != null)
                {
                    if (LV_NetworkNodes.SelectedItems.Count == 1)
                    {

                        // Access to file info of one file / one device at a time
                        if (comboBoxBleFile.SelectedIndex >= 0)
                        {
                            byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                            msg = Muse_Utils.Cmd_MemoryFileInfo(comboBoxBleFile.SelectedIndex);

                            //await mpRef.BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);

                            await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);
                        }
                    }
                }
            }
        }


        private async Task<bool> ManageReadAll()
        {

            FolderBrowserDialog folderDialog = new FolderBrowserDialog();

            if (folderDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string selectedPath = folderDialog.SelectedPath;

                if (selectedPath != "")
                {
                    selectedPath = selectedPath + "\\";

                    MessageBox.Text = "Picked folder: " + selectedPath;

                    for (int item = 0; item < comboBoxBleFile.Items.Count; item++)
                    {
                        string file_name = selectedPath + (comboBoxBleFile.Items[item].ToString().Replace("/", "-") + ".txt").Replace(":", "");

                        bool write = true;

                        if (File.Exists(file_name))
                        {
                            var result = System.Windows.Forms.MessageBox.Show("File" + (comboBoxBleFile.Items[item].ToString().Replace("/", "-") + ".txt").Replace(":", "") + "already exists. Overwrite it?",
                                "File already exists", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                            if ( result == System.Windows.Forms.DialogResult.No)
                            {
                                write = false;
                            }
                        }
                        if (write)
                        {
                            try
                            {
                                FileStream fs = System.IO.File.Open(file_name, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);

                                var file = new StreamWriter(fs);
                                readFile = true;

                                byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                                msg = Muse_Utils.Cmd_MemoryFileInfo(item); // get time and file info 

                                // file

                                while (BLENetwork[LV_NetworkNodes.SelectedIndex].fileTimestamp == processedFileName || BLENetwork[LV_NetworkNodes.SelectedIndex].fileTimestamp == new DateTime())
                                    await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);

                                processedFileName = BLENetwork[LV_NetworkNodes.SelectedIndex].fileTimestamp;

                                // FILE HEADER
                                string hline = "";
                                hline = "INFORMATION\n\n";

                                hline += "Date:\t" + BLENetwork[LV_NetworkNodes.SelectedIndex].fileTimestamp.ToString("dd/MM/yyyy") + "\n";
                                hline += "Time:\t" + BLENetwork[LV_NetworkNodes.SelectedIndex].fileTimestamp.ToString("HH:mm:ss") + "\n";

                                //hline += "Firmware Version\t" + PagesGateway.ConfigPg.FirmwareVersion.ToString() + "\n";

                                hline += "Mode:\t" + BLENetwork[LV_NetworkNodes.SelectedIndex].modeStr + "\n";
                                hline += "Freq:\t" + BLENetwork[LV_NetworkNodes.SelectedIndex].freqStr + "\n";

                                hline += "Gyr FS: " + BLENetwork[LV_NetworkNodes.SelectedIndex].full_scales.gyr_fsStr + ";\t" +
                                    "Axl FS: " + BLENetwork[LV_NetworkNodes.SelectedIndex].full_scales.axl_fsStr + ";\t" +
                                    "Mag FS: " + BLENetwork[LV_NetworkNodes.SelectedIndex].full_scales.mag_fsStr + ";\t" +
                                    "Hdr FS: " + BLENetwork[LV_NetworkNodes.SelectedIndex].full_scales.hdr_fsStr + "\n\n";

                                hline += "DATA\n\n";

                                if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_TIMESTAMP) > 0)
                                    hline += "Timestamp [ms][dev]";
                                else
                                    hline += "Timestamp [ms][pc]";

                                if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_GYRO) > 0)
                                    hline += "\tGyr.X\tGyr.Y\tGyr.Z";
                                if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_AXL) > 0)
                                    hline += "\tAxl.X\tAxl.Y\tAxl.Z";
                                if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_MAGN) > 0)
                                    hline += "\tMag.X\tMag.Y\tMag.Z";
                                if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_HDR) > 0)
                                    hline += "\tHdr.X\tHdr.Y\tHdr.Z";
                                if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_ORIENTATION) > 0)
                                    hline += "\tq.w\tq.i\tq.j\tq.k";
                                if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_TEMP_HUM) > 0)
                                    hline += "\tTemp\tHum";
                                if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_TEMP_PRESS) > 0)
                                    hline += "\tTemp\tPress";
                                //if ((mpRef.BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Acquisition_Type.DATA_MODE_RANGE) > 0)
                                if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_ENCODER) > 0)
                                    hline += "\tENC.x\tENC.y\tENC.z\n";

                                file.WriteLine(hline);

                                msg = Muse_Utils.Cmd_MemoryFileDownload(item, 0x01);
                                await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);

                                while (BLENetwork[LV_NetworkNodes.SelectedIndex].num_of_chunks == 0 || !BLENetwork[LV_NetworkNodes.SelectedIndex].end_of_file_offload)
                                    await Task.Delay(25);


                                string line = "";

                                BLENode.acquisitionList.ForEach(delegate (Muse_Data data)
                                {
                                    if(data != null)
                                    {
                                        line = data.getTimestampString();
                                        if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_GYRO) > 0)
                                        {
                                            line += "\t" + data.getGyroString();
                                        }
                                        if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_AXL) > 0)
                                        {
                                            line += "\t" + data.getAXLString();
                                        }
                                        if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_MAGN) > 0)
                                        {
                                            line += "\t" + data.getMagnString();
                                        }
                                        if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_HDR) > 0)
                                        {
                                            line += "\t" + data.getHDRAXLString();
                                        }
                                        if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_ORIENTATION) > 0)
                                        {
                                            line += "\t" + data.getOrientationString();
                                        }
                                        if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_ENCODER) > 0)
                                        {
                                            line += "\t" + data.getENCString();
                                        }
                                        if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_TEMP_HUM) > 0)
                                        {
                                            line += "\t" + data.getTHString();
                                        }
                                        if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_TEMP_PRESS) > 0)
                                        {
                                            line += "\t" + data.getTPString();
                                        }
                                        if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_RANGE) > 0)
                                        {
                                            line += "\t" + data.getRangeString();
                                        }

                                        line = line.Replace(',', '.');
                                        file.WriteLine(line);
                                    }
                                    
                                });

                                
                                file.Close();

                                BLENode.acquisitionList = new List<Muse_Data>();

                                MessageBox.Text = "File " + (item + 1).ToString() + "/" + comboBoxBleFile.Items.Count + ": " + (BLENetwork[LV_NetworkNodes.SelectedIndex].fileTimestamp.ToString("dd/MM/yyyy HH:mm:ss").Replace("/", "-") + ".txt").Replace(":", "") + " saved.";

                                BLENetwork[LV_NetworkNodes.SelectedIndex].end_of_file_offload = false;
                                BLENetwork[LV_NetworkNodes.SelectedIndex].fileTimestamp = new DateTime();
                            }
                            catch (IOException ex)
                            {
                                System.Windows.Forms.MessageBox.Show("File in use. Please close it.",
                                "File in use", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }

                        }

                    }

                }
                else
                {
                    MessageBox.Text = "Operation cancelled.";
                    return false;
                }

            }

            
            return true;
        }

        private async void BtnReadAll_Click(object sender, RoutedEventArgs e)
        {
            if (CBConnectedDevices.Items.Count > 0)
            {
                //Add selected device to connectable list
                List<BluetoothLEDeviceDisplay> tmp = new List<BluetoothLEDeviceDisplay>();
                foreach (var item in CBConnectedDevices.SelectedItems)
                        tmp.Add(item as BluetoothLEDeviceDisplay);

                // Device connection management
                bool result = await ConnectBleDevice(tmp);

                if (LV_NetworkNodes != null)
                {
                    if (LV_NetworkNodes.SelectedItems.Count == 1)
                    {
                        await ManageReadAll();
                    }
                }
            }

        }

        private async Task<bool> ManageReadFile()
        {

            FolderBrowserDialog folderDialog = new FolderBrowserDialog();

            if (folderDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string selectedPath = folderDialog.SelectedPath;

                if (selectedPath != "")
                {
                    selectedPath = selectedPath + "\\";

                    MessageBox.Text = "Picked folder: " + selectedPath;
                        readFile = true;

                        byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                        msg = Muse_Utils.Cmd_MemoryFileInfo(comboBoxBleFile.SelectedIndex); // get time and file info 

                        // file
                        while (BLENetwork[LV_NetworkNodes.SelectedIndex].fileTimestamp == processedFileName || BLENetwork[LV_NetworkNodes.SelectedIndex].fileTimestamp == new DateTime())
                            await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);

                        processedFileName = BLENetwork[LV_NetworkNodes.SelectedIndex].fileTimestamp;

                        string file_name = selectedPath + (BLENetwork[LV_NetworkNodes.SelectedIndex].fileTimestamp.ToString("dd/MM/yyyy HH:mm:ss").Replace("/", "-") + ".txt").Replace(":", "");

                        bool write = true;

                        if (File.Exists(file_name))
                        {
                            if (System.Windows.Forms.MessageBox.Show("File" + (BLENetwork[LV_NetworkNodes.SelectedIndex].fileTimestamp.ToString("dd/MM/yyyy HH:mm:ss").Replace("/", "-") + ".txt").Replace(":", "")  + "already exists. Overwrite it?",
                                "File already exists", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.No)
                            {
                                write = false;
                            }
                        }
                        if (write)
                        {
                        try
                        {
                            FileStream fs = System.IO.File.Open(file_name, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);

                            var file = new StreamWriter(fs);


                            // FILE HEADER
                            string hline = "";
                            hline = "INFORMATION\n\n";

                            hline += "Date:\t" + BLENetwork[LV_NetworkNodes.SelectedIndex].fileTimestamp.ToString("dd/MM/yyyy") + "\n";
                            hline += "Time:\t" + BLENetwork[LV_NetworkNodes.SelectedIndex].fileTimestamp.ToString("HH:mm:ss") + "\n";

                            //hline += "Firmware Version\t" + PagesGateway.ConfigPg.FirmwareVersion.ToString() + "\n";

                            hline += "Mode:\t" + BLENetwork[LV_NetworkNodes.SelectedIndex].modeStr + "\n";
                            hline += "Freq:\t" + BLENetwork[LV_NetworkNodes.SelectedIndex].freqStr + "\n";

                            hline += "Gyr FS: " + BLENetwork[LV_NetworkNodes.SelectedIndex].full_scales.gyr_fsStr + ";\t" +
                                "Axl FS: " + BLENetwork[LV_NetworkNodes.SelectedIndex].full_scales.axl_fsStr + ";\t" +
                                "Mag FS: " + BLENetwork[LV_NetworkNodes.SelectedIndex].full_scales.mag_fsStr + ";\t" +
                                "Hdr FS: " + BLENetwork[LV_NetworkNodes.SelectedIndex].full_scales.hdr_fsStr + "\n\n";

                            hline += "DATA\n\n";

                            if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_TIMESTAMP) > 0)
                                hline += "Timestamp [ms][dev]";
                            else
                                hline += "Timestamp [ms][pc]";

                            if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_GYRO) > 0)
                                hline += "\tGyr.X\tGyr.Y\tGyr.Z";
                            if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_AXL) > 0)
                                hline += "\tAxl.X\tAxl.Y\tAxl.Z";
                            if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_MAGN) > 0)
                                hline += "\tMag.X\tMag.Y\tMag.Z";
                            if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_HDR) > 0)
                                hline += "\tHdr.X\tHdr.Y\tHdr.Z";
                            if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_ORIENTATION) > 0)
                                hline += "\tq.w\tq.i\tq.j\tq.k";
                            if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_TEMP_HUM) > 0)
                                hline += "\tTemp\tHum";
                            if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_TEMP_PRESS) > 0)
                                hline += "\tTemp\tPress";
                            //if ((mpRef.BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Acquisition_Type.DATA_MODE_RANGE) > 0)
                            if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_ENCODER) > 0)
                                hline += "\tENC.x\tENC.y\tENC.z\n";

                            file.WriteLine(hline);

                            msg = Muse_Utils.Cmd_MemoryFileDownload(comboBoxBleFile.SelectedIndex, 0x01);
                            await BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);

                            while (BLENetwork[LV_NetworkNodes.SelectedIndex].num_of_chunks == 0 || !BLENetwork[LV_NetworkNodes.SelectedIndex].end_of_file_offload)
                                await Task.Delay(25);


                            string line = "";

                            BLENode.acquisitionList.ForEach(delegate (Muse_Data data)
                            {
                                line = data.getTimestampString();
                                if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_GYRO) > 0)
                                {
                                    line += "\t" + data.getGyroString();
                                }
                                if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_AXL) > 0)
                                {
                                    line += "\t" + data.getAXLString();
                                }
                                if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_MAGN) > 0)
                                {
                                    line += "\t" + data.getMagnString();
                                }
                                if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_HDR) > 0)
                                {
                                    line += "\t" + data.getHDRAXLString();
                                }
                                if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_ORIENTATION) > 0)
                                {
                                    line += "\t" + data.getOrientationString();
                                }
                                if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_ENCODER) > 0)
                                {
                                    line += "\t" + data.getENCString();
                                }
                                if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_TEMP_HUM) > 0)
                                {
                                    line += "\t" + data.getTHString();
                                }
                                if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_TEMP_PRESS) > 0)
                                {
                                    line += "\t" + data.getTPString();
                                }
                                if ((BLENetwork[LV_NetworkNodes.SelectedIndex].acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_RANGE) > 0)
                                {
                                    line += "\t" + data.getRangeString();
                                }

                                line = line.Replace(',', '.');
                                file.WriteLine(line);
                            });


                            file.Close();
                            BLENode.acquisitionList = new List<Muse_Data>();

                            MessageBox.Text = "File " + (BLENetwork[LV_NetworkNodes.SelectedIndex].fileTimestamp.ToString("dd/MM/yyyy HH:mm:ss").Replace("/", "-") + ".txt").Replace(":", "") + " saved.";

                            BLENetwork[LV_NetworkNodes.SelectedIndex].end_of_file_offload = false;
                            BLENetwork[LV_NetworkNodes.SelectedIndex].fileTimestamp = new DateTime();
                        }
                        catch (IOException ex)
                        {
                            System.Windows.Forms.MessageBox.Show("File in use. Please close it.",
                            "File in use", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }

                    }


                }
                else
                {
                    MessageBox.Text = "Operation cancelled.";
                    return false;
                }

            }


            return true;
        }

        private async void BtnReadFile_Click(object sender, RoutedEventArgs e)
        {
            if (CBConnectedDevices.Items.Count > 0)
            {
                //Add selected device to connectable list
                List<BluetoothLEDeviceDisplay> tmp = new List<BluetoothLEDeviceDisplay>();
                foreach (var item in CBConnectedDevices.SelectedItems)
                        tmp.Add(item as BluetoothLEDeviceDisplay);

                // Device connection management
                bool result = await ConnectBleDevice(tmp);

                if (LV_NetworkNodes != null)
                {
                   if (LV_NetworkNodes.SelectedItems.Count == 1)
                    {
                        await ManageReadFile();
                    }
                }
            }

        }
        #endregion

        int calibrationIndex = 0;
        private void CBCalibMatrix_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (CBCalibMatrix.SelectedIndex)
            {
                case 0:
                    calibrationIndex = CBCalibMatrix.SelectedIndex;
                    CBCalibMatrix.Text = "Accelerometer";
                    break;
                case 1:
                    calibrationIndex = CBCalibMatrix.SelectedIndex;
                    CBCalibMatrix.Text = "Gyroscope";
                    break;
                case 2:
                    calibrationIndex = CBCalibMatrix.SelectedIndex;
                    CBCalibMatrix.Text = "Magnetometer";
                    break;
                default:
                    calibrationIndex = 0;
                    break;
            }
        }

        private void BtnDeviceInfo_Click(object sender, RoutedEventArgs e)
        {
            if (LV_NetworkNodes != null)
            {
                
                if (LV_NetworkNodes.SelectedItems.Count == 1)
                {
                    byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                    msg = Muse_Utils.Cmd_GetUserConfiguration();
                    BLENetwork[LV_NetworkNodes.SelectedIndex].SendMessage(msg);

                    updateStatus();
                    UpdateLogFiles();
                }
            }
        }
    }

}
