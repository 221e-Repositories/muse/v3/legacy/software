﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using _221e.Muse;

namespace MuseV3Viewer
{
    public struct Device
    {
        public string COM;
        public USBNode Connection;
    }

    public partial class USB : Window
    {
        string fw_version;

        private string selectedCOMPort;

        bool read_all = false;
        UInt16 currentFileIndex = 0;

        FileStream fs = null;

        USBNode connection;

        public USB()
        {
            InitializeComponent();
            this.DataContext = this;

            UpdateDevicesList();

        }

        private void ButtonFechar_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void btnMaximize_Click(object sender, RoutedEventArgs e)
        {
            switch (this.WindowState)
            {
                case WindowState.Normal:
                    this.WindowState = WindowState.Maximized;
                    break;
                case WindowState.Maximized:
                    this.WindowState = WindowState.Normal;
                    break;
            }
        }

        private void btnMinimize_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void GridBarraTitulo_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }


        private void Ble_Click(object sender, MouseButtonEventArgs e)
        {
            if (attemptToShutDown())
                lblStatus.Text = selectedCOMPort + " closed.";

            BLE win2 = new BLE();
            win2.Show();
            win2.WindowState = this.WindowState;
            this.Close();

            
        }

        private void UpdateDevicesList()
        {

            // Search for any device having a Vendor ID = 0x0483 and Product ID = 0x5740
            List<string> portNumbers = new List<string>();

            try
            {
                using (var searcher = new System.Management.ManagementObjectSearcher("root\\CIMV2", "SELECT Name, DeviceID FROM Win32_PnPEntity WHERE DeviceID like '%VID_0483&PID_5740%'"))
                    foreach (System.Management.ManagementBaseObject queryObj in searcher.Get())
                    {
                        string temp_name = queryObj["Name"].ToString();
                        if (temp_name.Contains("COM"))
                        {
                            temp_name = temp_name.Split(new char[] { '(', ')' }, 3)[1];
                            portNumbers.Add(temp_name);
                        }
                    }
            }
            catch (Exception e)
            {
            }

            portNumbers.Sort();

            cmbDev.ItemsSource = new BindingSource(portNumbers, null);
            cmbDev.SelectedIndex = -1;

        }

        #region BUTTON SECTION

        private void BtnConnect_Click(object sender, RoutedEventArgs e)
        {
            if (cmbDev.SelectedIndex != -1)
            {
                selectedCOMPort = cmbDev.SelectedItem.ToString();

                lblStatus.Text = "Initializing " + selectedCOMPort;

                if (attemptToConnect())
                {
                    Muse_HW.SystemState s = (Muse_HW.SystemState)connection.getState();
                    connection.packetUploaded += new Muse_packetUploaded(updatePacketUpload);
                    connection.newLogSample += new Muse_pageDownloaded(updateFileDownload);

                    if (s == Muse_HW.SystemState.SYS_IDLE || s == Muse_HW.SystemState.SYS_LOG)
                    updateStatus();
                    else
                        lblStatus.Text = "Error! Please set the system in configuration state and restart the application!";

                }

            }
        }

        private void BtnDisconnect_Click(object sender, RoutedEventArgs e)
        {
            if (attemptToShutDown())
                lblStatus.Text = selectedCOMPort + " closed.";

            LblCurrentState.Text = "---";
            LblBatteryCharge.Text = "---";
            LblMemory.Text = "---";
            LblFirmwareVersion.Text = "---";
            LblDeviceId.Text = "---";
            LblDateTime.Text = "---";
        }

        private void BtnGetState_Click(object sender, RoutedEventArgs e)
        {
            LblCurrentState.Text = Muse_Utils.GetDeviceStateString(connection.getState());
        }

        private void BtnGetBatteryCharge_Click(object sender, RoutedEventArgs e)
        {
            LblBatteryCharge.Text = String.Format("{0} %", connection.getBatteryCharge());
        }

        private void BtnGetMemory_Click(object sender, RoutedEventArgs e)
        {
            LblMemory.Text = String.Format("{0} %", connection.getMemoryStatus());
        }

        private void BtnGetFirmwareVersion_Click(object sender, RoutedEventArgs e)
        {
            //firmware version
            string[] temp_versions = connection.getFW();
            string boot_fw_version = temp_versions[0];
            fw_version = temp_versions[1];
            LblFirmwareVersion.Text = boot_fw_version + " " + fw_version;
        }

        private void BtnGetDeviceId_Click(object sender, RoutedEventArgs e)
        {
            LblDeviceId.Text = connection.getID();
        }

        private void BtnGetDateTime_Click(object sender, RoutedEventArgs e)
        {
            LblDateTime.Text = connection.getDate().ToString();
        }


        private bool attemptToConnect()
        {

            try
            {
                connection = new USBNode(selectedCOMPort);
                return true;
            }
            catch (Exception ex)
            {
                //Display problems!
                showWarningDialog();
                return false;
            }
        }

        private bool attemptToShutDown()
        {

            lblStatus.Text = "Shutting down selected device";
            if(connection != null)
                connection.closeConnection();
            return true;
        }

        private void showWarningDialog()
        {
            lblStatus.Text = "Error! Problems while opening COM port.";
        }


        private void updatePacketUpload(object sender, int value, bool state)
        {
            if (state)
            {
                //Update the progress bar
                if (value >= 100)
                    lblStatus.Text = "Firmware uploaded successfully!";
                else
                    lblStatus.Text = "Firmware upload: " + value;
            }
            else
                lblStatus.Text = "Firmware upload error!";

        }

        private void buttonBrowse_Click(object sender, EventArgs e)
        {
            // Displays an OpenFileDialog so the user can select a Cursor.
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Binary files|*.bin";
            openFileDialog1.Title = "Select a binary file";

            // Show the Dialog.
            // If the user clicked OK in the dialog and
            // a .CUR file was selected, open it.
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                fs = new FileStream(openFileDialog1.FileName,
                                           FileMode.Open,
                                           FileAccess.Read);
            }
        }

        private void buttonStartApp_Click(object sender, EventArgs e)
        {
            connection.startApplication();
            System.Threading.Thread.Sleep(500);
            connection.closeConnection();
            lblStatus.Text = "Shutting down selected device";
            connection.closeConnection();

            cmbDev.SelectedIndex = -1;

            lblStatus.Text = "Ready";
        }


        private void errorReadout(object sender, EventArgs e)
        {
            updateStatus();
        }

        string device_id = "";

        public void updateStatus()
        {
            if (connection != null)
            {
                // state
                LblCurrentState.Text = Muse_Utils.GetDeviceStateString(connection.getState());
                System.Threading.Thread.Sleep(100);

                // battery
                LblBatteryCharge.Text = String.Format("{0} %", connection.getBatteryCharge());
                System.Threading.Thread.Sleep(100);

                //memory
                int memory = connection.getMemoryStatus();
                LblMemory.Text = String.Format("{0} %", memory);

                //files list
                System.Threading.Thread.Sleep(100);
                if (memory == 100 && comboBoxFile.Items.Count > 0)
                {
                    comboBoxFile.Items.Clear();
                }
                comboBoxFile.ItemsSource = connection.file_list;

                //firmware version
                string[] temp_versions = connection.getFW();
                string boot_fw_version = temp_versions[0];
                fw_version = temp_versions[1];
                LblFirmwareVersion.Text = boot_fw_version + " " + fw_version;

                //ID readout
                device_id = connection.getID();
                LblDeviceId.Text = device_id;

                // date
                LblDateTime.Text = connection.getDate().ToString();

                lblStatus.Text = "Ready.";
            }

        }

        private async void updateFileDownload(object sender, int value)
        {
            if (read_all && connection.file_list.Count != 0) {

                value = value * (currentFileIndex + 1) / connection.file_list.Count;
                //Update the progress bar
                if (value >= 100)
                {
                    await Dispatcher.InvokeAsync(() => lblStatus.Text = "File " + currentFileIndex + "/" + connection.file_list.Count + " downloaded.");
                    await Dispatcher.InvokeAsync(() => writeAcquisition());

                }
                else
                {
                    if (value > 0 && value <= 100)
                        await Dispatcher.InvokeAsync(() => lblStatus.Text = "File " + currentFileIndex + "/" + connection.file_list.Count + " download: " + value.ToString() + " %");


                }
            }
            else
            {
                //Update the progress bar
                if (value >= 100)
                {
                    await Dispatcher.InvokeAsync(() => lblStatus.Text = "File downloaded.");
                    Dispatcher.BeginInvoke(new MethodInvoker(() => writeAcquisition()));

                }
                else
                {
                    if (value > 0 && value <= 100)
                        await Dispatcher.InvokeAsync(() => lblStatus.Text = "File download: " + value.ToString() + " %");


                }
            }
                

           
        }

        private void BtnEraseDevice_Click(object sender, EventArgs e)
        {
            if (connection != null && connection.eraseMemory(1))
                lblStatus.Text = "Memory erased correctly";
            else
                lblStatus.Text = "Problem during memory erase!";
            
        }

        private void BtnFileToLocal_Click(object sender, EventArgs e)
        {
            if (connection != null && !connection.acqLog)
            {
                connection.tempQueue.Clear();
                if (!connection.readFile((UInt16)(comboBoxFile.SelectedIndex)))
                {
                    System.Windows.Forms.MessageBox.Show("File too short!",
                                    "Error",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                }

            }
        }


        private void BtnAllToLocal_Click(object sender, EventArgs e)
        {
            if (connection != null && !connection.acqLog && connection.file_list.Count != 0)
            {
                read_all = true;
                connection.tempQueue.Clear();
                for (currentFileIndex = 0; currentFileIndex < connection.file_list.Count; currentFileIndex++)
                {
                    comboBoxFile.SelectedIndex = currentFileIndex;
                    if (!connection.readFile((UInt16)(comboBoxFile.SelectedIndex)))
                    {
                        System.Windows.Forms.MessageBox.Show("File too short!",
                                        "Error",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);
                    }
                    while (connection.acqLog) ;

                }
                //read_all = false;
            }
        }

        private void storeData(string path)
        {
            // Salvo i dati
            string fileName = "";
            string[] headerLines = new string[7];
            // if la coda non è vuota

            foreach (var acqStamped in connection.tempQueue)
            {
                comboBoxFile.SelectedIndex = acqStamped.id;
                DateTime parsedDate = DateTime.ParseExact(comboBoxFile.SelectedItem.ToString(), "dd/MM/yyyy HH:mm:ss", null);
                string line = "";
                headerLines[0] = "Information\t\t\t\t\t\t\t\t";
                headerLines[1] = "Date\t" + parsedDate.ToString("dd/MM/yyyy") + "\t\t\t\t\t\t\t";
                headerLines[2] = "Time\t" + parsedDate.ToString("HH:mm:ss") + "\t\t\t\t\t\t\t";
                headerLines[3] = "Firmware Version\t" + fw_version + "\t\t\t\t\t\t\t";
                headerLines[4] = "Mode\t" + acqStamped.log_mode + "\t\t\t\t\t\t\t";
                headerLines[5] = "Sampling frequency [Hz]\t" + acqStamped.log_frequency.ToString() + " Hz\t\t\t\t\t\t\t";
                headerLines[6] = "Gyr FS:\t" + acqStamped.fs[0].ToString() + ";\t" +
                                "Axl FS: " + acqStamped.fs[1].ToString() + ";\t" +
                                "Mag FS: " + acqStamped.fs[2].ToString() + ";\t" +
                                "Hdr FS: " + acqStamped.fs[3].ToString() + "\t\t\t\t\t\t\t";


                //Header build up

                UInt32 temp_mode = acqStamped.file_mode;

                if ((temp_mode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_TIMESTAMP) > 0)
                    line = "Timestamp [ms][dev]";
                else
                    line = "Timestamp [ms][pc]";

                
                if ((temp_mode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_GYRO) > 0)
                {
                    line += "\tGyr.X\tGyr.Y\tGyr.Z";
                }
                if ((temp_mode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_AXL) > 0)
                {
                    line += "\tAxl.X\tAxl.Y\tAxl.Z";
                }
                if ((temp_mode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_HDR) > 0)
                {
                    line += "\tHDR.X\tHDR.Y\tHDR.Z";
                }
                if ((temp_mode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_MAGN) > 0)
                {
                    line += "\tMag.X\tMag.Y\tMag.Z";
                }
                if ((temp_mode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_ORIENTATION) > 0)
                {
                    line += "\tq.w\tq.i\tq.j\tq.k";
                }
                if ((temp_mode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_ENCODER) > 0)
                {
                    line += "\tENC.x\tENC.y\tENC.z";
                }
                if ((temp_mode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_TEMP_HUM) > 0)
                {
                    line += "\tTEMP\tRH";
                }
                if ((temp_mode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_TEMP_PRESS) > 0)
                {
                    line += "\tTEMP\tPressure";
                }
                if ((temp_mode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_RANGE) > 0)
                {
                    line += "\tLum VIS\tLum IR\tRange";
                }

                fileName = path + (comboBoxFile.SelectedItem.ToString().Replace("/", "-") + ".txt").Replace(":", "");
                bool write = true;

                if (System.IO.File.Exists(fileName))
                {
                    if (System.Windows.Forms.MessageBox.Show("File" + (comboBoxFile.SelectedItem.ToString().Replace("/", "-") + ".txt").Replace(":", "") + "already exists. Overwrite it?",
                        "File already exists", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.No)
                    {
                        write = false;
                    }
                }
                if (write)
                {
                    try
                    {
                        FileStream fs = System.IO.File.Open(fileName, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);

                        var file = new StreamWriter(fs);

                        //Write the header for the data download, otherwise write the header for the error log
                        for (int i = 0; i < headerLines.Length; i++)
                            file.WriteLine(headerLines[i]);

                        file.WriteLine(line);
                        System.Threading.Thread.Sleep(1000);
                        acqStamped.acquisitionList.ForEach(delegate (Muse_Data data)
                        {
                            line = data.getTimestampString();
                            if ((temp_mode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_GYRO) > 0)
                            {
                                line += "\t" + data.getGyroString();
                            }
                            if ((temp_mode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_AXL) > 0)
                            {
                                line += "\t" + data.getAXLString();
                            }
                            if ((temp_mode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_MAGN) > 0)
                            {
                                line += "\t" + data.getMagnString();
                            }
                            if ((temp_mode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_HDR) > 0)
                            {
                                line += "\t" + data.getHDRAXLString();
                            }
                            if ((temp_mode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_ORIENTATION) > 0)
                            {
                                line += "\t" + data.getOrientationString();
                            }
                            if ((temp_mode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_ENCODER) > 0)
                            {
                                line += "\t" + data.getENCString();
                            }
                            if ((temp_mode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_TEMP_HUM) > 0)
                            {
                                line += "\t" + data.getTHString();
                            }
                            if ((temp_mode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_TEMP_PRESS) > 0)
                            {
                                line += "\t" + data.getTPString();
                            }
                            if ((temp_mode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_RANGE) > 0)
                            {
                                line += "\t" + data.getRangeString();
                            }

                            line = line.Replace(',', '.');
                            file.WriteLine(line);
                        });
                        file.Close();
                        lblStatus.Text = "File " + (comboBoxFile.SelectedItem.ToString().Replace("/", "-") + ".txt").Replace(":", "") + " saved.";
                    }
                    catch (IOException ex)
                    {
                        System.Windows.Forms.MessageBox.Show("File in use. Please close it.",
                        "File in use", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }


            }

        }

        }


        private void writeAcquisition()
        {
            FolderBrowserDialog folderDialog = new FolderBrowserDialog();

            if (folderDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string selectedPath = folderDialog.SelectedPath;

                if (selectedPath != "")
                {
                    try
                    {
                        string storage_path = selectedPath + "\\";
                        storeData(storage_path);
                        System.Threading.Thread.Sleep(1000);
                        read_all = false;

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("The process failed: {0}", e.ToString());
                    }
                    finally { }
                }
                else
                {
                    lblStatus.Text = "Operation cancelled.";
                }
            }
            
        }


        private void BtnCom_Click(object sender, RoutedEventArgs e)
        {
            UpdateDevicesList();
        }

        private void BtnDeviceInfo_Click(object sender, RoutedEventArgs e)
        {
            updateStatus();
        }


        #endregion

    }

}
