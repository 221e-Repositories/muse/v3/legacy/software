﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Forms.Integration;
using System.Windows.Interop;
using ZedGraph;


namespace MuseV3Viewer
{
    public partial class LineChartWindow : Window
    {
        private const int GWL_STYLE = -16;
        private const int WS_SYSMENU = 0x80000;
        [DllImport("user32.dll", SetLastError = true)]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);

        private int nSeries_;
        private string[] seriesLabels_;
        private string xTitle_;
        private string yTitle_;
        //private int[] xRange_;
        //private int[] yRange_;

        public ZedGraphControl chartControl_;
        private TextObj textObj;

        public LineChartWindow(int numOfSeries, string[] seriesLabels, string xAxisTitle, string yAxisTitle) //, int[] xRange, int[] yRange)
        {
            InitializeComponent();

            nSeries_ = numOfSeries;
            seriesLabels_ = seriesLabels;
            xTitle_ = xAxisTitle;
            yTitle_ = yAxisTitle;
            //xRange_ = xRange;
            //yRange_ = yRange;

            chartControl_ = new ZedGraphControl();
            textObj = new TextObj("", 1, 1.18, CoordType.ChartFraction, AlignH.Right, AlignV.Bottom);
            textObj.FontSpec = new FontSpec("Segoe UI", 14, System.Drawing.Color.White, false, false, false,
                                            System.Drawing.Color.FromArgb(0, 200, 200, 200),
                                            new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(0, 200, 200, 200)),
                                            FillType.None);
            textObj.FontSpec.Border = new ZedGraph.Border(true, System.Drawing.Color.Gray, 1f);

            InitChart();
            ClearChart();

            if (numOfSeries == 4)
                textObj.IsVisible = true;
        }

        public void chartWindow_Loaded(object sender, RoutedEventArgs e)
        {
            // Hide CLOSE button
            //var hwnd = new WindowInteropHelper(this).Handle;
            //SetWindowLong(hwnd, GWL_STYLE, GetWindowLong(hwnd, GWL_STYLE) & ~WS_SYSMENU);

            // Setup windows form host
            WindowsFormsHost host = new WindowsFormsHost();
            host.Child = chartControl_;
            chartMainGrid.Children.Add(host);
        }

        public void InitChart()
        {
            // General
            chartControl_.GraphPane.Chart.Border.IsVisible = false;
            chartControl_.GraphPane.Chart.Fill = new Fill(new System.Drawing.Color[] { System.Drawing.Color.FromArgb(70, 70, 70), System.Drawing.Color.FromArgb(10, 10, 10), System.Drawing.Color.FromArgb(70, 70, 70) }, 90);
            chartControl_.GraphPane.Fill = new Fill(System.Drawing.Color.FromArgb(70, 70, 70));
            chartControl_.GraphPane.Title.IsVisible = false;

            // X Axis
            chartControl_.GraphPane.XAxis.Color = System.Drawing.Color.LightGray;
            chartControl_.GraphPane.XAxis.MajorGrid.IsVisible = true;
            chartControl_.GraphPane.XAxis.MajorGrid.Color = System.Drawing.Color.Gray;
            chartControl_.GraphPane.XAxis.MajorTic.Color = System.Drawing.Color.Gray;
            chartControl_.GraphPane.XAxis.MinorTic.Color = System.Drawing.Color.Gray;
            chartControl_.GraphPane.XAxis.Scale.FontSpec = new FontSpec("Segoe UI", 12, System.Drawing.Color.White, false, false, false,
                                                                        System.Drawing.Color.FromArgb(0, 200, 200, 200),
                                                                        new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(0, 200, 200, 200)),
                                                                        FillType.None);
            chartControl_.GraphPane.XAxis.Scale.FontSpec.Border.IsVisible = false;
            chartControl_.GraphPane.XAxis.Scale.Min = 0;
            chartControl_.GraphPane.XAxis.Scale.Max = 100;
            //chartControl_.GraphPane.XAxis.Scale.MinAuto = true;
            //chartControl_.GraphPane.XAxis.Scale.MaxAuto = true;
            chartControl_.GraphPane.XAxis.Title.IsVisible = false;

            // Y Axis
            chartControl_.GraphPane.YAxis.Color = System.Drawing.Color.LightGray;
            chartControl_.GraphPane.YAxis.MajorGrid.IsVisible = true;
            chartControl_.GraphPane.YAxis.MajorGrid.Color = System.Drawing.Color.Gray;
            chartControl_.GraphPane.YAxis.MajorTic.Color = System.Drawing.Color.Gray;
            chartControl_.GraphPane.YAxis.MinorTic.Color = System.Drawing.Color.Gray;
            chartControl_.GraphPane.YAxis.Scale.FontSpec = new FontSpec("Segoe UI", 12, System.Drawing.Color.White, false, false, false,
                                                                        System.Drawing.Color.FromArgb(0, 200, 200, 200),
                                                                        new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(0, 200, 200, 200)),
                                                                        FillType.None);
            chartControl_.GraphPane.YAxis.Scale.FontSpec.Angle = 90;
            chartControl_.GraphPane.YAxis.Scale.FontSpec.Border.IsVisible = false;
            //chartControl_.GraphPane.YAxis.Scale.Min = yRange_[0];
            //chartControl_.GraphPane.YAxis.Scale.Max = yRange_[1];
            chartControl_.GraphPane.YAxis.Scale.MinAuto = true;
            chartControl_.GraphPane.YAxis.Scale.MaxAuto = true;

            chartControl_.GraphPane.YAxis.Title.IsVisible = false;


            // Legend
            chartControl_.GraphPane.Legend.Border = new ZedGraph.Border(true, System.Drawing.Color.Gray, 1f);
            chartControl_.GraphPane.Legend.Fill = new Fill(System.Drawing.Color.FromArgb(70, 70, 70));
            chartControl_.GraphPane.Legend.FontSpec = new FontSpec("Segoe UI", 14, System.Drawing.Color.White, false, false, false,
                                                                   System.Drawing.Color.FromArgb(255, 200, 200, 200),
                                                                   new System.Drawing.SolidBrush(System.Drawing.Color.FromArgb(255, 200, 200, 200)),
                                                                   FillType.None);
            chartControl_.GraphPane.Legend.FontSpec.Border.IsVisible = false;
            chartControl_.GraphPane.Legend.Position = LegendPos.Bottom;

            // Update chart
            chartControl_.GraphPane.AxisChange();

            // Add magnitude label
            chartControl_.GraphPane.GraphObjList.Add(textObj);


        }

        public void ClearChart()
        {
            switch (nSeries_)
            {
                case 1:
                    chartControl_.GraphPane.CurveList.Clear();
                    chartControl_.GraphPane.CurveList.Add(new LineItem(seriesLabels_[0], new PointPairList(), System.Drawing.Color.Red, SymbolType.None, 1.5f));
                    break;
                case 2:
                    chartControl_.GraphPane.CurveList.Clear();
                    chartControl_.GraphPane.CurveList.Add(new LineItem(seriesLabels_[0], new PointPairList(), System.Drawing.Color.Red, SymbolType.None, 1.5f));
                    chartControl_.GraphPane.CurveList.Add(new LineItem(seriesLabels_[1], new PointPairList(), System.Drawing.Color.Green, SymbolType.None, 1.5f));
                    break;
                case 3:
                    chartControl_.GraphPane.CurveList.Clear();
                    chartControl_.GraphPane.CurveList.Add(new LineItem(seriesLabels_[0], new PointPairList(), System.Drawing.Color.Red, SymbolType.None, 1.5f));
                    chartControl_.GraphPane.CurveList.Add(new LineItem(seriesLabels_[1], new PointPairList(), System.Drawing.Color.Green, SymbolType.None, 1.5f));
                    chartControl_.GraphPane.CurveList.Add(new LineItem(seriesLabels_[2], new PointPairList(), System.Drawing.Color.Blue, SymbolType.None, 1.5f));
                    break;
                case 4:
                    chartControl_.GraphPane.CurveList.Clear();
                    chartControl_.GraphPane.CurveList.Add(new LineItem(seriesLabels_[0], new PointPairList(), System.Drawing.Color.Orange, SymbolType.None, 1.5f));
                    chartControl_.GraphPane.CurveList.Add(new LineItem(seriesLabels_[1], new PointPairList(), System.Drawing.Color.Red, SymbolType.None, 1.5f));
                    chartControl_.GraphPane.CurveList.Add(new LineItem(seriesLabels_[2], new PointPairList(), System.Drawing.Color.Green, SymbolType.None, 1.5f));
                    chartControl_.GraphPane.CurveList.Add(new LineItem(seriesLabels_[3], new PointPairList(), System.Drawing.Color.Blue, SymbolType.None, 1.5f));
                    break;
            }
        }

        public void PlotData(int x, int y)
        {
            chartControl_.GraphPane.CurveList[0].AddPoint(x, y);
            chartControl_.Invalidate();
        }

        public void PlotData(int x, int[] y)
        {
            if (y.Length > 0 && y.Length <= 4)
            {
                for (int i = 0; i < y.Length; i++)
                    chartControl_.GraphPane.CurveList[i].AddPoint(x, y[i]);
                chartControl_.Invalidate();

                if (y.Length == 4)
                    textObj.Text = y[0].ToString();
            }
        }

        public void PlotData(float x, float y)
        {
            chartControl_.GraphPane.CurveList[0].AddPoint(x, y);
            chartControl_.Invalidate();
        }

        public void PlotData(float x, float[] y)
        {
            if (y.Length > 0 && y.Length <= 4)
            {
                for (int i = 0; i < y.Length; i++)
                {
                    chartControl_.GraphPane.CurveList[i].AddPoint(x, y[i]);

                }
                    
                chartControl_.Invalidate();

                if (y.Length == 4)
                    textObj.Text = y[0].ToString();
            }
        }
    }
}
