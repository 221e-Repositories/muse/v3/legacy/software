﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MuseV3Viewer
{
    class PagesGateway
    {
        public static BLE BlePg { get; set; }
        public static USB UsbPg { get; set; }
    }
}
