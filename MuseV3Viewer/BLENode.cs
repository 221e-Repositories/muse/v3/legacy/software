﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using System.Windows.Forms;
using _221e.Muse;
using Windows.Devices.Bluetooth;
using Windows.Devices.Bluetooth.GenericAttributeProfile;
using Windows.Devices.Enumeration;
using Windows.Security.Cryptography;
using Windows.Storage.Streams;
using static _221e.Muse.Muse_HW;

namespace MuseV3Viewer
{
    public class AxlReading
    {
        public DateTime AxlTick
        {
            get;
            set;
        }

        public float Axl_X
        {
            get;
            set;
        }
    }

    public class BLENode : INotifyPropertyChanged
    {
        public static int iterationCounter { get; set; }
        // public static List<Tuple<int, int, Muse_Data>> networkMessages = new List<Tuple<int, int, Muse_Data>>();
        public static bool enablePublishing { get; set; }


        #region ACQUISITION PROPERTIES
        public bool acquisitionStreamEnabled { get; set; }
        public bool acquisitionLogEnabled { get; set; }

        public bool enableView { get; set; }
        #endregion

        #region CALIBRATION PROPERTIES
        public Muse_HW.Calibration_Type cCalibrationType { get; set; }
        public bool calibrationStreamEnabled { get; set; }
        public List<float> calibChX { get; set; }
        public List<float> calibChY { get; set; }
        public List<float> calibChZ { get; set; }
        public float sumX { get; set; }
        public float sumY { get; set; }
        public float sumZ { get; set; }
        public int itCounter { get; set; }
        public float[] calibResult { get; set; }
        public bool enableUploadCalibration { get; set; }
        public bool enableDownloadCalibration { get; set; }
        public int calibColCounter { get; set; }
        #endregion

        // Variables used for upload binary fw
        int packetDim = 18;
        int total_NumberIteration_Upload = 0;
        int current_NumberIteration_Upload = 0;
        int parity_upload = 0;
        private int NUMBER_OF_PACKET_ITERATION = 12; // number of packet to send before recevice ack

        public bool enable_file_info_iteration = false;
        public int file_info_iterator = 0;
        public int num_of_files = 0;
        public bool empty_memory = false;

        private int packet_counter_data_ = 0;

        private int logPktsPerPage_ = 0;

        public int logFileSize_ = 0;
        private int logFilePackets_ = 0;

        public static List<Muse_Data> acquisitionList = new List<Muse_Data>();
        public static List<List<Muse_Data>> tempQueue = new List<List<Muse_Data>>();

        public List<Tuple<UInt64, byte[]>> acquisitionListRaw;

        //public List<Muse_Data> acquisitionList = new List<Muse_Data>();
        public List<string> dateList = new List<string>();
        UInt64 currentTimestamp = 0;

        public byte logMode = 0;
        UInt64 currentTime_val = 0;
        public UInt64 fileTime = 0;
        int packet_begin = 0, packet_end = 0, prev_packet_end = 0;

        public static List<Muse_Data> logData = new List<Muse_Data>();
        byte[] tmp = new byte[0];

        public List<int> log_mode_list = new List<int>();
        public List<int> log_freq_list = new List<int>();

        public List<int> axl_fs_list = new List<int>();
        public List<int> gyro_fs_list = new List<int>();
        public List<int> mag_fs_list = new List<int>();
        public List<int> hdr_fs_list = new List<int>();

        private int num_of_packets = 0;
        public int num_of_chunks = 0;

        DateTime T1;
        DateTime T4;
        int estimate_iterations = 10;
        int current_estimate_iterations = 0;
        public double result_estimate = 0;

        public bool EnablePublishing { get; set; }
        public string ChannelToBePublished { get; set; }

        private string streamString;
        public string StreamString
        {
            get { return streamString; }
            set
            {
                streamString = value;
                OnPropertyChanged("StreamString");
            }
        }

        private bool enableFileDownload;

        private static System.Timers.Timer aTimer;

        Data_Typedef mData;

        public BLENode(BluetoothLEDevice dev, ObservableCollection<BluetoothLEAttributeDisplay> serv, ObservableCollection<BluetoothLEAttributeDisplay> chars)
        {
            // Muse Data Typedef
            mData = new Data_Typedef();

            if (dev != null || serv != null || chars != null)
            {
                BLEDevice = dev;
                ServiceCollection = serv;
                CharacteristicCollection = chars;

                DeviceName = BLEDevice.Name;

                // Manage check before assignment
                SelectedService = ServiceCollection[2].service;

                SelectedCharacteristic_cmd = CharacteristicCollection[0].characteristic;
                SelectedCharacteristic_data = CharacteristicCollection[1].characteristic;

                CmdCharacteristicSubscribeButton_Click();
                DataCharacteristicSubscribeButton_Click();
            }

        }

        private void RemoveValueChangedHandlerCmd()
        {
            if (subscribedForNotifications_cmd)
            {
                foreach (var rc in registeredCharacteristic_cmd)
                    rc.ValueChanged -= CmdCharacteristic_ValueChanged;

                registeredCharacteristic_cmd = new ObservableCollection<GattCharacteristic>();
                subscribedForNotifications_cmd = false;
            }
        }

        private void RemoveValueChangedHandlerData()
        {
            if (subscribedForNotifications_data)
            {
                foreach (var rc in registeredCharacteristic_data)
                    rc.ValueChanged -= DataCharacteristic_ValueChanged;

                registeredCharacteristic_data = new ObservableCollection<GattCharacteristic>();
                subscribedForNotifications_data = false;
            }
        }

        public static async Task<ObservableCollection<BluetoothLEAttributeDisplay>> GetCharacteristics(GattDeviceService selectedService)
        {
            ObservableCollection<BluetoothLEAttributeDisplay> charsCollection = new ObservableCollection<BluetoothLEAttributeDisplay>();

            IReadOnlyList<GattCharacteristic> characteristics = null;
            try
            {
                // Ensure we have access to the device.
                var accessStatus = await selectedService.RequestAccessAsync();
                if (accessStatus == DeviceAccessStatus.Allowed)
                {
                    // BT_Code: Get all the child characteristics of a service. Use the cache mode to specify uncached characterstics only 
                    // and the new Async functions to get the characteristics of unpaired devices as well. 
                    var result = await selectedService.GetCharacteristicsAsync(BluetoothCacheMode.Uncached);
                    if (result.Status == GattCommunicationStatus.Success)
                    {
                        characteristics = result.Characteristics;
                    }
                    else
                    {
                        // On error, act as if there are no characteristics.
                        characteristics = new List<GattCharacteristic>();
                    }
                }
                else
                {
                    // On error, act as if there are no characteristics.
                    characteristics = new List<GattCharacteristic>();
                }
            }
            catch (Exception)
            {
                // On error, act as if there are no characteristics.
                characteristics = new List<GattCharacteristic>();
            }

            foreach (GattCharacteristic c in characteristics)
                charsCollection.Add(new BluetoothLEAttributeDisplay(c));

            return charsCollection;
        }

        private char[] whitespace = new char[] { ' ', '\t' };

        public ushort DeviceID { get; set; }

        public BluetoothLEDevice BLEDevice { get; set; }

        public ObservableCollection<BluetoothLEAttributeDisplay> ServiceCollection { get; set; }
        public ObservableCollection<BluetoothLEAttributeDisplay> CharacteristicCollection { get; set; }

        // Only one registered characteristic at a time.
        public ObservableCollection<GattCharacteristic> registeredCharacteristic_cmd = new ObservableCollection<GattCharacteristic>();
        public ObservableCollection<GattCharacteristic> registeredCharacteristic_data = new ObservableCollection<GattCharacteristic>();

        private bool subscribedForNotifications_cmd = false;
        private bool subscribedForNotifications_data = false;
        public GattDeviceService SelectedService { get; set; }
        public GattCharacteristic SelectedCharacteristic_cmd { get; set; }
        public GattCharacteristic SelectedCharacteristic_data { get; set; }

        public static ObservableCollection<GattCharacteristic> SelectedCharacteristicNetwork_cmd { get; set; }
        public static ObservableCollection<GattCharacteristic> SelectedCharacteristicNetwork_data { get; set; }

        public bool SubscribedForNotifications_cmd { get; set; }
        public bool SubscribedForNotifications_data { get; set; }

        private string deviceName;
        public string DeviceName
        {
            get { return deviceName; }
            set
            {
                if (value != DeviceName)
                    if (value != DeviceName)
                    {
                        deviceName = value;
                        OnPropertyChanged("DeviceName");
                    }
            }
        }

        public Muse_HW.FullScales full_scales;

        public string modeStr = "";
        public string freqStr = "";

        //public string acqModeStr = "";
        //public string acqFreqStr = "";

        public int current_acquisition_frequency = 0;
        public int current_acquisition_dt = 0;

        private int counter = 0;

        public DateTime fileTimestamp;

        public async Task<bool> SendMessage(byte[] message)
        {
            // Setup data writer object
            var writer = new DataWriter();
            writer.ByteOrder = ByteOrder.BigEndian;

            writer.WriteBytes(message);

            try
            {
                await SelectedCharacteristic_cmd.WriteValueAsync(writer.DetachBuffer());

                // Writes the value from the buffer to the characteristic
                // var res = await SelectedCharacteristic_cmd.WriteValueWithResultAsync(writer.DetachBuffer());

                // if (res.Status == GattCommunicationStatus.Success)
                return true;
                // return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        private async void CmdCharacteristicSubscribeButton_Click()
        {
            if (!SubscribedForNotifications_cmd)
            {
                // Status initialization
                GattCommunicationStatus status = GattCommunicationStatus.Unreachable;
                var cccdValue = GattClientCharacteristicConfigurationDescriptorValue.None;
                if (SelectedCharacteristic_cmd.CharacteristicProperties.HasFlag(GattCharacteristicProperties.Indicate))
                {
                    cccdValue = GattClientCharacteristicConfigurationDescriptorValue.Indicate;
                }

                else if (SelectedCharacteristic_cmd.CharacteristicProperties.HasFlag(GattCharacteristicProperties.Notify))
                {
                    cccdValue = GattClientCharacteristicConfigurationDescriptorValue.Notify;
                }

                try
                {
                    // Must write the CCCD in order for server to send indications.
                    // We receive them in the ValueChanged event handler.
                    // status = 
                    SelectedCharacteristic_cmd.WriteClientCharacteristicConfigurationDescriptorAsync(cccdValue);

                    // if (status == GattCommunicationStatus.Success)
                    AddValueChangedHandlerCmd();
                }
                catch (UnauthorizedAccessException)
                {
                }
            }
            else
            {
                try
                {
                    // Must write the CCCD in order for server to send notifications.
                    // We receive them in the ValueChanged event handler.
                    // This configures either Indicate or Notify, but not both.
                    var result = await
                            SelectedCharacteristic_cmd.WriteClientCharacteristicConfigurationDescriptorAsync(
                                GattClientCharacteristicConfigurationDescriptorValue.None);
                    if (result == GattCommunicationStatus.Success)
                    {
                        SubscribedForNotifications_cmd = false;
                        // RemoveValueChangedHandlerCmd();
                    }
                }
                catch (UnauthorizedAccessException)
                {
                }
            }
        }

        private async void DataCharacteristicSubscribeButton_Click()
        {
            if (!SubscribedForNotifications_data)
            {
                // Status initialization
                GattCommunicationStatus status = GattCommunicationStatus.Unreachable;
                var cccdValue = GattClientCharacteristicConfigurationDescriptorValue.None;
                if (SelectedCharacteristic_data.CharacteristicProperties.HasFlag(GattCharacteristicProperties.Indicate))
                {
                    cccdValue = GattClientCharacteristicConfigurationDescriptorValue.Indicate;
                }

                else if (SelectedCharacteristic_data.CharacteristicProperties.HasFlag(GattCharacteristicProperties.Notify))
                {
                    cccdValue = GattClientCharacteristicConfigurationDescriptorValue.Notify;
                }

                try
                {
                    // Must write the CCCD in order for server to send indications.
                    // We receive them in the ValueChanged event handler.
                    if (cccdValue != GattClientCharacteristicConfigurationDescriptorValue.None)
                    {
                        status = await SelectedCharacteristic_data.WriteClientCharacteristicConfigurationDescriptorAsync(cccdValue);

                        if (status == GattCommunicationStatus.Success)
                            AddValueChangedHandlerData();
                    }
                }
                catch (UnauthorizedAccessException)
                {
                }
            }
            else
            {
                try
                {
                    // Must write the CCCD in order for server to send notifications.
                    // We receive them in the ValueChanged event handler.
                    // This configures either Indicate or Notify, but not both.
                    var result = await
                            SelectedCharacteristic_data.WriteClientCharacteristicConfigurationDescriptorAsync(
                                GattClientCharacteristicConfigurationDescriptorValue.None);
                    if (result == GattCommunicationStatus.Success)
                        SubscribedForNotifications_data = false;
                    RemoveValueChangedHandlerData();
                }
                catch (UnauthorizedAccessException)
                {
                }
            }
        }

        private void AddValueChangedHandlerCmd()
        {
            if (!subscribedForNotifications_cmd)
            {
                // int len = BLENetwork.Count;
                // NetworkNode.SelectedCharacteristicNetwork_cmd[len-1].ValueChanged += CmdCharacteristic_ValueChanged;

                // node.SelectedCharacteristic_cmd.ValueChanged += CmdCharacteristic_ValueChanged;
                // subscribedForNotifications_cmd = true;

                registeredCharacteristic_cmd.Add(SelectedCharacteristic_cmd);
                int len = registeredCharacteristic_cmd.Count;

                registeredCharacteristic_cmd[len - 1].ValueChanged += CmdCharacteristic_ValueChanged;
                subscribedForNotifications_cmd = true;
            }

        }

        private void AddValueChangedHandlerData()
        {
            if (!subscribedForNotifications_data)
            {
                // node.SelectedCharacteristic_data.ValueChanged += DataCharacteristic_ValueChanged;
                // subscribedForNotifications_data = true;

                registeredCharacteristic_data.Add(SelectedCharacteristic_data);
                int len = registeredCharacteristic_data.Count;
                registeredCharacteristic_data[len - 1].ValueChanged += DataCharacteristic_ValueChanged;
                subscribedForNotifications_data = true;
            }
        }

        // Manage formatted output string
        private string cmd_output_string_;
        public string cmd_output_string
        {
            get { return cmd_output_string_; }
            set
            {
                cmd_output_string_ = value;
                OnPropertyChanged("cmd_output_string");
            }
        }

        // Copy received message as raw byte array
        private string cmd_raw_output_string_;

        public string cmd_raw_output_string
        {
            get { return cmd_raw_output_string_; }
            set
            {
                cmd_raw_output_string_ = value;
                OnPropertyChanged("cmd_raw_output_string");
            }
        }

        public UInt32 acqMode;
        public List<Muse_Data> encData;
        public int num_of_pages;

        public bool enable_file_offload = false;
        public List<byte[]> file_offload_container = new List<byte[]>();

        private async void CmdCharacteristic_ValueChanged(GattCharacteristic sender, GattValueChangedEventArgs args)
        {
            // Get raw characteristic value from event args
            byte[] data;
            CryptographicBuffer.CopyToByteArray(args.CharacteristicValue, out data);

            // Check string format and length
            string newValue = BitConverter.ToString(data).Replace("-", " ");
            if (newValue.Length != 0)
            {
                // Access to characteristic content as hex array
                string[] ssizes = newValue.Split(whitespace);
                byte[] messageValue = new byte[ssizes.Length];

                for (int i = 0; i < ssizes.Length; i++)
                {
                    int value = Convert.ToInt16(ssizes[i], 16);
                    messageValue[i] = Convert.ToByte(value);
                }

                int payload_size = 0;
                bool read = false;
                byte[] tmp;

                // Copy received message as raw byte array
                cmd_raw_output_string = newValue;

                // Show raw characteristic value as hex array
                if (PagesGateway.BlePg != null)
                    await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => PagesGateway.BlePg.LblRead.Text = cmd_raw_output_string);

                // Switch towards available command codes
                var temp = messageValue[2] & 0x7F;

                switch (messageValue[2] & 0x7F)
                {
                    case (byte)Muse_HW.Command.CMD_STATE:
                        if (messageValue[3] == 0)
                        {
                            read = (messageValue[2] & 0x80) != 0;

                            if (read)
                            {
                                mData.State = (Muse_HW.SystemState)messageValue[4];

                                // Manage state command response (read mode)
                                await PagesGateway.BlePg.Dispatcher.InvokeAsync(() =>
                                PagesGateway.BlePg.LblCurrentState.Text = Muse_Utils.GetDeviceStateString(messageValue[4]),
                                System.Windows.Threading.DispatcherPriority.Normal);


                            }
                            else
                            {
                                // Manage set state command response (write mode)
                                if (messageValue[1] == 9) // 
                                {
                                    iterationCounter = 0;
                                    encData = new List<Muse_Data>();

                                    byte[] currentFS = new byte[4];
                                    Array.Copy(messageValue, 4, currentFS, 0, 3);
                                    UInt32 currentFSCode = BitConverter.ToUInt32(currentFS, 0);
                                    full_scales = Muse_Utils.ExtractMEMSResolutions(currentFSCode);

                                    //Mode
                                    byte[] currentAcqMode = new byte[4];
                                    Array.Copy(messageValue, 7, currentAcqMode, 0, 3);
                                    acqMode = BitConverter.ToUInt32(currentAcqMode, 0);
                                    modeStr = Muse_Utils.ExtractMode(acqMode);

                                    //Frequency
                                    current_acquisition_frequency = messageValue[10];
                                    freqStr = Muse_Utils.ExtractFrequency(messageValue[10]);

                                }
                            }
                        }
                        else
                        {
                            // ERROR CONDITION TO BE NOTIFIED
                        }
                        break;
                    case (byte)Muse_HW.Command.CMD_RESTART:
                        // Reset all 
                        break;
                    case (byte)Muse_HW.Command.CMD_TIME:
                        if (messageValue[3] == 0)
                        {
                            read = (messageValue[2] & 0x80) != 0;

                            if (read)
                            {
                                // Decode characteristic value
                                byte[] currentTime = new byte[4];
                                Array.Copy(messageValue, 4, currentTime, 0, 4);
                                UInt64 currentTime_val = BitConverter.ToUInt32(currentTime, 0);

                                var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                                DateTime currentDateTime = epoch.AddSeconds(currentTime_val);

                                if (PagesGateway.BlePg != null)
                                    await PagesGateway.BlePg.Dispatcher.InvokeAsync(() =>
                                    PagesGateway.BlePg.LblDateTime.Text = currentDateTime.ToString(), System.Windows.Threading.DispatcherPriority.Normal);

                            }
                        }
                        break;
                    case (byte)Muse_HW.Command.CMD_FW_UPLOAD:
                        if (messageValue[3] == 0)
                        {
                            if (messageValue[0] == 0x00 && messageValue[1] == 0x03)
                            {
                                // Manage end of FOTA procedure

                                await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => PagesGateway.BlePg.MessageBox.Text = "UPDATE COMPLETED", System.Windows.Threading.DispatcherPriority.Normal);

                                // TODO: Add progress bar and automatic restart command and UI reset on next sw revision
                            }
                            else
                            {
                                // Manage binary upload on data characteristic

                                byte[] cBuffer = new byte[Muse_HW.COMM_MESSAGE_LEN_DAT];

                                // -------------------- Private Key -------------------------
                                // TODO RSA
                                //string key_path = @"Keys/privatekey.pem"; 
                                //PemReader pr = new PemReader(new System.IO.StreamReader(key_path));
                                //AsymmetricCipherKeyPair KeyPair = (AsymmetricCipherKeyPair)pr.ReadObject();
                                //RSAParameters rsaParams = DotNetUtilities.ToRSAParameters((RsaPrivateCrtKeyParameters)KeyPair.Private);

                                //RSACryptoServiceProvider csp = new RSACryptoServiceProvider();// cspParams);
                                //csp.ImportParameters(rsaParams);
                                //byte[] testMessage = new byte[20];
                                ////FIXME -------------Microcontroller ID (hardcoded)
                                //testMessage[16] = 0xc7;
                                //testMessage[17] = 0xf0;
                                //testMessage[18] = 0x0b;
                                //testMessage[19] = 0x6f;
                                ////END FIXME---------------



                                ////Save the current time
                                //TimeSpan timeSpan = DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
                                //uint secondsSinceEpoch = (uint)timeSpan.TotalSeconds;
                                //byte[] tempTime = BitConverter.GetBytes(secondsSinceEpoch);
                                //Array.Reverse(tempTime);
                                //Array.Copy(tempTime, 0, testMessage, 12, 4);


                                //byte[] testSign = csp.SignHash(testMessage, CryptoConfig.MapNameToOID("SHA1"));

                                //for (int i = 0; i < 512 / 32; i++)
                                //{
                                //    Array.Reverse(testSign, i * 4, 4);
                                //}

                                //await SelectedCharacteristic_data.WriteValueAsync(testSign.AsBuffer(), GattWriteOption.WriteWithoutResponse);

                                // ------------------------------------------------------------

                                // Upload binary file writing on 72-byte data characteristics
                                while (current_NumberIteration_Upload * Muse_HW.COMM_MESSAGE_LEN_DAT < PagesGateway.BlePg.fsBytesLength_ - Muse_HW.COMM_MESSAGE_LEN_DAT)
                                {

                                    // Load current payload into buffer
                                    Array.Copy(PagesGateway.BlePg.fsBytes_, current_NumberIteration_Upload * Muse_HW.COMM_MESSAGE_LEN_DAT, cBuffer, 0, Muse_HW.COMM_MESSAGE_LEN_DAT);

                                    // stopwatch.Restart();

                                    await SelectedCharacteristic_data.WriteValueAsync(cBuffer.AsBuffer(), GattWriteOption.WriteWithoutResponse);
                                    // await Task.Delay(3);

                                    //  stopwatch.Stop();
                                    // ts.Add(stopwatch.Elapsed);

                                    // Update percentage label
                                    cmd_output_string = String.Format("{0}/{1}", current_NumberIteration_Upload * Muse_HW.COMM_MESSAGE_LEN_DAT, PagesGateway.BlePg.fsBytesLength_);
                                    await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => PagesGateway.BlePg.MessageBox.Text = cmd_output_string, System.Windows.Threading.DispatcherPriority.Normal);

                                    // Update packet iteration counter
                                    current_NumberIteration_Upload++;
                                }

                                int lastPacketLen = PagesGateway.BlePg.fsBytesLength_ - (current_NumberIteration_Upload * Muse_HW.COMM_MESSAGE_LEN_DAT);
                                if (lastPacketLen > 0)
                                {
                                    cBuffer = new byte[Muse_HW.COMM_MESSAGE_LEN_DAT];

                                    // Load current payload into buffer
                                    Array.Copy(PagesGateway.BlePg.fsBytes_, current_NumberIteration_Upload * Muse_HW.COMM_MESSAGE_LEN_DAT, cBuffer, 0, lastPacketLen);

                                    // stopwatch.Restart();

                                    await SelectedCharacteristic_data.WriteValueAsync(cBuffer.AsBuffer(), GattWriteOption.WriteWithoutResponse);

                                    // stopwatch.Stop();
                                    // ts.Add(stopwatch.Elapsed);

                                    // Update percentage label
                                    cmd_output_string = String.Format("{0}/{1}", (current_NumberIteration_Upload * Muse_HW.COMM_MESSAGE_LEN_DAT) + lastPacketLen, PagesGateway.BlePg.fsBytesLength_);
                                    await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => PagesGateway.BlePg.MessageBox.Text = cmd_output_string, System.Windows.Threading.DispatcherPriority.Normal);
                                }
                            }
                        }

                        cmd_raw_output_string = "";
                        current_NumberIteration_Upload = 0;

                        break;

                    case (byte)Muse_HW.Command.CMD_BATTERY_CHARGE:
                        if (messageValue[3] == 0)
                        {
                            read = (messageValue[2] & 0x80) != 0;

                            if (read)
                            {
                                // BatteryCharge = String.Format("{0} %", messageValue[4]);
                                await PagesGateway.BlePg.Dispatcher.InvokeAsync(() =>
                                PagesGateway.BlePg.LblBatteryCharge.Text = String.Format("{0} %", messageValue[4]),
                                System.Windows.Threading.DispatcherPriority.Normal);
                            }
                        }
                        break;


                    case (byte)Muse_HW.Command.CMD_BATTERY_VOLTAGE:
                        if (messageValue[3] == 0)
                        {
                            read = (messageValue[2] & 0x80) != 0;

                            if (read)
                            {
                                // Decode characteristic value
                                UInt16 battery_voltage = BitConverter.ToUInt16(messageValue, 4);

                                await PagesGateway.BlePg.Dispatcher.InvokeAsync(() =>
                               PagesGateway.BlePg.LblBatteryVoltage.Text = String.Format("{0} mV", battery_voltage),
                                System.Windows.Threading.DispatcherPriority.Normal);
                            }
                        }
                        break;

                    case (byte)Muse_HW.Command.CMD_CHECK_UP:
                        if (messageValue[3] == 0)
                        {
                            read = (messageValue[2] & 0x80) != 0;
                            if (read)
                            {
                                string tmpOut = "";
                                for (int i = 0; i < messageValue[1] - 2; i++)
                                    tmpOut += messageValue[i + 4].ToString("X") + " ";

                                cmd_output_string = tmpOut;
                                //await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => 
                                //PagesGateway.BlePg.LblCheckUp.Text = cmd_output_string, System.Windows.Threading.DispatcherPriority.Normal);

                            }
                        }
                        break;
                    case (byte)Muse_HW.Command.CMD_FW_VERSION:
                        if (messageValue[3] == 0)
                        {
                            read = (messageValue[2] & 0x80) != 0;
                            if (read)
                            {
                                // Decode characteristic value
                                payload_size = messageValue[1] - 2;
                                byte[] var_fw_ver = new byte[payload_size];
                                for (int i = 0; i < payload_size; i++)
                                    var_fw_ver[i] = messageValue[4 + i];
                                cmd_output_string = System.Text.Encoding.UTF8.GetString(var_fw_ver).Replace("\0", " ");
                                string[] words = cmd_output_string.Split();
                                var twoWords = words.Take(2);
                                await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => PagesGateway.BlePg.LblFirmwareVersion.Text =
                                string.Join(" ", twoWords), System.Windows.Threading.DispatcherPriority.Normal);
                            }
                        }
                        break;
                    case (byte)Muse_HW.Command.CMD_BLE_NAME: //  = 0x0C,            //!< CMD_BLE_NAME
                        if (messageValue[3] == 0)
                        {
                            read = (messageValue[2] & 0x80) != 0;

                            if (read)
                            {
                                tmp = new byte[16];
                                Array.Copy(messageValue, 4, tmp, 0, 16);
                                cmd_output_string = System.Text.Encoding.ASCII.GetString(tmp);
                                await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => PagesGateway.BlePg.LblCurrentBLEName.Text =
                                cmd_output_string, System.Windows.Threading.DispatcherPriority.Normal);
                            }
                            else
                            {
                                cmd_output_string = "Ble name set!";
                                await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => PagesGateway.BlePg.MessageBox.Text =
                                cmd_output_string, System.Windows.Threading.DispatcherPriority.Normal);
                            }

                            

                        }
                        break;
                    case (byte)Muse_HW.Command.CMD_DEVICE_ID:
                        if (messageValue[3] == 0)
                        {
                            read = (messageValue[2] & 0x80) != 0;
                            if (read)
                            {
                                UInt32 temp_id = BitConverter.ToUInt32(messageValue, 4);

                                await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => PagesGateway.BlePg.LblDeviceId.Text =
                                temp_id.ToString("X8"), System.Windows.Threading.DispatcherPriority.Normal);


                            }
                        }
                        break;
                    // memory

                    case (byte)Muse_HW.Command.CMD_MEM_CONTROL: //  = 0x20,         //!< CMD_MEMORY_CONTROL
                        if (messageValue[3] == 0)
                        {
                            read = (messageValue[2] & 0x80) != 0;

                            if (read)
                            {
                                if (BLE.logUpdate)
                                {
                                    // Number of files saved
                                    tmp = new byte[2];
                                    Array.Copy(messageValue, 5, tmp, 0, 2);
                                    num_of_files = BitConverter.ToUInt16(tmp, 0);

                                    if (PagesGateway.BlePg.comboBoxBleFile.Items.Count > 0)
                                        await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => PagesGateway.BlePg.comboBoxBleFile.Items.Clear(), System.Windows.Threading.DispatcherPriority.Normal);

                                    if (num_of_files > 0)
                                    {
                                        for (int i = 0; i < num_of_files; i++)
                                        {
                                            byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                                            msg = Muse_Utils.Cmd_MemoryFileInfo(i);

                                            await SendMessage(msg);

                                            while (PagesGateway.BlePg.comboBoxBleFile.Items.Count != i + 1)
                                                await Task.Delay(25);

                                        }

                                    }

                                }

                                else
                                {
                                    // Available memory space in %
                                    var available_memory = messageValue[4];

                                    // Number of files saved
                                    tmp = new byte[2];
                                    Array.Copy(messageValue, 5, tmp, 0, 2);
                                    num_of_files = BitConverter.ToUInt16(tmp, 0);

                                    cmd_output_string = "Free memory: " + available_memory + "%; Num of files: " + num_of_files;
                                    await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => PagesGateway.BlePg.LblMemoryControl.Text = cmd_output_string, System.Windows.Threading.DispatcherPriority.Normal);
                                }

                            }
                            else
                            {
                                cmd_output_string = "";
                                await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => PagesGateway.BlePg.LblMemoryControl.Text = cmd_output_string);
                                await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => PagesGateway.BlePg.FileInfo = cmd_output_string);
                                await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => PagesGateway.BlePg.MessageBox.Text = cmd_output_string);
                                await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => PagesGateway.BlePg.comboBoxBleFile.SelectedValue = cmd_output_string);

                                if (PagesGateway.BlePg.comboBoxBleFile.Items.Count > 0)
                                    await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => PagesGateway.BlePg.comboBoxBleFile.Items.Clear(), System.Windows.Threading.DispatcherPriority.Normal);
                            }

                        }
                        break;
                    case (byte)Muse_HW.Command.CMD_MEM_FILE_INFO: //  = 0x21,       //!< CMD_MEMORY_FILE_INFO
                        if (messageValue[3] == 0)
                        {
                            read = (messageValue[2] & 0x80) != 0;

                            if (read)
                            {
                                if (BLE.logUpdate)
                                {
                                    // File timestamp
                                    tmp = new byte[8];
                                    Array.Copy(messageValue, 4, tmp, 0, 5);
                                    UInt64 currentTime_val = BitConverter.ToUInt64(tmp, 0);
                                    currentTime_val += ((UInt64)reference_epoch) * 1000;

                                    await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => PagesGateway.BlePg.comboBoxBleFile.Items.Add(DateTimeOffset.FromUnixTimeMilliseconds((long)currentTime_val).DateTime.ToString("dd/MM/yyyy HH:mm:ss").ToString()), System.Windows.Threading.DispatcherPriority.Normal);

                                    if (PagesGateway.BlePg.comboBoxBleFile.Items.Count == num_of_files)
                                        BLE.logUpdate = false;

                                }
                                else
                                {

                                    // File timestamp
                                    tmp = new byte[8];
                                    Array.Copy(messageValue, 4, tmp, 0, 5);
                                    UInt64 currentTime_val = BitConverter.ToUInt64(tmp, 0);
                                    currentTime_val += ((UInt64)reference_epoch) * 1000;

                                    // MEMS full scales
                                    byte[] currentFS = new byte[4];
                                    Array.Copy(messageValue, 9, currentFS, 0, 1);
                                    UInt32 currentFSCode = BitConverter.ToUInt32(currentFS, 0);

                                    full_scales = Muse_Utils.ExtractMEMSResolutions(currentFSCode);

                                    // Log mode
                                    tmp = new byte[4];
                                    Array.Copy(messageValue, 10, tmp, 0, 3);
                                    acqMode = BitConverter.ToUInt32(tmp, 0);
                                    modeStr = Muse_Utils.ExtractMode(acqMode);
                                    freqStr = Muse_Utils.ExtractFrequency(messageValue[13]);

                                    fileTimestamp = DateTimeOffset.FromUnixTimeMilliseconds((long)currentTime_val).DateTime;

                                    if (!BLE.readFile)
                                    {
                                        cmd_output_string = "Timestamp: " + fileTimestamp.ToString("dd/MM/yyyy HH:mm:ss") + ";\nMode: " + modeStr + ";\nFreq: " + freqStr + ";\nAxl FS: " + full_scales.axl_fsStr + "- Gyr FS: " + full_scales.gyr_fsStr + "- Mag FS: " + full_scales.mag_fsStr + "- Hdr FS: " + full_scales.hdr_fsStr;
                                        await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => PagesGateway.BlePg.LblCurrentFileInfo.Text = cmd_output_string, System.Windows.Threading.DispatcherPriority.Normal);

                                    }
                                }

                            }
                        }
                        break;
                    case (byte)Muse_HW.Command.CMD_MEM_FILE_DOWNLOAD:
                        // CMD_MEMORY_FILE_DOWNLOAD (in Read mode)
                        if (messageValue[3] == 0x00)
                        {
                            enable_file_offload = true;
                            file_offload_container = new List<byte[]>();

                            // Get file size (first 4 Bytes of payload)
                            byte[] fileSize = new byte[4];
                            Array.Copy(messageValue, 4, fileSize, 0, 4);

                            logFileSize_ = BitConverter.ToInt32(fileSize, 0);

                            num_of_pages = (int)(logFileSize_ / 2048);
                            num_of_chunks = (int)Math.Ceiling((logFileSize_ / 128.0f));

                            packet_counter_data_ = 0;

                            acquisitionListRaw = new List<Tuple<UInt64, byte[]>>();

                            payload_size = 0;
                            enableFileDownload = true;
                            timeoutCounter = 0;

                            // Create a timer and set a two second interval.
                            aTimer = new System.Timers.Timer();
                            aTimer.Interval = 2000;

                            // Hook up the Elapsed event for the timer. 
                            aTimer.Elapsed += OnTimedEvent;

                            // Have the timer fire repeated events (true is the default)
                            aTimer.AutoReset = false;

                            // Start the timer
                            aTimer.Enabled = true;

                            byte[] ackBuffer = new byte[20];
                            ackBuffer[0] = 0x00;  // 00
                            ackBuffer[1] = 0x01; // 01
                            ackBuffer[2] = (byte)Muse_HW.Command.CMD_MEM_FILE_DOWNLOAD; // 22

                            await SelectedCharacteristic_cmd.WriteValueAsync(ackBuffer.AsBuffer());

                        }

                        break;

                    case (byte)Muse_HW.Command.CMD_SENSORS_FS:
                        if (messageValue[3] == 0x00)
                        {
                            read = (messageValue[2] & 0x80) != 0;

                            if (read)
                            {
                                byte[] currentFS = new byte[4];
                                Array.Copy(messageValue, 4, currentFS, 0, 3);
                                UInt32 currentFSCode = BitConverter.ToUInt32(currentFS, 0);
                                full_scales = Muse_Utils.ExtractMEMSResolutions(currentFSCode);
                                cmd_output_string = full_scales.axl_fsStr + "; " + full_scales.gyr_fsStr + "; " + full_scales.mag_fsStr + "; " + full_scales.hdr_fsStr;

                                await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => PagesGateway.BlePg.Lbl6DOFFS.Text = cmd_output_string, System.Windows.Threading.DispatcherPriority.Normal);

                            }
                            else
                                await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => PagesGateway.BlePg.MessageBox.Text = "Full Scales correctly set", System.Windows.Threading.DispatcherPriority.Normal);
                        }
                        break;
                    case (byte)Muse_HW.Command.CMD_BTN_LOG:
                        if (messageValue[3] == 0x00)
                        {
                            read = (messageValue[2] & 0x80) != 0;

                            if (read)
                            {
                                // Log mode
                                tmp = new byte[4];
                                Array.Copy(messageValue, 4, tmp, 0, 3);
                                acqMode = BitConverter.ToUInt32(tmp, 0);
                                modeStr = Muse_Utils.ExtractMode(acqMode);
                                freqStr = Muse_Utils.ExtractFrequency(messageValue[7]);

                                cmd_output_string = modeStr + "; " + freqStr + "; ";
                                await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => PagesGateway.BlePg.LblGetLogMode.Text = cmd_output_string, System.Windows.Threading.DispatcherPriority.Normal);

                            }
                            else
                                await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => PagesGateway.BlePg.MessageBox.Text = "Log mode and frequency correctly set", System.Windows.Threading.DispatcherPriority.Normal);
                        }
                        break;
                    case (byte)Muse_HW.Command.CMD_CALIB_MATRIX:
                        if (messageValue[3] == 0)
                        {
                            read = (messageValue[2] & 0x80) != 0;

                            if (read)
                            {
                                // Manage calibration matrix read and visualization
                                if (enableDownloadCalibration == true && calibColCounter < 4)
                                {
                                    // Decode received acknowledge payload
                                    byte[] currentPayload = new byte[12];
                                    Array.Copy(messageValue, 4, currentPayload, 0, 12);
                                    for (int i = 0; i < 3; i++)
                                    {
                                        byte[] tmpVal = new byte[4];
                                        Array.Copy(currentPayload, i * 4, tmpVal, 0, 4);
                                        calibResult[i + ((calibColCounter - 1) * 3)] = BitConverter.ToSingle(tmpVal, 0);
                                    }

                                    // Send next params request
                                    byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                                    await PagesGateway.BlePg.Dispatcher.InvokeAsync(async () =>
                                    {
                                        msg = Muse_Utils.Cmd_GetCalibrationMatrix((byte)PagesGateway.BlePg.CBCalibMatrix.SelectedIndex, (byte)calibColCounter);

                                    }, System.Windows.Threading.DispatcherPriority.Normal);

                                    calibColCounter++;

                                    await SendMessage(msg);
                                }
                                else
                                {
                                    if (calibColCounter == 4)
                                    {
                                        // Decode last received acknowledge payload
                                        byte[] currentPayload = new byte[12];
                                        Array.Copy(messageValue, 4, currentPayload, 0, 12);
                                        for (int i = 0; i < 3; i++)
                                        {
                                            byte[] tmpVal = new byte[4];
                                            Array.Copy(currentPayload, i * 4, tmpVal, 0, 4);
                                            calibResult[i + ((calibColCounter - 1) * 3)] = BitConverter.ToSingle(tmpVal, 0);
                                        }



                                        var title = "Device Calibration Parameters";

                                        var calibration = String.Format("{0} {1} {2} {3}\n{4} {5} {6} {7}\n{8} {9} {10} {11}",
                                            calibResult[0], calibResult[3], calibResult[6], calibResult[9],
                                            calibResult[1], calibResult[4], calibResult[7], calibResult[10],
                                            calibResult[2], calibResult[5], calibResult[8], calibResult[11]);

                                        var content = calibration + "\n\nSave it?";

                                        if (System.Windows.Forms.MessageBox.Show(content, title, MessageBoxButtons.YesNoCancel) == System.Windows.Forms.DialogResult.Yes)
                                        {

                                            await PagesGateway.BlePg.Dispatcher.InvokeAsync(async () =>
                                            {
                                            FolderBrowserDialog folderDialog = new FolderBrowserDialog();
                                                if (folderDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                                                {
                                                    string selectedPath = folderDialog.SelectedPath;

                                                    if (selectedPath != "")
                                                    {
                                                        selectedPath = selectedPath + "\\";
                                                        string fileName = selectedPath + "\\" + "Calibration.txt";
                                                        bool write = true;

                                                        if (File.Exists(fileName))
                                                        {
                                                            if (System.Windows.Forms.MessageBox.Show("The file already exists. Overwrite it?",
                                                                "File already exists", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.No)
                                                            {
                                                                write = false;
                                                            }
                                                        }
                                                        if (write)
                                                        {
                                                            StreamWriter file = new StreamWriter(fileName);
                                                            file.Write(calibration);
                                                            System.Threading.Thread.Sleep(1000);
                                                            file.Close();
                                                        }


                                                    }
                                                }

                                        }, System.Windows.Threading.DispatcherPriority.Normal);
                                        }
                                        // else if (command == cancelCommand)
                                        else
                                        {
                                            calibColCounter = 0;
                                            calibResult = null;
                                        }
                                    }

                                    enableDownloadCalibration = false;
                                    calibColCounter = 0;
                                    calibResult = null;
                                }
                            }
                            else
                            {
                                if (enableUploadCalibration == true && calibColCounter < 4)
                                {
                                    byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                                   
                                    calibColCounter++;

                                    await SendMessage(msg);
                                }
                                else
                                {
                                    enableUploadCalibration = false;
                                    calibColCounter = 0;
                                    calibResult = null;
                                }
                            }
                        }
                        break;
                    case (byte)Muse_HW.Command.CMD_CFG:
                        if (messageValue[3] == 0)
                        {
                            read = (messageValue[2] & 0x80) != 0;

                            if (read)
                            {
                                byte[] currentConfiguration = new byte[4];
                                Array.Copy(messageValue, 4, currentConfiguration, 0, 2);
                                UInt32 conf = BitConverter.ToUInt32(currentConfiguration, 0);

                                // standby is enabled --> you can only disabled
                                if ((conf & (UInt32)0x01) > 0)
                                {
                                    PagesGateway.BlePg.enable_standby = false;
                                    await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => PagesGateway.BlePg.BtnStandby.Content = "Disable Standby", System.Windows.Threading.DispatcherPriority.Normal);
                                }
                                else
                                {
                                    PagesGateway.BlePg.enable_standby = true;
                                    await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => PagesGateway.BlePg.BtnStandby.Content = "Enable Standby", System.Windows.Threading.DispatcherPriority.Normal);
                                }

                                // circular memory is enabled --> you can only set linear memory
                                if ((conf & (UInt32)0x02) > 0)
                                {
                                    PagesGateway.BlePg.set_circular = false;
                                    await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => PagesGateway.BlePg.BtnMemoryBuffer.Content = "Disable Circular", System.Windows.Threading.DispatcherPriority.Normal);
                                }
                                else
                                {
                                    PagesGateway.BlePg.set_circular = true;
                                    await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => PagesGateway.BlePg.BtnMemoryBuffer.Content = "Enable Circular", System.Windows.Threading.DispatcherPriority.Normal);
                                }
                                //


                            }
                            else
                            {
                                if (PagesGateway.BlePg.standby_changed)
                                {
                                    if (PagesGateway.BlePg.enable_standby == true)
                                    {
                                        PagesGateway.BlePg.enable_standby = false;
                                        await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => PagesGateway.BlePg.BtnStandby.Content = "Disable Standby", System.Windows.Threading.DispatcherPriority.Normal);
                                    }
                                    else
                                    {
                                        PagesGateway.BlePg.enable_standby = true;
                                        await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => PagesGateway.BlePg.BtnStandby.Content = "Enable Standby", System.Windows.Threading.DispatcherPriority.Normal);
                                    }

                                    PagesGateway.BlePg.standby_changed = false;
                                }

                                if (PagesGateway.BlePg.memory_changed)
                                {
                                    if (PagesGateway.BlePg.set_circular == true)
                                    {
                                        PagesGateway.BlePg.set_circular = false;
                                        await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => PagesGateway.BlePg.BtnMemoryBuffer.Content = "Disable Circular", System.Windows.Threading.DispatcherPriority.Normal);
                                    }
                                    else
                                    {
                                        PagesGateway.BlePg.set_circular = true;
                                        await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => PagesGateway.BlePg.BtnMemoryBuffer.Content = "Enable Circular", System.Windows.Threading.DispatcherPriority.Normal);
                                    }
                                }

                                cmd_output_string = "User Configuration updated.";
                                if (PagesGateway.BlePg != null)
                                    await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => PagesGateway.BlePg.MessageBox.Text = cmd_output_string, System.Windows.Threading.DispatcherPriority.Normal);


                            }
                        }
                        break;
                    case (byte)Muse_HW.Command.CMD_CONDITIONAL_LOG_THRESHOLD:
                        if (messageValue[3] == 0)
                        {
                            read = (messageValue[2] & 0x80) != 0;

                            if (read)
                            {

                                byte[] threshold = new byte[4];
                                Array.Copy(messageValue, 4, threshold, 0, 4);
                                float threshold1 = BitConverter.ToSingle(threshold, 0);

                                Array.Copy(messageValue, 8, threshold, 0, 4);
                                float threshold2 = BitConverter.ToSingle(threshold, 0);

                                cmd_output_string = "Thr 1: " + threshold1.ToString() + " [dps] - Thr 2: " + threshold2.ToString() + " [mg]";

                                await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => PagesGateway.BlePg.LblThresholdsOutput.Text = cmd_output_string, System.Windows.Threading.DispatcherPriority.Normal);
                            }
                            else
                            {
                                cmd_output_string = "Conditional Log Thresholds updated.";
                                await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => PagesGateway.BlePg.MessageBox.Text = cmd_output_string, System.Windows.Threading.DispatcherPriority.Normal);

                            }
                        }
                        break;

                    default:
                        break;
                }
            }
        }

        private int timeoutCounter = 0;
        private async void OnTimedEvent(Object source, System.Timers.ElapsedEventArgs e)
        {
            timeoutCounter++;

            if (timeoutCounter >= 10)
            {
                // Disable procedure
                enableFileDownload = false;
                timeoutCounter = 0;

                // Stop timer
                aTimer.Stop();

                // Set device in IDLE state
                byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                msg = Muse_Utils.Cmd_SetState(Muse_HW.SystemState.SYS_IDLE);
                await SendMessage(msg);

            }
            else
            {
                if (packet_counter_data_ <= 28)
                {
                    int fileId = -1;
                    bool isNumeric = false;

                    byte[] msg = new byte[Muse_HW.COMM_MESSAGE_LEN_CMD];
                    msg = Muse_Utils.Cmd_MemoryFileDownload(fileId, 0x01);

                    await SendMessage(msg);

                }
                else
                {
                    aTimer.Stop();
                    aTimer.Enabled = false;
                }
            }

        }

        private float[] tmpSum4Calib = new float[3];

        public byte[] RawDataCharContent;
        public string devId;
        public bool stopHere = false;

        public int triggerCounter = 0;

        Stopwatch stopwatch = new Stopwatch();
        UInt64 reference_epoch = 1580000000; // s

        public bool end_of_file_offload = false;

       
        int page_length = 2048; //Length of each page logged - in bytes
        public int received_bytes = 0;

        private async void DataCharacteristic_ValueChanged(GattCharacteristic sender, GattValueChangedEventArgs args)
        {

            CryptographicBuffer.CopyToByteArray(args.CharacteristicValue, out RawDataCharContent);

            string newValue = BitConverter.ToString(RawDataCharContent).Replace("-", " ");
            if (newValue.Length != 0)
            {
                // Access to characteristic content as hex array
                string[] ssizes = newValue.Split(whitespace);
                byte[] messageValue = new byte[ssizes.Length];

                for (int i = 0; i < ssizes.Length; i++)
                {
                    int value = Convert.ToInt16(ssizes[i], 16);
                    messageValue[i] = Convert.ToByte(value);
                }

                if (calibrationStreamEnabled == true)
                { acqMode = (byte)Muse_HW.Acquisition_Type.DATA_MODE_AXL; }


                if (enable_file_offload)
                {
                    // Manage log file download from device memory
                    var tasks = new List<Task>();
                    tasks.Add(Task.Run(() => file_offload_container.Add(messageValue)));

                    Task t = Task.WhenAll(tasks);
                    try
                    {
                        t.Wait();
                    }
                    catch { }

                        if (file_offload_container.Count % 16 == 0 || file_offload_container.Count == num_of_chunks)
                        {
                            int bytesToRead = page_length;

                            if (logFileSize_ - received_bytes < page_length)
                            {
                                bytesToRead = logFileSize_ - received_bytes;
                            }

                            received_bytes += bytesToRead;

                            // Send acknowledge to device in order to enable the next file offload iteration
                            byte[] ackBuffer = new byte[20];
                            ackBuffer[0] = 0x00;  // 00
                            ackBuffer[1] = 0x01; // 01
                            ackBuffer[2] = (byte)Muse_HW.Command.CMD_MEM_FILE_DOWNLOAD; // 22

                            await SelectedCharacteristic_cmd.WriteValueAsync(ackBuffer.AsBuffer());

                        }

                        await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => PagesGateway.BlePg.MessageBox.Text = (file_offload_container.Count * 100 / num_of_chunks).ToString() + "%");

                        if (received_bytes >= logFileSize_)
                        {

                        byte[] buffer = file_offload_container.SelectMany(a => a).ToArray();
                        int bytesToRead = page_length;
                        int bytes = 0;

                        while(bytes < logFileSize_)
                        {
                            if (logFileSize_ - bytes < page_length)
                            {
                                bytesToRead = logFileSize_ - bytes;
                            }

                            int packet_offset = 8;
                            int iterations = (bytesToRead - packet_offset) / Muse_Utils.GetPacketDimension(acqMode);
                            float[] tempFloat = new float[3];
                            int[] tempInt = new int[3];
                            int j;

                            UInt64 timestamp = BitConverter.ToUInt64(buffer, 0);
                            timestamp &= 0x000000FFFFFFFFFF;
                            timestamp += ((UInt64)reference_epoch) * 1000;

                            for (int i = 0; i < iterations; i++)
                            {

                                Muse_Data tempData = new Muse_Data();
                                tempData.Timestamp = timestamp;
                                if ((acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_GYRO) > 0)
                                {
                                    //Gyroscope decoding
                                    for (j = 0; j < 3; j++)
                                    {
                                        tempFloat[j] = BitConverter.ToInt16(buffer, bytes + packet_offset + j * 2);
                                        tempFloat[j] *= full_scales.current_gyr_resolution;
                                    }
                                    tempData.Gyro = tempFloat;

                                    packet_offset += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_GYRO;

                                }
                                if ((acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_AXL) > 0)
                                {
                                    //Accelerometer decoding
                                    for (j = 0; j < 3; j++)
                                    {
                                        tempFloat[j] = BitConverter.ToInt16(buffer, bytes + packet_offset + j * 2);
                                        tempFloat[j] *= full_scales.current_axl_resolution;
                                    }
                                    tempData.Axl = tempFloat;
                                    //Packet offset update
                                    packet_offset += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_AXL;
                                }
                                if ((acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_MAGN) > 0)
                                {
                                    //Magnetometer decoding
                                    for (j = 0; j < 3; j++)
                                    {
                                        tempFloat[j] = BitConverter.ToInt16(buffer, bytes + packet_offset + j * 2);
                                        tempFloat[j] *= full_scales.current_mag_resolution;
                                    }
                                    tempData.Magn = tempFloat;
                                    //Packet offset update
                                    packet_offset += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_MAGN;
                                }

                                if ((acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_HDR) > 0)
                                {
                                    //HDR Accelerometer decoding
                                    for (j = 0; j < 3; j++)
                                    {
                                        tempFloat[j] = BitConverter.ToInt16(buffer, bytes + packet_offset + j * 2);
                                        tempFloat[j] *= full_scales.current_hdr_resolution;
                                        tempFloat[j] /= 16;
                                    }
                                    tempData.HDR_Axl = tempFloat;
                                    //Packet offset update
                                    packet_offset += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_HDR;
                                }

                                if ((acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_ORIENTATION) > 0)
                                {
                                    for (j = 0; j < 3; j++)
                                    {
                                        tempFloat[j] = BitConverter.ToInt16(buffer, bytes + packet_offset + j * 2);
                                        tempFloat[j] /= 32767;
                                        tempData.Quaternion[j + 1] = tempFloat[j];
                                    }
                                    //Compute the quaternion real component
                                    tempData.Quaternion[0] = (float)Math.Sqrt(1 - (tempData.Quaternion[1] * tempData.Quaternion[1] +
                                        tempData.Quaternion[2] * tempData.Quaternion[2] + tempData.Quaternion[3] * tempData.Quaternion[3]));

                                    //Packet offset update
                                    packet_offset += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_ORIENTATION;
                                }

                                if ((acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_TIMESTAMP) > 0)
                                {
                                    UInt64 tempTime = BitConverter.ToUInt16(buffer, bytes + packet_offset);
                                    tempTime &= 0x0000FFFFFFFFFFFF;
                                    tempTime += ((UInt64)reference_epoch) * 1000;
                                    tempData.Timestamp = tempTime;
                                    packet_offset += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_TIMESTAMP;
                                }

                                if ((acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_TEMP_HUM) > 0)
                                {

                                    //Temperature and humidity decoding
                                    tempFloat[0] = BitConverter.ToUInt16(buffer, bytes + packet_offset);
                                    //(buffer[packet_offset]<<8)+ buffer[packet_offset+1];
                                    tempFloat[0] *= 0.002670f;
                                    tempFloat[0] -= 45;
                                    tempFloat[1] = BitConverter.ToUInt16(buffer, bytes + packet_offset + 2);
                                    //(buffer[packet_offset+2] << 8) + buffer[packet_offset + 3];
                                    tempFloat[1] *= 0.001907f;
                                    tempFloat[1] -= 6;
                                    tempData.TH = tempFloat;

                                    //Packet offset update
                                    packet_offset += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_TEMP_HUM;
                                }

                                if ((acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_TEMP_PRESS) > 0)
                                {
                                    //Temperature and pressure decoding
                                    tempFloat[0] = BitConverter.ToInt16(buffer, bytes + packet_offset + 3);
                                    tempFloat[0] /= 100;
                                    tempFloat[1] = buffer[bytes + packet_offset] + (buffer[bytes + packet_offset + 1] << 8) + (buffer[bytes + packet_offset + 2] << 16);
                                    tempFloat[1] /= 4096;

                                    tempData.TP = tempFloat;

                                    //Packet offset update
                                    packet_offset += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_TEMP_PRESS;
                                }

                                if ((acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_RANGE) > 0)
                                {
                                    //Temperature and pressure decoding
                                    tempInt[0] = BitConverter.ToUInt16(buffer, bytes + packet_offset);
                                    tempInt[1] = BitConverter.ToUInt16(buffer, bytes + packet_offset + 2);
                                    tempInt[2] = BitConverter.ToUInt16(buffer, bytes + packet_offset + 4);

                                    tempData.Range = tempInt;

                                    //Packet offset update
                                    packet_offset += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_RANGE;
                                }

                                if ((acqMode & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_ENCODER) > 0)
                                {
                                    //Encoder decoding
                                    for (j = 0; j < 3; j++)
                                    {
                                        tempInt[j] = BitConverter.ToInt16(buffer, bytes+ packet_offset + j * 2);
                                        //Sum up the last value retrieved to compute the overall incremental count
                                        if (acquisitionList.Count > 0)
                                        {
                                            tempInt[j] += acquisitionList[acquisitionList.Count - 1].ENC[j];
                                        }
                                    }
                                    tempData.ENC = tempInt;
                                    //Packet offset update
                                    packet_offset += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_ENCODER;
                                }
                                acquisitionList.Add(tempData);
                            }

                            bytes += bytesToRead;


                        }


                        file_offload_container.Clear();
                        end_of_file_offload = true;
                        received_bytes = 0;
                        logData = new List<Muse_Data>();


                        BLE.readFile = false;

                        }

                   

                }
                else
                {
                    // Manage data streaming
                    // Get header: reference timestamp (5 Byte, sec) + mode (3 Byte)
                    byte[] currentTime = new byte[8];
                    Array.Copy(messageValue, 0, currentTime, 0, 5);
                    UInt64 currentTime_val = BitConverter.ToUInt64(currentTime, 0);
                    currentTime_val += ((UInt64)reference_epoch) * 1000;

                    string msgHeaderStr = String.Format("{0}", currentTime_val.ToString());

                    // Show message header

                    // Message parsing based on acquisition mode
                    int packet_dimension = Muse_Utils.GetPacketDimension(acqMode);

                    // Get number of packets within raw message byte array
                    int num_of_packets = 0;
                    if (packet_dimension > 0)
                        num_of_packets = 120 / packet_dimension;

                    // Init list of muse data object

                    int indexOffset = 8; // Initially equal to the default length of the message header

                    string payloadDataStr = "";
                    // Iterate towards the raw message byte array
                    for (int i = 0; i < num_of_packets; i++)
                    {
                        // Init Muse_Data object

                        // Get the current message within payload
                        byte[] tmpRawVal = new byte[packet_dimension];
                        Array.Copy(messageValue, indexOffset, tmpRawVal, 0, packet_dimension);

                        int offset = 0;

                        var currentData = Muse_Utils.retrieveData(acqMode,
                                                                tmpRawVal,
                                                                currentTime_val,
                                                                full_scales.current_axl_resolution, full_scales.current_gyr_resolution, full_scales.current_mag_resolution, full_scales.current_hdr_resolution,
                                                                logData);



                        if ((acqMode & (UInt32)Acquisition_Type.DATA_MODE_GYRO) > 0)
                        {
                            payloadDataStr += currentData.getGyroString() + "\t";
                            offset += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_GYRO;

                            await PagesGateway.BlePg.Dispatcher.InvokeAsync(() =>
                            {
                                if (PagesGateway.BlePg.gyroChart_ != null)
                                {

                                    for (int k = 0; k < 3; k++)
                                    {
                                        if (PagesGateway.BlePg.gyroChart_.chartControl_.GraphPane.CurveList[k].Points.Count >= PagesGateway.BlePg.gyroChart_.chartControl_.GraphPane.XAxis.Scale.Max)
                                        {
                                            PagesGateway.BlePg.gyroChart_.chartControl_.GraphPane.CurveList[k].RemovePoint(0);

                                            for (int j = 0; j < PagesGateway.BlePg.gyroChart_.chartControl_.GraphPane.CurveList[k].Points.Count - 1; j++)
                                                PagesGateway.BlePg.gyroChart_.chartControl_.GraphPane.CurveList[k][j].X -= 1;
                                        }

                                    }

                                    PagesGateway.BlePg.gyroChart_.PlotData(PagesGateway.BlePg.gyroChart_.chartControl_.GraphPane.CurveList[0].Points.Count - 1, currentData.Gyro);

                                    for (int k = 0; k < 3; k++)
                                    {

                                        for (int j = 0; j < PagesGateway.BlePg.gyroChart_.chartControl_.GraphPane.CurveList[k].Points.Count; j++)
                                            if (PagesGateway.BlePg.gyroChart_.chartControl_.GraphPane.CurveList[k][j] == null)
                                            {
                                                for (int z = 0; z < 3; z++)
                                                {
                                                    PagesGateway.BlePg.gyroChart_.chartControl_.GraphPane.CurveList[z].RemovePoint(j);
                                                    for (int w = 0; w < PagesGateway.BlePg.gyroChart_.chartControl_.GraphPane.CurveList[z].Points.Count - 2; j++)
                                                        PagesGateway.BlePg.gyroChart_.chartControl_.GraphPane.CurveList[z][w].X -= 1;
                                                }

                                            }

                                    }

                                    PagesGateway.BlePg.gyroChart_.chartControl_.GraphPane.AxisChange();

                                }
                            }

                            );

                        }

                        if ((acqMode & (UInt32)Acquisition_Type.DATA_MODE_AXL) > 0)
                        {
                            if (calibrationStreamEnabled)
                            {
                                byte[] fullPayload = new byte[(byte)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_AXL * 20];
                                Array.Copy(messageValue, 8, fullPayload, 0, (byte)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_AXL * 20);
                                byte[] samplePayload = new byte[(byte)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_AXL];

                                for (int j = 0; j < 20; j++)
                                {
                                    Array.Copy(fullPayload, j * (byte)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_AXL, samplePayload, 0, (byte)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_AXL);
                                    // Decode current payload
                                    var currentAxl = Muse_Utils.DataTypeAXL(samplePayload, offset, full_scales.current_axl_resolution);
                                    offset += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_AXL;

                                    if (this.cCalibrationType == Muse_HW.Calibration_Type.CALIB_TYPE_AXL)
                                    {
                                        sumX += currentAxl.Axl[0];
                                        sumY += currentAxl.Axl[1];
                                        sumZ += currentAxl.Axl[2];
                                        itCounter++;
                                    }
                                }
                            }
                            else
                            {
                                payloadDataStr += currentData.getAXLString() + "\t";
                                offset += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_AXL;

                                await PagesGateway.BlePg.Dispatcher.InvokeAsync(() =>
                                {

                                    if (PagesGateway.BlePg.accChart_ != null)
                                    {
                                        for (int k = 0; k < 3; k++)
                                        {
                                            if (PagesGateway.BlePg.accChart_.chartControl_.GraphPane.CurveList[k].Points.Count >= PagesGateway.BlePg.accChart_.chartControl_.GraphPane.XAxis.Scale.Max)
                                            {
                                                PagesGateway.BlePg.accChart_.chartControl_.GraphPane.CurveList[k].RemovePoint(0);

                                                for (int j = 0; j < PagesGateway.BlePg.accChart_.chartControl_.GraphPane.CurveList[k].Points.Count - 1; j++)
                                                    PagesGateway.BlePg.accChart_.chartControl_.GraphPane.CurveList[k][j].X -= 1;
                                            }

                                        }

                                        PagesGateway.BlePg.accChart_.PlotData(PagesGateway.BlePg.accChart_.chartControl_.GraphPane.CurveList[0].Points.Count - 1, currentData.Axl);

                                        for (int k = 0; k < 3; k++)
                                        {

                                            for (int j = 0; j < PagesGateway.BlePg.accChart_.chartControl_.GraphPane.CurveList[k].Points.Count; j++)
                                                if (PagesGateway.BlePg.accChart_.chartControl_.GraphPane.CurveList[k][j] == null)
                                                {
                                                    for (int z = 0; z < 3; z++)
                                                    {
                                                        PagesGateway.BlePg.accChart_.chartControl_.GraphPane.CurveList[z].RemovePoint(j);
                                                        for (int w = 0; w < PagesGateway.BlePg.accChart_.chartControl_.GraphPane.CurveList[z].Points.Count - 1; j++)
                                                            PagesGateway.BlePg.accChart_.chartControl_.GraphPane.CurveList[z][w].X -= 1;
                                                    }
                                                }

                                        }


                                        PagesGateway.BlePg.accChart_.chartControl_.GraphPane.AxisChange();

                                    }
                                });
                            }

                        }
                        if ((acqMode & (UInt32)Acquisition_Type.DATA_MODE_HDR) > 0)
                        {
                            payloadDataStr += currentData.getHDRAXLString() + "\t";
                            offset += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_HDR;

                            await PagesGateway.BlePg.Dispatcher.InvokeAsync(() =>
                            {

                                if (PagesGateway.BlePg.accHDRChart_ != null)
                                {
                                    for (int k = 0; k < 3; k++)
                                    {
                                        if (PagesGateway.BlePg.accHDRChart_.chartControl_.GraphPane.CurveList[k].Points.Count >= PagesGateway.BlePg.accHDRChart_.chartControl_.GraphPane.XAxis.Scale.Max)
                                        {
                                            PagesGateway.BlePg.accHDRChart_.chartControl_.GraphPane.CurveList[k].RemovePoint(0);

                                            for (int j = 0; j < PagesGateway.BlePg.accHDRChart_.chartControl_.GraphPane.CurveList[k].Points.Count - 1; j++)
                                                PagesGateway.BlePg.accHDRChart_.chartControl_.GraphPane.CurveList[k][j].X -= 1;
                                        }

                                    }


                                    PagesGateway.BlePg.accHDRChart_.PlotData(PagesGateway.BlePg.accHDRChart_.chartControl_.GraphPane.CurveList[0].Points.Count - 1, currentData.HDR_Axl);

                                    for (int k = 0; k < 3; k++)
                                    {

                                        for (int j = 0; j < PagesGateway.BlePg.accHDRChart_.chartControl_.GraphPane.CurveList[k].Points.Count; j++)
                                            if (PagesGateway.BlePg.accHDRChart_.chartControl_.GraphPane.CurveList[k][j] == null)
                                            {
                                                for (int z = 0; z < 3; z++)
                                                {
                                                    PagesGateway.BlePg.accHDRChart_.chartControl_.GraphPane.CurveList[z].RemovePoint(j);
                                                    for (int w = 0; w < PagesGateway.BlePg.accHDRChart_.chartControl_.GraphPane.CurveList[z].Points.Count - 1; j++)
                                                        PagesGateway.BlePg.accHDRChart_.chartControl_.GraphPane.CurveList[z][w].X -= 1;
                                                }
                                            }

                                    }

                                    PagesGateway.BlePg.accHDRChart_.chartControl_.GraphPane.AxisChange();

                                }

                            });
                        }
                        if ((acqMode & (UInt32)Acquisition_Type.DATA_MODE_MAGN) > 0)
                        {
                            payloadDataStr += currentData.getMagnString() + "\t";
                            offset += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_MAGN;

                            await PagesGateway.BlePg.Dispatcher.InvokeAsync(() =>
                            {

                                if (PagesGateway.BlePg.spaceChart_ != null)
                                    PagesGateway.BlePg.spaceChart_.ViewModel.Radius = currentData.Magn[0] / 50;

                                if (PagesGateway.BlePg.magnChart_ != null && currentData.Magn != null)
                                {
                                    for (int k = 0; k < 3; k++) { 
                                        if (PagesGateway.BlePg.magnChart_.chartControl_.GraphPane.CurveList[k].Points.Count >= PagesGateway.BlePg.magnChart_.chartControl_.GraphPane.XAxis.Scale.Max)
                                        {
                                            PagesGateway.BlePg.magnChart_.chartControl_.GraphPane.CurveList[k].RemovePoint(0);

                                            for (int j = 0; j < PagesGateway.BlePg.magnChart_.chartControl_.GraphPane.CurveList[k].Points.Count - 1; j++)
                                                PagesGateway.BlePg.magnChart_.chartControl_.GraphPane.CurveList[k][j].X -= 1;
                                        }

                                    }

                                    PagesGateway.BlePg.magnChart_.PlotData(PagesGateway.BlePg.magnChart_.chartControl_.GraphPane.CurveList[0].Points.Count - 1, currentData.Magn);

                                    for (int k = 0; k < 3; k++)
                                    {
                                        for (int j = 0; j < PagesGateway.BlePg.magnChart_.chartControl_.GraphPane.CurveList[k].Points.Count; j++)
                                            if (PagesGateway.BlePg.magnChart_.chartControl_.GraphPane.CurveList[k][j] == null)
                                            {
                                                for (int z = 0; z < 3; z++)
                                                {
                                                    PagesGateway.BlePg.magnChart_.chartControl_.GraphPane.CurveList[z].RemovePoint(j);
                                                    for (int w = 0; w < PagesGateway.BlePg.magnChart_.chartControl_.GraphPane.CurveList[z].Points.Count - 1; j++)
                                                        PagesGateway.BlePg.magnChart_.chartControl_.GraphPane.CurveList[z][w].X -= 1;
                                                }
                                            }
                                    }


                                    PagesGateway.BlePg.magnChart_.chartControl_.GraphPane.AxisChange();

                                }
                            });

                        }
                        if ((acqMode & (UInt32)Acquisition_Type.DATA_MODE_ORIENTATION) > 0)
                        {
                            payloadDataStr += currentData.getOrientationString() + "\t";
                            offset += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_ORIENTATION;

                            await PagesGateway.BlePg.Dispatcher.InvokeAsync(() =>
                            {

                                if (PagesGateway.BlePg.spaceChart_ != null && currentData.Quaternion != null)
                                    PagesGateway.BlePg.spaceChart_.ViewModel.Quaternion = new System.Windows.Media.Media3D.Quaternion(currentData.Quaternion[1],
                                                                                                                                   currentData.Quaternion[2],
                                                                                                                                   currentData.Quaternion[3],
                                                                                                                                   currentData.Quaternion[0]);

                                if (PagesGateway.BlePg.quatChart_ != null)
                                {
                                    for (int k = 0; k < 4; k++)
                                    {
                                        if (PagesGateway.BlePg.quatChart_.chartControl_.GraphPane.CurveList[k].Points.Count >= PagesGateway.BlePg.quatChart_.chartControl_.GraphPane.XAxis.Scale.Max)
                                        {
                                            PagesGateway.BlePg.quatChart_.chartControl_.GraphPane.CurveList[k].RemovePoint(0);

                                            for (int j = 0; j < PagesGateway.BlePg.quatChart_.chartControl_.GraphPane.CurveList[k].Points.Count - 1; j++)
                                                PagesGateway.BlePg.quatChart_.chartControl_.GraphPane.CurveList[k][j].X -= 1;
                                        }

                                    }


                                    PagesGateway.BlePg.quatChart_.PlotData(PagesGateway.BlePg.quatChart_.chartControl_.GraphPane.CurveList[0].Points.Count - 1, currentData.Quaternion);

                                    for (int k = 0; k < 4; k++)
                                    {

                                        for (int j = 0; j < PagesGateway.BlePg.quatChart_.chartControl_.GraphPane.CurveList[k].Points.Count; j++)
                                            if (PagesGateway.BlePg.quatChart_.chartControl_.GraphPane.CurveList[k][j] == null)
                                            {

                                                for (int z = 0; z < 4; z++)
                                                {
                                                    PagesGateway.BlePg.quatChart_.chartControl_.GraphPane.CurveList[z].RemovePoint(j);
                                                    for (int w = 0; w < PagesGateway.BlePg.quatChart_.chartControl_.GraphPane.CurveList[z].Points.Count - 1; j++)
                                                        PagesGateway.BlePg.quatChart_.chartControl_.GraphPane.CurveList[z][w].X -= 1;
                                                }
                                            }

                                    }

                                    PagesGateway.BlePg.quatChart_.chartControl_.GraphPane.AxisChange();

                                }
                            });
                        }
                        if ((acqMode & (UInt32)Acquisition_Type.DATA_MODE_TIMESTAMP) > 0)
                        {
                            var currentTimestamp = Muse_Utils.DataTypeTimestamp(tmpRawVal, offset);
                            payloadDataStr = currentTimestamp.getTimestampString() + "\t";
                            offset += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_TIMESTAMP;

                        }
                        if ((acqMode & (UInt32)Acquisition_Type.DATA_MODE_TEMP_HUM) > 0)
                        {
                            var currentTempHum = Muse_Utils.DataTypeTempHum(tmpRawVal, offset);
                            payloadDataStr += currentTempHum.getTHString() + "\t";
                            offset += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_TEMP_HUM;
                        }
                        if ((acqMode & (UInt32)Acquisition_Type.DATA_MODE_TEMP_PRESS) > 0)
                        {
                            var currentTempPress = Muse_Utils.DataTypeTempPress(tmpRawVal, offset);
                            payloadDataStr += currentTempPress.getTPString() + "\t";
                            offset += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_TEMP_PRESS;
                        }
                        // if ((acqMode & (UInt32)Acquisition_Type.DATA_MODE_RANGE) > 0)
                        if ((acqMode & (UInt32)Acquisition_Type.DATA_MODE_ENCODER) > 0)
                        {
                            var currentEncoder = Muse_Utils.DataTypeEncoder(tmpRawVal, offset, logData);
                            if (encData != null)
                                encData.Add(currentEncoder);
                            payloadDataStr += currentEncoder.ToString() + "\t";
                            offset += (int)Muse_HW.AcquisitionPayloadSize.DATA_SIZE_ENCODER;
                        }

                        // Update array index offset before the next iteration
                        indexOffset += packet_dimension;

                        // Show decoded payload content
                        await PagesGateway.BlePg.Dispatcher.InvokeAsync(() => PagesGateway.BlePg.MessageBox.Text = payloadDataStr);


                        payloadDataStr = "";

                        iterationCounter++;
                    }
                }

            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
