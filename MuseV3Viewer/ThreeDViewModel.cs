﻿using HelixToolkit.Wpf;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace MuseV3Viewer
{
    public class ThreeDViewModel : INotifyPropertyChanged
    {
        private string PCB_MODEL_PATH = @"museV3_v3.4.stl";
        public Model3D Model { get; set; }

        private Quaternion quat_;

        private double rValue_;
        private double tValue_;
        private double pValue_;

        public ThreeDViewModel()
        {
            ModelImporter importer = new ModelImporter();

            Material material = new DiffuseMaterial(new SolidColorBrush(Colors.Beige));
            importer.DefaultMaterial = material;

            Model3D pcb = importer.Load(PCB_MODEL_PATH);

            Transform3DGroup pcb_transform = new Transform3DGroup();
            RotateTransform3D pcb_rotation = new RotateTransform3D(new AxisAngleRotation3D(new Vector3D(0, 0, 1), -90));
            TranslateTransform3D pcb_translation = new TranslateTransform3D(-12.5, 12.5, 0);
            pcb_transform.Children.Add(pcb_rotation);
            pcb_transform.Children.Add(pcb_translation);
            pcb.Transform = pcb_transform;

            Model = pcb;
        }

        public Quaternion Quaternion
        {
            get { return quat_; }
            set
            {
                if (quat_ != value)
                {
                    quat_ = value;
                    OnPropertyChanged();
                }
            }
        }

        public double Radius
        {
            get { return rValue_; }
            set
            {
                if (rValue_ != value)
                {
                    rValue_ = value;
                    OnPropertyChanged();
                }
            }
        }

        public double Theta
        {
            get { return tValue_; }
            set
            {
                if (tValue_ != value)
                {
                    tValue_ = value;
                    OnPropertyChanged();
                }
            }
        }

        public double Phi
        {
            get { return pValue_; }
            set
            {
                if (pValue_ != value)
                {
                    pValue_ = value;
                    OnPropertyChanged();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
