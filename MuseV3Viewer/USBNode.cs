﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Timers;

using _221e.Muse;

namespace MuseV3Viewer
{
    public delegate void Muse_packetUploaded(object sender, int value, bool state);

    public delegate void Muse_pageDownloaded(object sender, int value);

    public class USBNode
    {
        private System.Timers.Timer spTimer_;           // Timer

        public struct AcquisitionStamped
        {
            public UInt16 id;
            public string log_mode;
            public int log_frequency;
            public List<string> fs;
            public uint file_mode;
            public List<Muse_Data> acquisitionList;
        }


        // The COM port
        private string _portName;
        private SerialPort comPort = new SerialPort();

        public event Muse_packetUploaded packetUploaded;

        public event Muse_pageDownloaded newLogSample;

        public List<byte[]> byteList = new List<byte[]>();
        public List<List<byte[]>> tempByteQueue = new List<List<byte[]>>();


        public List<Muse_Data> acquisitionList = new List<Muse_Data>();
        public List<AcquisitionStamped> tempQueue = new List<AcquisitionStamped>();
        UInt16 currentFileIndex = 0;

        //public List<DateTime> file_list = new List<DateTime>();
        public List<string> file_list = new List<string>();
        public List<UInt32> log_mode_list = new List<UInt32>();
        public List<int> log_freq_list = new List<int>();
        public List<float> axl_fs_list = new List<float>();
        public List<float> gyro_fs_list = new List<float>();
        public List<float> magn_fs_list = new List<float>();
        public List<float> hdr_axl_fs_list = new List<float>();

        public List<string> axl_fs_list_str = new List<string>();
        public List<string> gyro_fs_list_str = new List<string>();
        public List<string> magn_fs_list_str = new List<string>();
        public List<string> hdr_axl_fs_list_str = new List<string>();

        public bool acqLog = false;

        int file_dimension = 0;//dimension of the file read out - in bytes
        int received_bytes = 0;//bytes received in file readout mode
        int page_length = 2048; //Length of each page logged - in bytes
        int current_readout_file_index = 0;
        int ack_ko_attempts = 0;
        int current_file = 0;
        UInt64 reference_epoch = 1580000000; // s

        float axl_base_sensitivity = 0.122f;
        float gyro_base_sensitivity = 0.00875f;
        float hdr_axl_base_sensitivity = 49;
        float magn_base_sensitivity = 1000.0f / 6842.0f;

        public USBNode(string comName)
        {
            _portName = comName;
            try
            {
                //set the properties of our SerialPort Object
                comPort.BaudRate = 921600;    //BaudRate
                comPort.DataBits = 8;    //DataBits
                comPort.StopBits = StopBits.One;    //StopBits
                comPort.Parity = Parity.None; //Parity
                comPort.PortName = _portName;   //PortName
                comPort.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler);
                byteList = new List<byte[]>();
                acquisitionList = new List<Muse_Data>();
                comPort.DtrEnable = true;

                //first check if the port is already open. If its open then close it
                if (comPort.IsOpen == true)
                    comPort.Close();


                //open the port
                while (comPort.IsOpen == false)
                {
                    comPort.Open();
                }
                //display message
                Console.WriteLine("Port " + _portName + " opened at " + DateTime.Now + "\n");
            }
            catch (Exception ex)
            {
                Console.WriteLine("First attempt to open port " + _portName + " failed. Re-connecting...");
                //open the port
                while (comPort.IsOpen == false)
                {
                    comPort.Open();
                }
                //display message
                Console.WriteLine("Port " + _portName + " opened at " + DateTime.Now + "\n");
            }
            //Flush the buffer
            emptyBuffer();
            //comPort.ReceivedBytesThreshold = 512;
            spTimer_ = new System.Timers.Timer(500);//FIXME
            spTimer_.Elapsed += new ElapsedEventHandler(PacketKO);
        }

        public void PacketKO(object sender, EventArgs e)
        {
            spTimer_.Stop();
            comPort.DiscardInBuffer();
            byte[] buffer = new byte[8];
            buffer[0] = (byte)Muse_HW.Command.CMD_MEM_FILE_DOWNLOAD;
            buffer[1] = 0x01;
            transmitMessage(Muse_HW.Command.CMD_ACK, 2, buffer, false); ;
            ack_ko_attempts++;
            if (ack_ko_attempts > 5)
            {
                //Give up
                acqLog = false;
                ack_ko_attempts = 0;
                received_bytes = 0;
            }
            else
            {
                spTimer_.Start();
            }
        }

        #region Other commands

        public void closeConnection()
        {
            this.comPort.Close();
        }

        #endregion

        private void ByteReceivedHandler(
                      object sender,
                      SerialDataReceivedEventArgs e)
        {
            if (acqLog)
            {
                byte[] buffer = new byte[page_length + 4];
                int bytesToRead = page_length;
                if (file_dimension - received_bytes < page_length)
                {
                    bytesToRead = file_dimension - received_bytes;
                }
                //Increase bytes to read to account the trailer - !!!!
                bytesToRead += 4;

                if (comPort.BytesToRead >= bytesToRead)
                {
                    comPort.Read(buffer, 0, bytesToRead);
                    if (buffer[bytesToRead - 4] == '!' && buffer[bytesToRead - 3] == '!' && buffer[bytesToRead - 2] == '!' && buffer[bytesToRead - 1] == '!')
                    {
                        // Stop timer
                        spTimer_.Stop();
                        ack_ko_attempts = 0;
                        int packet_offset = 8;
                        int iterations = (bytesToRead - 4 - packet_offset) / (Muse_Utils.GetPacketDimension(log_mode_list[current_file]));
                        float[] tempFloat = new float[3];
                        int[] tempInt = new int[3];
                        int j;


                        byte[] acq = new byte[bytesToRead-4];
                        Buffer.BlockCopy(buffer, 0, acq, 0, bytesToRead-4);

                        byteList.Add(acq);

                        received_bytes += (bytesToRead - 4);

                        buffer[0] = (byte)Muse_HW.Command.CMD_MEM_FILE_DOWNLOAD;
                        buffer[1] = 0x00;
                        transmitMessage(Muse_HW.Command.CMD_ACK, 2, buffer, false);
                        comPort.DiscardInBuffer();

                        //Deal with a new packet
                        if (newLogSample != null)
                            newLogSample(this, (int)(((float)(received_bytes) / (float)(file_dimension)) * 100));

                        //Start timer
                        spTimer_.Start();
                    }
                    else
                    {
                        int a = 0;
                        a++;
                    }
                }
                else
                {
                    int a = 0;
                    a++;
                }

                if (received_bytes >= file_dimension)
                {
                    acqLog = false;
                    file_dimension = 0;
                    received_bytes = 0;

                    List<byte[]> temp = new List<byte[]>(byteList);
                    tempByteQueue.Add(temp);

                    // Stop timer
                    spTimer_.Stop();

                }
            }
        }

        private void DataReceivedHandler(
                       object sender,
                       SerialDataReceivedEventArgs e)
        {
            if (acqLog)
            {
                byte[] buffer = new byte[page_length + 4];
                int bytesToRead = page_length;
                if (file_dimension - received_bytes < page_length)
                {
                    bytesToRead = file_dimension - received_bytes;
                }
                //Increase bytes to read to account the trailer - !!!!
                bytesToRead += 4;

                if (comPort.BytesToRead >= bytesToRead)
                {
                    comPort.Read(buffer, 0, bytesToRead);
                    if (buffer[bytesToRead - 4] == '!' && buffer[bytesToRead - 3] == '!' && buffer[bytesToRead - 2] == '!' && buffer[bytesToRead - 1] == '!')
                    {
                        // Stop timer
                        spTimer_.Stop();
                        ack_ko_attempts = 0;
                        int packet_offset = 8;
                        int iterations = (bytesToRead - 4 - packet_offset) / (Muse_Utils.GetPacketDimension(log_mode_list[current_file]));
                        float[] tempFloat = new float[3];
                        int[] tempInt = new int[3];
                        int j;

                        UInt64 timestamp = BitConverter.ToUInt64(buffer, 0);
                        timestamp &= 0x000000FFFFFFFFFF;
                        timestamp += ((UInt64)reference_epoch) * 1000;

                        for (int i = 0; i < iterations; i++)
                        {
                            Muse_Data tempData = new Muse_Data();
                            tempData.Timestamp = timestamp;
                            if ((log_mode_list[current_file] & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_GYRO) > 0)
                            {
                                //Gyroscope decoding
                                for (j = 0; j < 3; j++)
                                {
                                    tempFloat[j] = BitConverter.ToInt16(buffer, packet_offset + j * 2);
                                    tempFloat[j] *= gyro_fs_list[current_file];
                                }
                                tempData.Gyro = tempFloat;

                                packet_offset += Muse_Utils.GetPacketDimension((UInt32)Muse_HW.Acquisition_Type.DATA_MODE_GYRO);

                            }
                            if ((log_mode_list[current_file] & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_AXL) > 0)
                            {
                                //Accelerometer decoding
                                for (j = 0; j < 3; j++)
                                {
                                    tempFloat[j] = BitConverter.ToInt16(buffer, packet_offset + j * 2);
                                    tempFloat[j] *= axl_fs_list[current_file];
                    }
                                tempData.Axl = tempFloat;
                                //Packet offset update
                                packet_offset += Muse_Utils.GetPacketDimension((UInt32)Muse_HW.Acquisition_Type.DATA_MODE_AXL);
                            }
                            if ((log_mode_list[current_file] & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_MAGN) > 0)
                            {
                                //Magnetometer decoding
                                for (j = 0; j < 3; j++)
                                {
                                    tempFloat[j] = BitConverter.ToInt16(buffer, packet_offset + j * 2);
                                    tempFloat[j] *= magn_fs_list[current_file];
                                }
                                tempData.Magn = tempFloat;
                                //Packet offset update
                                packet_offset += Muse_Utils.GetPacketDimension((UInt32)Muse_HW.Acquisition_Type.DATA_MODE_MAGN);
                            }

                            if ((log_mode_list[current_file] & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_HDR) > 0)
                            {
                                //HDR Accelerometer decoding
                                for (j = 0; j < 3; j++)
                                {
                                    tempFloat[j] = BitConverter.ToInt16(buffer, packet_offset + j * 2);
                                    tempFloat[j] *= hdr_axl_fs_list[current_file];
                                    tempFloat[j] /= 16;
                                }
                                tempData.HDR_Axl = tempFloat;
                                //Packet offset update
                                packet_offset += Muse_Utils.GetPacketDimension((UInt32)Muse_HW.Acquisition_Type.DATA_MODE_HDR);
                            }

                            if ((log_mode_list[current_file] & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_ORIENTATION) > 0)
                            {
                                for (j = 0; j < 3; j++)
                                {
                                    tempFloat[j] = BitConverter.ToInt16(buffer, packet_offset + j * 2);
                                    tempFloat[j] /= 32767;
                                    tempData.Quaternion[j + 1] = tempFloat[j];
                                }
                                //Compute the quaternion real component
                                tempData.Quaternion[0] = (float)Math.Sqrt(1 - (tempData.Quaternion[1] * tempData.Quaternion[1] +
                                    tempData.Quaternion[2] * tempData.Quaternion[2] + tempData.Quaternion[3] * tempData.Quaternion[3]));

                                //Packet offset update
                                packet_offset += Muse_Utils.GetPacketDimension((UInt32)Muse_HW.Acquisition_Type.DATA_MODE_ORIENTATION);
                            }

                            if ((log_mode_list[current_file] & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_TIMESTAMP) > 0)
                            {
                                UInt64 tempTime = BitConverter.ToUInt16(buffer, packet_offset);
                                tempTime &= 0x0000FFFFFFFFFFFF;
                                tempTime += ((UInt64)reference_epoch) * 1000;
                                tempData.Timestamp = tempTime;
                                packet_offset += Muse_Utils.GetPacketDimension((UInt32)Muse_HW.Acquisition_Type.DATA_MODE_TIMESTAMP);
                            }

                            if ((log_mode_list[current_file] & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_TEMP_HUM) > 0)
                            {

                                //Temperature and humidity decoding
                                tempFloat[0] = BitConverter.ToUInt16(buffer, packet_offset);
                                //(buffer[packet_offset]<<8)+ buffer[packet_offset+1];
                                tempFloat[0] *= 0.002670f;
                                tempFloat[0] -= 45;
                                tempFloat[1] = BitConverter.ToUInt16(buffer, packet_offset + 2);
                                //(buffer[packet_offset+2] << 8) + buffer[packet_offset + 3];
                                tempFloat[1] *= 0.001907f;
                                tempFloat[1] -= 6;
                                tempData.TH = tempFloat;

                                //Packet offset update
                                packet_offset += Muse_Utils.GetPacketDimension((UInt32)Muse_HW.Acquisition_Type.DATA_MODE_TEMP_HUM);
                            }

                            if ((log_mode_list[current_file] & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_TEMP_PRESS) > 0)
                            {
                                //Temperature and pressure decoding
                                tempFloat[0] = BitConverter.ToInt16(buffer, packet_offset + 3);
                                tempFloat[0] /= 100;
                                tempFloat[1] = buffer[packet_offset] + (buffer[packet_offset + 1] << 8) + (buffer[packet_offset + 2] << 16);
                                tempFloat[1] /= 4096;

                                tempData.TP = tempFloat;

                                //Packet offset update
                                packet_offset += Muse_Utils.GetPacketDimension((UInt32)Muse_HW.Acquisition_Type.DATA_MODE_TEMP_PRESS);
                            }

                            if ((log_mode_list[current_file] & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_RANGE) > 0)
                            {
                                //Temperature and pressure decoding
                                tempInt[0] = BitConverter.ToUInt16(buffer, packet_offset);
                                tempInt[1] = BitConverter.ToUInt16(buffer, packet_offset + 2);
                                tempInt[2] = BitConverter.ToUInt16(buffer, packet_offset + 4);

                                tempData.Range = tempInt;

                                //Packet offset update
                                packet_offset += Muse_Utils.GetPacketDimension((UInt32)Muse_HW.Acquisition_Type.DATA_MODE_RANGE);
                            }

                            if ((log_mode_list[current_file] & (UInt32)Muse_HW.Acquisition_Type.DATA_MODE_ENCODER) > 0)
                            {
                                //Encoder decoding
                                for (j = 0; j < 3; j++)
                                {
                                    tempInt[j] = BitConverter.ToInt16(buffer, packet_offset + j * 2);
                                    //Sum up the last value retrieved to compute the overall incremental count
                                    if (acquisitionList.Count > 0)
                                    {
                                        tempInt[j] += acquisitionList[acquisitionList.Count - 1].ENC[j];
                                    }
                                }
                                tempData.ENC = tempInt;
                                //Packet offset update
                                packet_offset += Muse_Utils.GetPacketDimension((UInt32)Muse_HW.Acquisition_Type.DATA_MODE_ENCODER);
                            }
                            acquisitionList.Add(tempData);
                        }

                        received_bytes += (bytesToRead - 4);

                        buffer[0] = (byte)Muse_HW.Command.CMD_MEM_FILE_DOWNLOAD;
                        buffer[1] = 0x00;
                        transmitMessage(Muse_HW.Command.CMD_ACK, 2, buffer, false);
                        comPort.DiscardInBuffer();

                        //Deal with a new packet
                        if (newLogSample != null)
                            newLogSample(this, (int)(((float)(received_bytes) / (float)(file_dimension)) * 100));

                        //Start timer
                        spTimer_.Start();
                    }
                    else
                    {
                        int a = 0;
                        a++;
                    }
                }
                else
                {
                    int a = 0;
                    a++;
                }

                if (received_bytes >= file_dimension)
                {

                    AcquisitionStamped tempAcq;

                    tempAcq.id = currentFileIndex;
                    tempAcq.log_mode = Muse_Utils.ExtractMode(log_mode_list[current_file]);
                    tempAcq.log_frequency = getLogFreq();
                    tempAcq.fs = getCurrentFS();
                    tempAcq.file_mode = getCurrentFileMode();
                    tempAcq.acquisitionList = new List<Muse_Data>(acquisitionList);
                    tempQueue.Add(tempAcq);

                    acqLog = false;
                    file_dimension = 0;
                    received_bytes = 0;

                    // Stop timer
                    spTimer_.Stop();

                }
            }
        }

        public void emptyBuffer()
        {
            comPort.DiscardInBuffer();
            comPort.DiscardOutBuffer();
        }

        public void startApplication()
        {
            emptyBuffer();
            byte[] b = new byte[10];
            b[0] = 0x00;
            transmitMessage(Muse_HW.Command.CMD_RESTART, 1, b, false);
        }

        public bool startBootLoader()
        {
            emptyBuffer();
            byte[] b = new byte[10];
            b[0] = 0x01;
            transmitMessage(Muse_HW.Command.CMD_RESTART, 1, b, false);
            if (receiveMessage(b) > 0)
            {
                if (b[2] == 0x00 && b[4] == (byte)(Muse_HW.Command.CMD_RESTART) && b[5] == 0x00)
                {
                    return true;
                }
            }
            return false;
        }

        public byte getState()
        {
            emptyBuffer();

            byte state = 0x00; // SYS_NULL

            byte[] b = new byte[20];
            transmitMessage(Muse_HW.Command.CMD_STATE, 0, b, true);
            if (receiveMessage(b) > 0)
            {
                if (b[2] == 0x00 && b[4] == (byte)(Muse_HW.Command.CMD_STATE + 0x80) && b[5] == 0x00)
                {
                    state = b[6];
                }
            }
            return state;
        }

        public int getBatteryCharge()
        {
            emptyBuffer();
            byte[] b = new byte[20];
            transmitMessage(Muse_HW.Command.CMD_BATTERY_CHARGE, 0, b, true);
            if (receiveMessage(b) > 0)
            {
                if (b[2] == 0x00 && b[4] == (byte)(Muse_HW.Command.CMD_BATTERY_CHARGE + 0x80) && b[5] == 0x00)
                {
                    return (int)b[6];
                }
            }
            return 0;
        }

        public float getBatteryVoltage()
        {
            emptyBuffer();
            byte[] b = new byte[20];
            transmitMessage(Muse_HW.Command.CMD_BATTERY_VOLTAGE, 0, b, true);
            if (receiveMessage(b) > 0)
            {
                if (b[2] == 0x00 && b[4] == (byte)(Muse_HW.Command.CMD_BATTERY_VOLTAGE + 0x80) && b[5] == 0x00)
                {
                    UInt16 temp_batt = BitConverter.ToUInt16(b, 6);
                    return temp_batt;
                }
            }
            return 0;
        }

        public string[] getFW()
        {
            emptyBuffer();
            string[] result = new string[2] { "0.0.00", "0.0.00" };
            byte[] b = new byte[50];
            transmitMessage(Muse_HW.Command.CMD_FW_VERSION, 0, b, true);
            if (receiveMessage(b) > 0)
            {
                if (b[2] == 0x00 && b[4] == (byte)(Muse_HW.Command.CMD_FW_VERSION + 0x80) && b[5] == 0x00)
                {
                    byte[] tempString = new byte[(b[3] - 2) / 2];
                    for (int i = 0; i < 7; i++)
                        tempString[i] = b[6 + i];
                    result[0] = System.Text.Encoding.UTF8.GetString(tempString);
                    for (int i = 0; i < 7; i++)
                        tempString[i] = b[6 + 7 + i];
                    result[1] = System.Text.Encoding.UTF8.GetString(tempString);
                }
            }
            return result;
        }

        public bool setDate()
        {
            emptyBuffer();
            TimeSpan timeSpan = DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
            UInt32 secondsSinceEpoch = (UInt32)timeSpan.TotalSeconds;
            byte[] b = BitConverter.GetBytes(secondsSinceEpoch);
            if (!BitConverter.IsLittleEndian)
                Array.Reverse(b);
            transmitMessage(Muse_HW.Command.CMD_TIME, 4, b, false);
            b = new byte[50];
            if (receiveMessage(b) > 0)
            {
                if (b[2] == 0x00 && b[4] == (byte)(Muse_HW.Command.CMD_TIME) && b[5] == 0x00)
                {
                    return true;
                }
            }
            return false;
        }

        public DateTime getDate()
        {
            emptyBuffer();
            DateTime result = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            byte[] b = new byte[20];
            transmitMessage(Muse_HW.Command.CMD_TIME, 0, b, true);
            DateTime t = DateTime.Now;
            while (comPort.BytesToRead == 0 && (DateTime.Now - t).TotalMilliseconds < 2000) ;

            if ((DateTime.Now - t).TotalMilliseconds < 6000)
            {
                comPort.Read(b, 0, 12);
                if (b[0] == '?' && b[1] == '!' && b[10] == '!' && b[11] == '?')
                {
                    if (b[2] == 0x00 && b[4] == (byte)(Muse_HW.Command.CMD_TIME + 0x80) && b[5] == 0x00)
                    {
                        int timeStamp = BitConverter.ToInt32(b, 6);
                        result = result.AddSeconds(timeStamp);
                        return result;
                    }
                }
            }
            return result;
        }

        public string getID()
        {
            emptyBuffer();
            byte[] b = new byte[20];
            transmitMessage(Muse_HW.Command.CMD_DEVICE_ID, 0, b, true);
            if (receiveMessage(b) > 0)
            {
                if (b[2] == 0x00 && b[4] == (byte)(Muse_HW.Command.CMD_DEVICE_ID + 0x80))
                {
                    UInt32 temp_id = BitConverter.ToUInt32(b, 6);
                    return temp_id.ToString("X8");
                }
            }
            return "";
        }

        public int getMemoryStatus()
        {
            emptyBuffer();
            byte[] b = new byte[5000];
            transmitMessage(Muse_HW.Command.CMD_MEM_CONTROL, 0, b, true);
            if (receiveMessage(b) > 0)
            {
                if (b[2] == 0x00 && b[4] == (byte)(Muse_HW.Command.CMD_MEM_CONTROL + 0x80) && b[5] == 0x00)
                {
                    int mem_available = b[6];
                    int numFiles = BitConverter.ToUInt16(b, 7);
                    if (numFiles > 0)
                    {
                        file_list.Clear();
                        log_mode_list.Clear();
                        log_freq_list.Clear();
                        axl_fs_list.Clear();
                        for (int i = 0; i < numFiles; i++)
                        {
                            b = BitConverter.GetBytes((UInt16)i);
                            if (!BitConverter.IsLittleEndian)
                                Array.Reverse(b);
                            transmitMessage(Muse_HW.Command.CMD_MEM_FILE_INFO, 2, b, true);
                            b = new byte[5000];
                            if (receiveMessage(b) > 0)
                            {
                                if (b[2] == 0x00 && b[4] == (byte)(Muse_HW.Command.CMD_MEM_FILE_INFO + 0x80) && b[5] == 0x00)
                                {
                                    UInt64 temp_time = BitConverter.ToUInt64(b, 6);
                                    temp_time &= 0x000000FFFFFFFFFF;
                                    temp_time /= 1000;
                                    temp_time += reference_epoch;
                                    DateTime d = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                                    d = d.AddSeconds(temp_time);
                                    string formattedDate = d.ToString("dd/MM/yyyy HH:mm:ss");
                                    //file_list.Add(d);
                                    file_list.Add(formattedDate);
                                    byte temp_fs = b[11];
                                    Muse_HW.FullScales full_scales = Muse_Utils.ExtractMEMSResolutions(temp_fs);

                                    gyro_fs_list.Add(full_scales.current_gyr_resolution);
                                    axl_fs_list.Add(full_scales.current_axl_resolution);
                                    hdr_axl_fs_list.Add(full_scales.current_hdr_resolution);
                                    magn_fs_list.Add(full_scales.current_mag_resolution);

                                    gyro_fs_list_str.Add(full_scales.gyr_fsStr);
                                    axl_fs_list_str.Add(full_scales.axl_fsStr);
                                    hdr_axl_fs_list_str.Add(full_scales.hdr_fsStr);
                                    magn_fs_list_str.Add(full_scales.mag_fsStr);

                                    UInt32 temp_mode = BitConverter.ToUInt32(b, 12);
                                    temp_mode &= 0x00FFFFFF;
                                    log_mode_list.Add(temp_mode);
                                    log_freq_list.Add(b[15]);
                                }
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        //file_list = new List<DateTime>();
                        file_list = new List<string>();
                    }

                    return mem_available;
                }
            }
            return 0;
        }


        public bool eraseMemory(byte bulk_erase)
        {
            emptyBuffer();
            byte[] b = new byte[20];
            b[0] = bulk_erase;
            transmitMessage(Muse_HW.Command.CMD_MEM_CONTROL, 1, b, false);
            if (receiveMessage(b) > 0)
            {
                if (b[2] == 0x00 && b[4] == (byte)(Muse_HW.Command.CMD_MEM_CONTROL) && b[5] == 0x00)
                {

                    return true;
                }
            }
            return false;
        }

        public bool readFile(UInt16 arg)
        {
            if (arg >= 0)
            {
                currentFileIndex = arg;
                acquisitionList.Clear();
                byteList.Clear();
                emptyBuffer();
                byte[] b1 = BitConverter.GetBytes((UInt16)arg);
                byte[] b = new byte[20];
                b1.CopyTo(b, 0);
                b[2] = 0;
                transmitMessage(Muse_HW.Command.CMD_MEM_FILE_DOWNLOAD, 3, b, false);

                if (receiveMessage(b) > 0)
                {
                    file_dimension = (int)BitConverter.ToUInt32(b, 6);
                    if (file_dimension > 0)
                    {
                        current_readout_file_index = arg;
                        acqLog = true;
                        current_file = arg;
                        ack_ko_attempts = 0;
                        received_bytes = 0;

                        b[0] = (byte)Muse_HW.Command.CMD_MEM_FILE_DOWNLOAD;
                        b[1] = 0x00;
                        transmitMessage(Muse_HW.Command.CMD_ACK, 2, b, false);


                        //Start timer
                        spTimer_.Start();
                        return true;
                    }
                }
            }
            return false;
        }

        public int getFS()
        {
            //emptyBuffer();
            //byte[] b = new byte[20];
            //transmitMessage(Command.CMD_FS, 0, b, true);
            //if (receiveMessage(b) > 0)
            //{
            //    if (b[2] == 0x00 && b[4] == (byte)(Command.CMD_FS + 0x80) && b[5] == 0x00)
            //    {
            //        return b[6];
            //    }
            //}
            return 0;
        }

        public bool setFS(byte fs)
        {
            //emptyBuffer();
            //byte[] b = new byte[20];
            //b[0] = fs;
            //transmitMessage(Command.CMD_FS, 1, b, false);
            //if (receiveMessage(b) > 0)
            //{
            //    if (b[2] == 0x00 && b[4] == (byte)(Command.CMD_FS))
            //    {
            //        return true;
            //    }
            //}
            return false;
        }

        public bool setBtnLog(byte mode, byte freq)
        {
            //emptyBuffer();
            //byte[] b = new byte[20];
            //b[0] = mode;
            //b[1] = freq;
            //transmitMessage(Command.CMD_BTN_LOG, 2, b, false);
            //if (receiveMessage(b) > 0)
            //{
            //    if (b[2] == 0x00 && b[4] == (byte)(Command.CMD_BTN_LOG))
            //    {
            //        return true;
            //    }
            //}
            return false;
        }

        public int[] getBtnLog()
        {
            //emptyBuffer();
            int[] result = new int[2];
            //result[0] = 0;
            //result[1] = 0;
            //byte[] b = new byte[20];
            //transmitMessage(Command.CMD_BTN_LOG, 0, b, true);
            //if (receiveMessage(b) > 0)
            //{
            //    if (b[2] == 0x00 && b[4] == (byte)(Command.CMD_BTN_LOG + 0x80) && b[5] == 0x00)
            //    {
            //        result[0] = b[6];
            //        result[1] = b[7];
            //    }
            //}
            return result;
        }

        private void transmitMessage(Muse_HW.Command cmd, byte payloadLen, byte[] payload, bool read)
        {

            byte[] b = new byte[payloadLen + 6];
            b[0] = (byte)'?';
            b[1] = (byte)'!';
            b[2] = (byte)cmd;
            if (read)
                b[2] |= 0x80;
            b[3] = payloadLen;
            Buffer.BlockCopy(payload, 0, b, 4, payloadLen);
            b[payloadLen + 4] = (byte)'!';
            b[payloadLen + 5] = (byte)'?';
            comPort.Write(b, 0, payloadLen + 6);


        }

        private UInt32 receiveMessage(byte[] payload)
        {
            DateTime t = DateTime.Now;
            while (comPort.BytesToRead == 0 && (DateTime.Now - t).TotalMilliseconds < 3000) ;
            ////Wait until all bytes have been received - 30 ms should be enough
            if ((DateTime.Now - t).TotalMilliseconds < 3000)
            {
                int bytes = comPort.Read(payload, 0, comPort.BytesToRead);

                if (payload[0] == '?' && payload[1] == '!' && payload[bytes - 2] == '!' && payload[bytes - 1] == '?' && bytes == (payload[3] + 6))
                {
                    return payload[3];
                }

            }
            return 0;
        }

      public Muse_HW.Acquisition_Type getLogMode()
        {
            return (Muse_HW.Acquisition_Type)log_mode_list[current_file];
        }


        public int getLogFreq()
        {
            return log_freq_list[current_file] * 25;
        }

        public List<string> getCurrentFS()
        {
            List<string> fs = new List<string>();

            fs.Add(gyro_fs_list_str[current_file]);
            fs.Add(axl_fs_list_str[current_file]);
            fs.Add(magn_fs_list_str[current_file]);
            fs.Add(hdr_axl_fs_list_str[current_file]);

            return fs;
        }

        public UInt32 getCurrentFileMode()
        {
            return log_mode_list[current_file];
        }
    }
}
